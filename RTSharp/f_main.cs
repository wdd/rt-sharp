﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using BrightIdeasSoftware;

using RTSharpIFace;
using static RTSharpIFace.Utils;
using static RTSharp.Utils;
using static RTSharp.Global;
using System.IO.Pipes;
using System.Security.AccessControl;
using System.Windows.Forms.DataVisualization.Charting;

namespace RTSharp {

	public partial class f_main : Form {
		readonly object lockRefresh = new object();

		public enum UPDATE_TORRENTS {
			OFF,
			UPDATE,
			FORCE_UPDATE
		}

		public UPDATE_TORRENTS updateTorrents;

		string AddTorrent;

		public f_main(string AddTorrent = null) {
			syncCtxSTA = SynchronizationContext.Current;
			InitializeComponent();
			t_filter.TextChanged += t_filter_TextChanged;

			this.AddTorrent = AddTorrent;
		}

		public readonly SynchronizationContext syncCtxSTA;

		public object TorrentsLock = new object();
		public List<TORRENT> Torrents = new List<TORRENT>();

#pragma warning disable 4014
		private async void f_main_Load(object sender, EventArgs e) {
			if (SystemInformation.TerminalServerSession)
				return;

			Task.Run(async () => {
				PipeSecurity ps = new PipeSecurity();
				ps.AddAccessRule(new PipeAccessRule("Users", PipeAccessRights.ReadWrite, AccessControlType.Allow));
				ps.AddAccessRule(new PipeAccessRule("SYSTEM", PipeAccessRights.FullControl, AccessControlType.Allow));
				var pipeServer = new NamedPipeServerStream(
					"RTSharpAddTorrent",
					PipeDirection.In,
					2,
					PipeTransmissionMode.Message,
					PipeOptions.WriteThrough | PipeOptions.Asynchronous,
					1024,
					1024,
					ps);
				var sr = new StreamReader(pipeServer);
				for (;;) {
					try {
						pipeServer.WaitForConnection();
						var l = await sr.ReadLineAsync();
						Debug.WriteLine("got pipe " + l);
						switch (l.Substring(0, 4)) {
							case "ATOR":
								InvokeOpt(() => new f_addTorrent(l.Remove(0, 4)).Show());
								break;
						}
						pipeServer.Disconnect();
					} catch (Exception ex) { MessageBox.Show(ex.ToString()); }
				}
			});

			foreach (var s in graph.Series)
				s.Points.Clear();
			graph.Legends[0].BackColor = Color.FromArgb(150, graph.Legends[0].BackColor);

			lv_torrents.SetObjects(Torrents);

			var wait = new WaitingBox("PopulateTorrentFxs...", ProgressBarStyle.Blocks, ProgressBarStyle.Blocks);
			wait.Show();

			Torrent.PopulateFxs();

			wait.UpdatePBarSingle(33);
			wait.UpdateText("InitCompanyToolTip...");

			Peer.InitCompanyToolTip();

			wait.UpdatePBarSingle(66);
			wait.UpdateText("LoadSettings...");

			IniSettings.LoadSettings();

			wait.UpdatePBarSingle(0);
			wait.UpdatePBarAll(33);
			wait.UpdateText("Torrent list...");

			graph.ChartAreas[0].AxisX.LabelStyle.Format = Settings.DateFormat;

			if (lv_torrents.SecondarySortColumn == null) {
				lv_torrents.SecondarySortOrder = SortOrder.Ascending;
				lv_torrents.SecondarySortColumn = lvc_name;
			} else {
				HSecondarySortColumn = lv_torrents.SecondarySortColumn;
				HSecondarySortOrder = lv_torrents.SecondarySortOrder;
			}

			if (lv_peers.SecondarySortColumn == null) {
				lv_peers.SecondarySortOrder = SortOrder.Ascending;
				lv_peers.SecondarySortColumn = lv_peers_ip;
			} else {
				HPSecondarySortColumn = lv_peers.SecondarySortColumn;
				HPSecondarySortOrder = lv_peers.SecondarySortOrder;
			}

			// Setting data source to dummy seems to fix sorting?
			//lv_torrents.VirtualListDataSource = new TorrentCustomSortingDataSource(lv_torrents);

			foreach (var col in lv_torrents.AllColumns) {
				switch (col.Text) {
					case "Name":
						col.AspectGetter = t => ((TORRENT)t).Name;
						break;
					case "Connection":
						col.AspectGetter = t => ((TORRENT)t).Owner;
						break;
					case "Label":
						col.AspectGetter = t => ((TORRENT)t).Label;
						break;
					case "State":
						col.AspectGetter = t => Tuple.Create(((TORRENT)t).State, ((TORRENT)t).ISFAState);
						break;
					case "Size":
						col.AspectGetter = t => ((TORRENT)t).Size;
						break;
					case "Downloaded":
						col.AspectGetter = t => ((TORRENT)t).Downloaded;
						break;
					case "Uploaded":
						col.AspectGetter = t => ((TORRENT)t).Uploaded;
						break;
					case "Remaining":
						col.AspectGetter = t => ((TORRENT)t).RemainingSize;
						break;
					case "DL Speed":
						col.AspectGetter = t => ((TORRENT)t).DLSpeed;
						break;
					case "UP Speed":
						col.AspectGetter = t => ((TORRENT)t).UPSpeed;
						break;
					case "Created On":
						col.AspectGetter = t => ((TORRENT)t).CreatedOnDate;
						break;
					case "Added On":
						col.AspectGetter = t => ((TORRENT)t).AddedOnDate;
						break;
					case "Finished On":
						col.AspectGetter = t => ((TORRENT)t).FinishedOnDate;
						break;
					case "Seeders":
						col.AspectGetter = t => ((TORRENT)t).Seeders;
						break;
					case "Peers":
						col.AspectGetter = t => ((TORRENT)t).Peers;
						break;
					case "Priority":
						col.AspectGetter = t => ((TORRENT)t).Priority;
						break;
					case "Tracker":
						col.AspectGetter = t => ((TORRENT)t).TrackerSingle?.OriginalString;
						break;
					case "Done":
						col.AspectGetter = t => ((TORRENT)t).Done;
						break;
					case "Ratio":
						col.AspectGetter = t => ((TORRENT)t).Ratio;
						break;
					case "ETA":
						col.AspectGetter = t => ((TORRENT)t).ETA;
						break;
				}
				if (FormatFxs.ContainsKey(Tuple.Create(Guid.Empty, "T_" + col.Text)))
					col.AspectToStringConverter = t => FormatFxs[Tuple.Create(Guid.Empty, "T_" + col.Text)](t);
			}

			lvc_tracker.ImageGetter = RowObject => {
				var tracker = FormatFxs[Tuple.Create(Guid.Empty, "T_Tracker")](((TORRENT)RowObject).TrackerSingle?.OriginalString);
				if (tracker == null)
					return null;
				return Tracker.TrackerImgs.ContainsKey(tracker) ? Tracker.TrackerImgs[tracker].Item2 : null;
			};

			lvc_owner.ImageGetter = RowObject => ((TORRENT)RowObject).Owner.Logo;

			Tracker.PopulateFxs();

			foreach (var col in lv_trackers.AllColumns) {
				switch (col.Text) {
					case "URI":
						col.AspectGetter = t => ((TRACKER)t).Uri;
						break;
					case "Status":
						col.AspectGetter = t => ((TRACKER)t).StatusStr;
						break;
					case "Seeders":
						col.AspectGetter = t => ((TRACKER)t).Seeders;
						break;
					case "Peers":
						col.AspectGetter = t => ((TRACKER)t).Peers;
						break;
					case "Downloaded":
						col.AspectGetter = t => ((TRACKER)t).Downloaded;
						break;
					case "Last updated":
						col.AspectGetter = t => ((TRACKER)t).LastUpdated;
						break;
					case "Interval":
						col.AspectGetter = t => ((TRACKER)t).Interval;
						break;
					case "Message":
						col.AspectGetter = t => ((TRACKER)t).StatusMsg;
						break;
				}
				if (FormatFxs.ContainsKey(Tuple.Create(Guid.Empty, "TR_" + col.Text)))
					col.AspectToStringConverter = t => FormatFxs[Tuple.Create(Guid.Empty, "TR_" + col.Text)](t);
			}

			Peer.PopulateFxs();

			foreach (var col in lv_peers.AllColumns) {
				switch (col.Text) {
					case "IP":
						col.AspectGetter = t => ((PEER)t)?.IPPort;
						break;
					case "Client":
						col.AspectGetter = t => ((PEER)t)?.Client;
						break;
					case "Flags":
						col.AspectGetter = t => ((PEER)t)?.Flags;
						break;
					case "Done":
						col.AspectGetter = t => ((PEER)t)?.Done;
						break;
					case "DL Speed":
						col.AspectGetter = t => ((PEER)t)?.DLSpeed;
						break;
					case "UP Speed":
						col.AspectGetter = t => ((PEER)t)?.UPSpeed;
						break;
					case "Downloaded":
						col.AspectGetter = t => ((PEER)t)?.Downloaded;
						break;
					case "Uploaded":
						col.AspectGetter = t => ((PEER)t)?.Uploaded;
						break;
				}
				if (FormatFxs.ContainsKey(Tuple.Create(Guid.Empty, "P_" + col.Text)))
					col.AspectToStringConverter = t => FormatFxs[Tuple.Create(Guid.Empty, "P_" + col.Text)](t);
			}

			dtlv_files.RootKeyValue = -1;
			dtlv_files.ParentKeyAspectName = "Parent";
			dtlv_files.KeyAspectName = "ArrayIndex";
			dtlv_files.AutoGenerateColumns = false;

			File.PopulateFxs();

			foreach (var col in dtlv_files.AllColumns) {
				switch (col.Text) {
					case "Name":
						col.AspectGetter = t => ((FILES)t).Item.Name;
						break;
					case "Size":
						col.AspectGetter = t => ((FILES)t).Item.Size;
						break;
					case "Downloaded":
						col.AspectGetter = t => ((FILES)t).Item.Downloaded;
						break;
					case "Done":
						col.AspectGetter = t => ((FILES)t).Item.Done;
						break;
					case "Priority":
						col.AspectGetter = t => ((FILES)t).Item.Priority;
						break;
				}
				if (FormatFxs.ContainsKey(Tuple.Create(Guid.Empty, "F_" + col.Text)))
					col.AspectToStringConverter = t => FormatFxs[Tuple.Create(Guid.Empty, "F_" + col.Text)](t);
			}

			lvc_name.ImageGetter = p => {
				var t = (TORRENT)p;
				if (t.State.HasFlag(TORRENT_STATE.ERRORED))
					return "IMG_ERRORED";
				if (t.State.HasFlag(TORRENT_STATE.DOWNLOADING))
					return "IMG_DOWNLOAD";
				if (t.State.HasFlag(TORRENT_STATE.ACTIVE))
					return "IMG_ACTIVE";
				if (t.State.HasFlag(TORRENT_STATE.SEEDING))
					return "IMG_UPLOAD";
				if (t.State.HasFlag(TORRENT_STATE.STOPPED))
					return "IMG_STOPPED";
				return "IMG_INACTIVE";
			};

			lv_peers_flag.ImageGetter = p => ((PEER)p)?.ImageKey ?? "qq";

			olv_sdss_main.ImageGetter = p => {
				var c = (ConnectionStatus)p;
				if (c.Status)
					return c.Owner.OkStatusImage == null ? "IMG_CONNECTED" : c.Owner.UniqueGUID + "|OK";
				return c.Owner.NotOkStatusImage == null ? "IMG_DISCONNECTED" : c.Owner.UniqueGUID + "|NOK";
			};

			lv_trackers_last_updated.AspectToStringConverter = Value => UnixTimeStampToDateTime((ulong)Value).ToString(Settings.DateFormat);
			lv_trackers_interval.AspectToStringConverter = Value => ToAgoString(TimeSpan.FromSeconds((uint)Value));

			var renderer = new DescribedTaskRenderer { DescriptionAspectName = "Description" };
			olv_sdss_main.Renderer = renderer;

			RatioColoring = (ratio) => {
				Color? color;
				if (ratio < 1)
					color = Color.FromArgb(255 - (int)(ratio / 1 * 100), 0, 0);
				else if (ratio >= 1 && ratio <= 5)
					color = Color.FromArgb(0, 100 + (int)((ratio - 1) / 4 * 155), 0);
				else if (ratio > 5 && ratio <= 25)
					color = Color.FromArgb(255 - (int)((ratio - 5) / 20 * 245), 0, 255 - (int)((ratio - 5) / 25 * 245));
				else if (ratio == Double.MaxValue)
					color = Color.FromArgb(0, 0, 0);
				else
					color = Color.FromArgb(255, 255, 255);

				return Tuple.Create(color, ratio > 5 ? (Color?)Color.Black : null);
			};

			RatioColoringEx = (ratio, item) => {
				if (ratio > 25 && ratio != Single.MaxValue)
					item.Decoration = new ImageDecoration(Properties.Resources.lightning, 100, ContentAlignment.MiddleRight);
			};

			wait.UpdatePBarAll(44);
			wait.UpdateText("Load plugins: Base");

			#region Load plugins

			// First, check if The Plugins File exists, if it doesn't, throw a big fat untitled error and shut down everything.
			if (!System.IO.File.Exists("plugins.ini")) {
				Logger.Log(LOG_LEVEL.FATAL, "plugins.ini not found");
				MessageBox.Show("PLUGINS.INI NOT FOUND");
				Debug.Assert(false);
				// ReSharper disable once HeuristicUnreachableCode
				Environment.Exit(1);
			}

			var pluginsIni = new FileStream("plugins.ini", FileMode.Open, FileAccess.ReadWrite);

			var pBaseAsm = Assembly.LoadFrom("RTSharpPlugin.dll");

			foreach (var t in pBaseAsm.GetTypes()) {
				if (t == typeof(PluginBase)) {
					var pBase = Activator.CreateInstance(t) as PluginBase;
					Debug.Assert(pBase != null);
					pBase.Init(Assembly.GetExecutingAssembly());
				}
				if (t.GetInterface("IRTSharpImports") != null) {
					RTSharpPluginImports = Activator.CreateInstance(t) as IRTSharpImports;
				}
			}

			if (RTSharpPluginImports == null) {
				Logger.Log(LOG_LEVEL.FATAL, "Failed to import class from plugin base");
				Environment.Exit(0);
			}

			wait.UpdatePBarAll(55);

			uint totalLoadedPlugins = 0;
			if (Directory.Exists("plugins")) {

				if (!Directory.Exists(Path.Combine("plugins", "configs"))) {
					Directory.CreateDirectory(Path.Combine("plugins", "configs"));
					Logger.Log(LOG_LEVEL.DEBUG, "Created directory \"plugins/configs\"");
				}

				var plugins = new Dictionary<string, List<Guid>>();
				var newFile = "";

				using (var sw = new StreamReader(pluginsIni)) {
					string line;
					
					while (!String.IsNullOrEmpty(line = sw.ReadLine())) {
 						if (line.StartsWith("#") || line.StartsWith(";") || line.StartsWith("//")) {
							newFile += line + Environment.NewLine;
							continue;
						}
						
						Guid uniq;
						var parts = line.Split(' ');
						Debug.Assert(parts.Length == 2);

						if (!Guid.TryParse(parts[0], out uniq)) {
							Logger.Log(LOG_LEVEL.DEBUG, "Generating new unique GUID for " + parts[1]);
							uniq = Guid.NewGuid();
							Logger.Log(LOG_LEVEL.DEBUG, uniq.ToString());
						}

						newFile += uniq + " " + parts[1] + Environment.NewLine;
						
						var name = Path.Combine("plugins", parts[1] + ".dll");

						if (plugins.ContainsKey(name))
							plugins[name].Add(uniq);
						else
							plugins.Add(name, new List<Guid>() { uniq });
					}
					newFile = newFile.Remove(newFile.Length - Environment.NewLine.Length, Environment.NewLine.Length);

					pluginsIni.Position = 0;
					pluginsIni.SetLength(0);
					pluginsIni.Write(Encoding.UTF8.GetBytes(newFile), 0, newFile.Length);
				}

				var asm = new Dictionary<Assembly, List<Guid>>();
				foreach(var p in plugins.Keys) {
					Logger.Log(LOG_LEVEL.DEBUG, "Loading assembly " + p + "...");
					asm.Add(Assembly.LoadFrom(p), plugins[p]);
				}

				var errors = "";

				if (asm.Count == 0) {
					Logger.Log(LOG_LEVEL.WARN, "No assemblies loaded");
					MessageBox.Show("Hello there, it seems that you have no plugins. The program is entirely useless without plugins.", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Question);
				}

				var x = 0;
				foreach (var kv in asm) {
					SetStatus("Loading plugins: " + x + " / " + asm.Keys.Count, false);
					x++;
					try {
						foreach (var t in kv.Key.GetTypes().Where(ft => ft.GetInterfaces().Contains(typeof(ISDS)))) {
							foreach (var uniq in kv.Value) {
								var sds = Activator.CreateInstance(t) as ISDS;
								Logger.Log(LOG_LEVEL.DEBUG, "Initializing SDS " + sds.Name + "...");
								try {
									sds.Init(new PRTSharp(sds), uniq, wait);
								} catch (Exception ex) {
									Logger.Log(LOG_LEVEL.ERROR, sds.UniqueGUID + " initialization failed");
									Logger.LogException(LOG_LEVEL.ERROR, ex);
									errors += sds.FriendlyName + " initialization failed:" + Environment.NewLine + ex + Environment.NewLine;
									continue;
								}
								SDSS.Add(new SystemDataSupplierClass(kv.Key, sds));
								Plugins.Add(new PluginClass(kv.Key, sds));

								wait.UpdateText("Load SDS: " + sds.FriendlyName);

								if (sds.OkStatusImage != null)
									ImgList.Images.Add(sds.UniqueGUID + "|OK", sds.OkStatusImage);

								if (sds.NotOkStatusImage != null)
									ImgList.Images.Add(sds.UniqueGUID + "|NOK", sds.NotOkStatusImage);

								totalLoadedPlugins++;
							}
							
						}
						foreach (var t in kv.Key.GetTypes().Where(ft => ft.GetInterfaces().Contains(typeof(IPlugin)) && !ft.GetInterfaces().Contains(typeof(ISDS)))) {
							foreach (var uniq in kv.Value) {
								var plugin = Activator.CreateInstance(t) as IPlugin;
								Logger.Log(LOG_LEVEL.DEBUG, "Initializing plugin " + plugin.Name + "...");
								try {
									plugin.Init(new PRTSharp(plugin), uniq, wait);

									wait.UpdateText("Load plugin: " + plugin.UniqueGUID);
								} catch (Exception ex) {
									Logger.Log(LOG_LEVEL.ERROR, uniq + " initialization failed");
									Logger.LogException(LOG_LEVEL.ERROR, ex);
									errors += uniq + " initialization failed:" + Environment.NewLine + ex + Environment.NewLine;
									continue;
								}
								Plugins.Add(new PluginClass(kv.Key, plugin));
								totalLoadedPlugins++;
							}
						}
					} catch (Exception ex) {
						Logger.Log(LOG_LEVEL.ERROR, "Unknown error at " + kv.Key.FullName);
						Logger.LogException(LOG_LEVEL.ERROR, ex);
						errors += "Failed to load plugin \"" + kv.Key.FullName + "\": " + ex + Environment.NewLine;
					}
				}

				Logger.Log(LOG_LEVEL.DEBUG, "Loaded " + totalLoadedPlugins + " plugin of " + plugins.Sum(p => p.Value.Count));
				SetStatus("Successfully loaded " + totalLoadedPlugins + " plugins (out of " + plugins.Sum(p => p.Value.Count) + ")", false);
				if (!String.IsNullOrEmpty(errors))
					new ResultsBox().Show(errors);
			} else
				MessageBox.Show("Hello there, it seems that you have no plugins. The program is entirely useless without plugins.", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Question);

			IniSettings.SaveSettings(); // Save SDSs after loading them

			#endregion Load plugins
			wait.UpdatePBarAll(66);
			#region Events

			RTSEvents.TorrentStateChange += RTSEvents_TorrentStateChange;
			RTSEvents.NewTorrent += RTSEvents_NewTorrent;
			RTSEvents.TorrentRemoved += File.EvTorrentRemoved;
			ISFAQueue.HookEvents();

			#endregion Events
			wait.UpdatePBarAll(77);
			#region Load cache

			if (!Directory.Exists("cache")) {
				Directory.CreateDirectory("cache");
				Logger.Log(LOG_LEVEL.DEBUG, "Created directory \"cache\"");
			}

			if (!System.IO.File.Exists("cache/company.dat")) {
				System.IO.File.Create("cache/company.dat").Close();
				Logger.Log(LOG_LEVEL.DEBUG, "Created file \"cache/company.dat\"");
			}

			if (!System.IO.File.Exists("cache/tracker.dat")) {
				System.IO.File.Create("cache/tracker.dat").Close();
				Logger.Log(LOG_LEVEL.DEBUG, "Created file \"cache/tracker.dat\"");
			}

			if (!System.IO.File.Exists("cache/files.dat")) {
				System.IO.File.Create("cache/files.dat").Close();
				Logger.Log(LOG_LEVEL.DEBUG, "Created file \"cache/files.dat\"");
			}

			if (!Directory.Exists("cache/company")) {
				Directory.CreateDirectory("cache/company");
				Logger.Log(LOG_LEVEL.DEBUG, "Created directory \"cache/company\"");
			}
			if (!Directory.Exists("cache/tracker")) {
				Directory.CreateDirectory("cache/tracker");
				Logger.Log(LOG_LEVEL.DEBUG, "Created directory \"cache/tracker\"");
			}

			Logger.Log(LOG_LEVEL.DEBUG, "Loading caches...");
			Logger.Log(LOG_LEVEL.DEBUG, "Trackers...");
			Tracker.LoadTrackerCache(wait);
			wait.UpdatePBarAll(88);
			Logger.Log(LOG_LEVEL.DEBUG, "Peer companies...");
			Peer.LoadCompanyCache(wait);
			wait.UpdatePBarAll(100);
			Logger.Log(LOG_LEVEL.DEBUG, "Files...");
			File.LoadFileCache(wait);
			wait.Close();

			#endregion Load cache

			List<Task> connWait = new List<Task>();

			foreach (var sds in SDSS) {
				connWait.Add(Task.Run(async () => {
					try {
						wait.UpdateText("Load SDS: " + sds.SDS.FriendlyName + ", connecting...");
						Logger.Log(LOG_LEVEL.DEBUG, "Connecting " + sds.SDS.UniqueGUID + "...");
						await sds.SDS.InitConnection();
					} catch (Exception ex) {
						Logger.Log(LOG_LEVEL.WARN, sds.SDS.UniqueGUID + " connection failed");
						Logger.LogException(LOG_LEVEL.WARN, ex);
					}
				}));
			}

			await Task.WhenAll(connWait);

			updateTorrents = UPDATE_TORRENTS.FORCE_UPDATE;

			Logger.Log(LOG_LEVEL.INFO, "RT# ready");

			if (AddTorrent != null)
				new f_addTorrent(AddTorrent).Show();

			await Task.Run(GlobalRefresh); // wait foreveer
		}

		private void RTSEvents_NewTorrent(object sender, RTSEvents.NewTorrentArgs e)
		{
			Logger.Log(LOG_LEVEL.DEBUG, "Event: New torrent, fetching peers and files...");

			Peer.FetchPeers(e.Torrent);
			File.FetchFiles(e.Torrent);
		}


		private async void RTSEvents_TorrentStateChange(object sender, RTSEvents.TorrentStateArgs e)
		{
			var torrent = (TORRENT)sender;
			Logger.Log(LOG_LEVEL.DEBUG, "Event: Torrent state change");

			if (e.State.HasFlag(TORRENT_STATE.PAUSED) || e.State.HasFlag(TORRENT_STATE.STOPPED))
				torrent.DLSpeed = 0;

			if (!(e.State.HasFlag(TORRENT_STATE.ACTIVE) || e.State.HasFlag(TORRENT_STATE.DOWNLOADING)))
				torrent.PeerList = null;

			if (e.State.HasFlag(TORRENT_STATE.SEEDING)) {
				try {
					var t = await torrent.Owner.GetTorrents(new[] { torrent.Hash });
					Torrent.TorrentUpdate(torrent, t.First());
					lv_torrents.UpdateObject(torrent);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to get single torrent after state change");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
				File.FetchFiles(torrent);
			}
		}
#pragma warning restore 4014

		async Task GlobalRefresh()
		{
			for (;;) {
				lock (lockRefresh) { }
				var c = Settings.RefreshInterval / 50;
				for (var i = 0; i < c; i++) {
					if (updateTorrents != UPDATE_TORRENTS.OFF)
						break;
					await Task.Delay(50);
				}
				if (updateTorrents == UPDATE_TORRENTS.OFF)
					updateTorrents = UPDATE_TORRENTS.UPDATE;

				RefreshTorrents(updateTorrents != UPDATE_TORRENTS.UPDATE);
				PopulateSDSSList().Wait();

				//await Task.WhenAll(refresh, sdss);
				updateTorrents = UPDATE_TORRENTS.OFF;
			}
			// ReSharper disable once FunctionNeverReturns
		}

		public void ShowTrayMessage(string Title, string Message, ToolTipIcon TipIcon, int Timeout = 2000)
		{
			InvokeOpt(() => ni_global.ShowBalloonTip(Timeout, Title, Message, TipIcon));
		}

		public async Task SetStatus(string In, bool Error)
		{
			await InvokeOpt(async () => {
				l_status.Text = In;
				l_status.ForeColor = Error ? Color.Red : SystemColors.ControlText;
				await Task.Delay(TimeSpan.FromSeconds(5));
				l_status.Text = "";
			});
		}

		bool a = false;
		public void RefreshTorrents(bool fullUpdate)
		{
			if (a)
				Debugger.Break();
			a = true;
			var allTasks = new List<Task>();

			foreach (var sds in SDSS) {
				if (sds.UpdateTask != null && !sds.UpdateTask.IsCompleted) {
					sds.UpdateSkipCounter++;
					Logger.Log(LOG_LEVEL.INFO, sds.SDS.FriendlyName + " (" + sds.SDS.UniqueGUID + "): Update task is still running on next update cycle (x" + sds.UpdateSkipCounter + "), skipping update...");

					if (sds.UpdateSkipCounter == 20)
						ShowTrayMessage(
							sds.SDS.FriendlyName + " (" + sds.SDS.UniqueGUID + ")",
							"Update interval is too fast for this connection (" + Settings.RefreshInterval + " ms)",
							ToolTipIcon.Info,
							0);

					continue;
				}

				if (fullUpdate || sds.SDS.FullUpdate) {
					sds.UpdateTask = Task.Run(async () => {
						List<TORRENT> t;
						try {
							t = await sds.SDS.GetTorrents(null);
						} catch (Exception ex) {
							Logger.Log(LOG_LEVEL.WARN, "GetAll failed for " + sds.SDS.UniqueGUID);
							Logger.LogException(LOG_LEVEL.WARN, ex);
							return;
						}
						IEnumerable<TORRENT> r;
						lock (TorrentsLock)
							r = Torrents.Where(x => x.Owner == sds.SDS).ToList();

						syncCtxSTA.Post(o => {
							lv_torrents.RemoveObjects((List<TORRENT>)o);
						}, r);

						lock (TorrentsLock) {
							Torrents.RemoveAll(x => x.Owner == sds.SDS);
							Torrents.AddRange(t);
						}

						syncCtxSTA.Post(o => {
							lv_torrents.AddObjects((List<TORRENT>)o);
							lv_torrents.Sort();
						}, t.ToList());

						await File.FetchFiles(t.ToList());
					}).ContinueWith(x => sds.SDS.FullUpdate = false);
				} else {
					sds.UpdateTask = Task.Run(async () => {
						IEnumerable<TORRENT> t;
						try {
							t = await sds.SDS.GetAllChanged();
						} catch (Exception ex) {
							Logger.Log(LOG_LEVEL.WARN, "GetAllChanged failed for " + sds.SDS.UniqueGUID);
							Logger.LogException(LOG_LEVEL.WARN, ex);
							return;
						}

						lock (TorrentsLock) {
							t.ToList().AsParallel().ForAll(a => {
								var found = Torrents.Find(x => x.Equals(a));
								Torrent.TorrentUpdate(found, a);
							});
						}

						if (t.Count() != 0) {
							lv_torrents.RefreshObjects(t.ToList());
							lv_torrents.Sort();
						}
					});
				}
			}

			Peer.FetchPeersForSelectedTorrent();
			File.FetchFilesForSelectedTorrent();
			RefreshGeneralTab();
			RefreshGraphTab();

			a = false;
		}

		private void b_pause_Click(object sender, EventArgs e) {
			b_resume.Visible = true;
			b_pause.Visible = false;
			Monitor.Enter(lockRefresh);
		}

		private void b_resume_Click(object sender, EventArgs e) {
			b_resume.Visible = false;
			b_pause.Visible = true;
			Monitor.Exit(lockRefresh);
		}

		private void b_addTorrent_Click(object sender, EventArgs e) {
			new f_addTorrent().Show();
		}

		private void b_refresh_Click(object sender, EventArgs e)
		{
			Logger.Log(LOG_LEVEL.DEBUG, "Force updating...");
			updateTorrents = UPDATE_TORRENTS.FORCE_UPDATE;
		}

		private void f_main_FormClosing(object sender, FormClosingEventArgs e)
		{
			Logger.Log(LOG_LEVEL.DEBUG, "Saving settings...");
			IniSettings.SaveSettings();

			Logger.Log(LOG_LEVEL.DEBUG, "Saving cache...");
			Logger.Log(LOG_LEVEL.DEBUG, "Peer companies...");
			Peer.SaveCompanyCache();
			Logger.Log(LOG_LEVEL.DEBUG, "Trackers...");
			Tracker.SaveTrackerCache();
			Logger.Log(LOG_LEVEL.DEBUG, "Files...");
			File.SaveFileCache();

			Logger.Log(LOG_LEVEL.DEBUG, "Unloading plugins...");
			foreach (var p in Plugins)
				p.IFace.Unload();

			Logger.Log(LOG_LEVEL.DEBUG, "Goodbye");

			Environment.Exit(0);
		}

		bool SDSSListOpen;

		async Task PopulateSDSSList()
		{
			if (!SDSSListOpen || !IsHandleCreated)
				return;

			await InvokeOpt(async () => {
				var copy = lv_sdss.SelectedIndex;
				var cs = new List<ConnectionStatus>();
				foreach (var sds in SDSS) {
					ConnectionStatus res;
					try {
						res = await sds.SDS.GetStatus();
					} catch (Exception ex) {
						Logger.Log(LOG_LEVEL.WARN, "Failed to get status of " + sds.SDS.UniqueGUID);
						Logger.LogException(LOG_LEVEL.WARN, ex);
						return;
					}
					cs.Add(res);
				}
				lv_sdss.SetObjects(cs);
				lv_sdss.SelectedIndex = copy;
			});
		}

		private void lv_sdss_CellRightClick(object sender, CellRightClickEventArgs e)
		{
			if (e.Item == null)
				return;

			var status = (ConnectionStatus)e.Item.RowObject;
			var sdss = SDSS.First(x => x.SDS.UniqueGUID == status.Owner.UniqueGUID);
			e.MenuStrip = sdss.SDS.GetPluginMenu();
		}

		SortOrder HSecondarySortOrder =	SortOrder.Ascending;
		OLVColumn HSecondarySortColumn;
		SortOrder HPSecondarySortOrder = SortOrder.Ascending;
		OLVColumn HPSecondarySortColumn;

		private void OLVBeforeSorting(object sender, BeforeSortingEventArgs e)
		{
			if (((FastObjectListView)sender).PrimarySortColumn == e.ColumnToSort) {
				((FastObjectListView)sender).PrimarySortOrder = e.SortOrder;
				switch (((FastObjectListView) sender).Name) {
					case "lv_torrents":
						HSecondarySortOrder = e.SortOrder;
						break;
					case "lv_peers":
						HPSecondarySortOrder = e.SortOrder;
						break;
				}
				
			} else {
				((FastObjectListView)sender).SecondarySortColumn = ((FastObjectListView)sender).PrimarySortColumn;
				switch (((FastObjectListView)sender).Name) {
					case "lv_torrents":
						HSecondarySortColumn = ((FastObjectListView)sender).SecondarySortColumn;
						((FastObjectListView)sender).SecondarySortOrder = HSecondarySortOrder;
						break;
					case "lv_peers":
						HPSecondarySortColumn = ((FastObjectListView)sender).SecondarySortColumn;
						((FastObjectListView)sender).SecondarySortOrder = HPSecondarySortOrder;
						break;
				}
				
				((FastObjectListView)sender).PrimarySortColumn = e.ColumnToSort;
			}
		}

		private void OLVFilter(object sender, FilterEventArgs e)
		{
			switch (((FastObjectListView)sender).Name) {
				case "lv_torrents":
					((FastObjectListView)sender).SecondarySortColumn = HSecondarySortColumn;
					((FastObjectListView)sender).SecondarySortOrder = HSecondarySortOrder;
					break;
				case "lv_peers":
					((FastObjectListView)sender).SecondarySortColumn = HPSecondarySortColumn;
					((FastObjectListView)sender).SecondarySortOrder = HPSecondarySortOrder;
					break;
			}
		}

		// HACKHACK
		object CurSelectedObject;
		private void lv_torrents_CellRightClick(object sender, CellRightClickEventArgs e)
		{
			var torrent = (TORRENT)e?.Item?.RowObject;

			if (torrent == null)
				return;

			if (e.Column?.Text == "Tracker") {
				lock (Tracker.TrackerImgsLock)
					b_fetch_icon.Enabled = !Tracker.TrackerImgs.ContainsKey(torrent.TrackerSingle.OriginalString);

				b_set_alias.Enabled = !Settings.TrackerMatches.ContainsValue(torrent.TrackerSingle.OriginalString);

				e.MenuStrip = ctxms_tracker;
			} else if (e.Column?.Text == "Connection") {
				e.MenuStrip = ctxms_connection;
			} else if (torrent.ISFAState == TORRENT_ISFA.PENDING || torrent.ISFAState == TORRENT_ISFA.EXECUTING) {
				l_sep1.Visible = true;
				b_isfaQueue.Visible = true;
				b_isfaTorrent.Enabled = false;
				e.MenuStrip = ctxms_torrent;
			} else {
				l_sep1.Visible = false;
				b_isfaQueue.Visible = false;
				b_isfaTorrent.Enabled = true;
				e.MenuStrip = ctxms_torrent;
				return;
			}

			CurSelectedObject = e.Item.RowObject;
		}

		private void b_copy_info_hash_Click(object sender, EventArgs e)
		{
			var t = Torrent.GetSelectedTorrents();
			var selEnumer = t as TORRENT[] ?? t.ToArray();

			if (selEnumer.Length > 1) {
				Clipboard.SetText(String.Join(Environment.NewLine, selEnumer.Select(x => x.Name + ": " + ByteArrayToHexString(x.Hash))));
			} else
				Clipboard.SetText(ByteArrayToHexString(selEnumer.First().Hash));
		}

		[SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
		private void lv_torrents_FormatCell(object sender, FormatCellEventArgs e)
		{
			if (e.ColumnIndex != lvc_ratio.Index)
				return;

			var ratio = ((TORRENT)e.Model).Ratio;
			
			var ret = RatioColoring(ratio);
			if (ret.Item1.HasValue)
				e.SubItem.ForeColor = ret.Item1.Value;
			if (ret.Item2.HasValue)
			e.SubItem.BackColor = ret.Item2.Value;

			RatioColoringEx?.Invoke(ratio, e.SubItem);
		}

		private void t_animate_Tick(object sender, EventArgs e)
		{
			if (updateTorrents != UPDATE_TORRENTS.OFF) {
				b_refresh.Image.RotateFlip(RotateFlipType.Rotate90FlipNone);
				b_refresh.Invalidate();
			}
		}

		private void t_filter_TextChanged(object sender, EventArgs e)
		{
			if (t_filter.Text == t_filter.Placeholder)
				return;
			lv_torrents.ModelFilter = TextMatchFilter.Regex(lv_torrents, t_filter.Text);
		}

		private void b_sdss_Click(object sender, EventArgs e)
		{
			new f_plugins(true).ShowDialog();
		}

		private void b_plugins_Click(object sender, EventArgs e)
		{
			new f_plugins(false).ShowDialog();
		}

		private void b_exit_Click(object sender, EventArgs e)
		{
			f_main_FormClosing(null, null);
		}

		private void b_about_Click(object sender, EventArgs e)
		{
			new f_about().ShowDialog();
		}

		private void b_setSDSHighlighting_Click(object sender, EventArgs e)
		{
			var t = (TORRENT)CurSelectedObject;
			List<TORRENT> ts;

			var cd = new ColorDialog();
			cd.ShowDialog();

			if (Settings.SDSHighlighting.ContainsKey(t.Owner.UniqueGUID))
				Settings.SDSHighlighting[t.Owner.UniqueGUID] = cd.Color;
			else
				Settings.SDSHighlighting.Add(t.Owner.UniqueGUID, cd.Color);
			IniSettings.SaveSettings();

			lock (TorrentsLock)
				ts = Torrents.Where(x => x.Owner.UniqueGUID == t.Owner.UniqueGUID).ToList();

			lv_torrents.RefreshObjects(ts);
		}

		private void lv_torrents_FormatRow(object sender, FormatRowEventArgs e)
		{
			 if (Settings.SDSHighlighting == null || !Settings.SDSHighlighting.ContainsKey(((TORRENT)e.Model).Owner.UniqueGUID))
				return;

			var sdsColor = Settings.SDSHighlighting[((TORRENT)e.Model).Owner.UniqueGUID];
			e.Item.BackColor = sdsColor;
		}

		public void RefreshGraphTab()
		{
			var sel = Torrent.GetSelectedTorrents();

			if (sel.Count() != 1)
				return;

			var torrent = sel.First();

			InvokeOpt(() => {
				var dlPoints = graph.Series["DL Speed"].Points;
				var upPoints = graph.Series["UP Speed"].Points;

				graph.Series["UP Speed"].Points.DataBindXY(torrent.UPSpeedHistory.Keys, torrent.UPSpeedHistory.Values);
				graph.Series["DL Speed"].Points.DataBindXY(torrent.DLSpeedHistory.Keys, torrent.DLSpeedHistory.Values);

			});
		}

		public void RefreshGeneralTab()
		{
			var ts = Torrent.GetSelectedTorrents();

			TORRENT torrent = null;

			if (ts.Count() == 1)
				torrent = ts.First();
			else if (ts.Count() > 1) {
				torrent = new TORRENT(null, new byte[] { });
				torrent.Downloaded = (ulong)ts.Sum(x => (decimal)x.Downloaded);
				torrent.Uploaded = (ulong)ts.Sum(x => (decimal)x.Uploaded);
				torrent.Seeders = Tuple.Create((uint)ts.Sum(x => x.Seeders.Item1), (uint)ts.Sum(x => x.Seeders.Item2));
				torrent.Peers = Tuple.Create((uint)ts.Sum(x => x.Peers.Item1), (uint)ts.Sum(x => x.Peers.Item2));
				torrent.DLSpeed = (ulong)ts.Sum(x => (decimal)x.DLSpeed);
				torrent.UPSpeed = (ulong)ts.Sum(x => (decimal)x.UPSpeed);
				torrent.Ratio = (float)torrent.Uploaded / torrent.Downloaded;
			}

			InvokeOpt(() => {
				if (torrent == null) {
					l_valTimeElapsed.Text =	l_valDownloaded.Text =	l_valUploaded.Text =	l_valSeeders.Text =
					l_valRemaining.Text =	l_valDlSpeed.Text =		l_valUpSpeed.Text =		l_valPeers.Text =
					l_valRatio.Text =		l_valWasted.Text =		l_valRemotePath.Text =	l_valCreatedOn.Text =
					l_valAddedOn.Text =		l_valInfoHash.Text =	l_valComment.Text =		l_valStatus.Text = "";
				} else {
					if (torrent.FinishedOnDate != 0 && torrent.AddedOnDate != 0) {
						var v = FormatFxs[Tuple.Create(Guid.Empty, "T_ETA")]((torrent.FinishedOnDate - torrent.AddedOnDate));
						l_valTimeElapsed.Text = v == "" ? "0s" : v;
					} else
						l_valTimeElapsed.Text = "0s";
					l_valDownloaded.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_Downloaded")](torrent.Downloaded);
					l_valUploaded.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_Uploaded")](torrent.Uploaded);
					l_valSeeders.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_Seeders")](torrent.Seeders);
					l_valRemaining.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_Remaining")](torrent.RemainingSize);
					l_valDlSpeed.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_DL Speed")](torrent.DLSpeed);
					l_valUpSpeed.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_UP Speed")](torrent.UPSpeed);
					l_valPeers.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_Peers")](torrent.Peers);
					l_valRatio.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_Ratio")](torrent.Ratio);

					var coloring = RatioColoring(torrent.Ratio);
					if (coloring.Item1.HasValue)
						l_valRatio.ForeColor = coloring.Item1.Value;
					else
						l_valRatio.ForeColor = SystemColors.ControlText;

					if (coloring.Item2.HasValue)
						l_valRatio.BackColor = coloring.Item2.Value;
					else
						l_valRatio.BackColor = Color.Transparent;

					l_valWasted.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_Size")](torrent.Wasted);

					l_valRemotePath.Text = torrent.RemotePath;
					l_valCreatedOn.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_Created On")](torrent.CreatedOnDate);
					l_valAddedOn.Text = FormatFxs[Tuple.Create(Guid.Empty, "T_Added On")](torrent.AddedOnDate);
					l_valInfoHash.Text = ByteArrayToHexString(torrent.Hash);
					l_valComment.Text = torrent.Comment;
					l_valStatus.Text = torrent.StatusMsg; // t.latest_event
				}
			});
		}

		int tabs_rightSelected = -1;
		private async void tabs_right_MouseClick(object sender, MouseEventArgs e)
		{
			int curTab = -1;
			for (int x = 0; x < tabs_right.TabCount; ++x) {
				if (tabs_right.GetTabRect(x).Contains(e.Location)) {
					curTab = x;
					break;
				}
			}

			if (curTab == -1)
				return;

			Func<Task> expand = async () => {
				tabs_right.Size = new System.Drawing.Size(400, tabs_right.Size.Height);

				switch (curTab) {
					case 0:
						if (SDSS.Count == 0)
							return;

						SDSSListOpen = true;

						await PopulateSDSSList();
						break;
				}
			};

			Action contract = () => {
				tabs_right.Size = new System.Drawing.Size(24, tabs_right.Size.Height);
				SDSSListOpen = false;
			};

			if (tabs_rightSelected == -1 || (tabs_rightSelected != -1 && tabs_rightSelected != curTab)) {
				await expand();
				tabs_rightSelected = curTab;
			} else if (tabs_rightSelected != -1 && tabs_rightSelected == curTab) {
				contract();
				tabs_rightSelected = -1;
			}
		}

		private void b_settings_Click(object sender, EventArgs e)
		{
			new f_settings().ShowDialog();
		}

		private void f_main_DragDrop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.Text)) {
				new f_addTorrent((string)e.Data.GetData(DataFormats.Text)).Show();
			}
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
				string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

				if (!files.All(x => Path.GetExtension(x) == ".torrent")) {
					e.Effect = DragDropEffects.None;
					return;
				}

				e.Effect = DragDropEffects.Copy;
				new f_addTorrent(files).Show();
			}
		}

		private void f_main_DragOver(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.Text) || e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;
		}

		private void graph_Customize(object sender, EventArgs e)
		{
			double max = 0, max2 = 0;
			if (graph.Series["UP Speed"].Points.Count != 0) {
				max = graph.Series["UP Speed"].Points.Max(x => x.YValues[0]);
				graph.Series["UP Speed"].Points.Where(x => x.YValues[0] == max).First().Label = GetSizeSI((long)max);
			}

			if (graph.Series["DL Speed"].Points.Count != 0) {
				max2 = graph.Series["DL Speed"].Points.Max(x => x.YValues[0]);
				graph.Series["DL Speed"].Points.Where(x => x.YValues[0] == max2).First().Label = GetSizeSI((long)max2);
			}

			foreach (var lbl in graph.ChartAreas[0].AxisY.CustomLabels)
				lbl.Text = GetSizeSI(UInt64.Parse(lbl.Text));

			var div = Math.Max(max, max2) / 5 / 1024;
			graph.ChartAreas[0].AxisY.Interval = (div + (1 - (div % 1))) * 1024;
		}

		ToolTip tooltip = new ToolTip();
		Point lastPoint;
		private void graph_MouseMove(object sender, MouseEventArgs e)
		{
			var pos = e.Location;
			
			var results = graph.HitTest(pos.X, pos.Y, false, ChartElementType.DataPoint);

			if (lastPoint == null || lastPoint == pos)
				return;

			tooltip.RemoveAll();

			DataPoint curPoint = null;
			Point curPos = new Point();
			double min = Double.MaxValue;

			foreach (var result in results) {
				if (result.ChartElementType == ChartElementType.DataPoint) {
					var prop = result.Object as DataPoint;
					if (prop != null) {
						var pointXPixel = result.ChartArea.AxisX.ValueToPixelPosition(prop.XValue);
						var pointYPixel = result.ChartArea.AxisY.ValueToPixelPosition(prop.YValues[0]);

						double len = Math.Sqrt(Math.Pow(pos.X - pointXPixel, 2) + Math.Pow(pos.Y - pointYPixel, 2));
						if (len < min) {
							curPoint = prop;
							curPos = pos;
						}
					}
				}
			}
			if (curPoint != null) {
				tooltip.Show(GetSizeSI((long)curPoint.YValues[0]) + "/s at " + DateTime.FromOADate(curPoint.XValue).ToString(Settings.DateFormat), graph, curPos.X, curPos.Y - 15);
			}
			lastPoint = pos;
		}

		private void lv_sdss_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			var info = lv_sdss.HitTest(e.Location);
			if (info.Item == null)
				return;

			var status = (ConnectionStatus)lv_sdss.GetModelObject(info.Item.Index);
			status.Owner.GetStatusFull().Show();
		}
	}
}
