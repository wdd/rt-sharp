﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using RTSharpIFace;

namespace RTSharp
{
	public partial class f_serverSettings : Form {
		readonly ISDS SDS;
		IEnumerable<ServerSetting> Settings;

		public f_serverSettings(ISDS Sds)
		{
			InitializeComponent();
			SDS = Sds;
		}

		private async void f_serverSettings_Load(object sender, EventArgs e)
		{
			try {
				Settings = await SDS.GetServerSettings();
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.WARN, "Failed to get server settings of " + SDS.UniqueGUID);
				Logger.LogException(LOG_LEVEL.WARN, ex);
				MessageBox.Show("Failed to get server settings:" + Environment.NewLine + ex);
				Close();
				return;
			}
			var settingsEnumer = Settings as ServerSetting[] ?? Settings.ToArray();

			var names = settingsEnumer.Select(x => x.Name);
			var namesEnumer = names as string[] ?? names.ToArray();

			if (namesEnumer.Length != namesEnumer.Distinct().Count()) {
				Logger.Log(LOG_LEVEL.FATAL, "Duplicate setting name!");
				MessageBox.Show("Duplicate setting name exists, please report this to plugin developer", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Close();
				return;
			}

			tv_settings.BeginUpdate();

			Action<string[]> addTreeViewItem = (item) => {
				var nodes = tv_settings.Nodes;

				foreach (var t in item)
					nodes = nodes.Find(t, false).Length > 0 ? nodes.Find(t, false)[0].Nodes : nodes.Add(t, t).Nodes;
			};

			foreach (var setting in settingsEnumer) {
				var newCat = new string[setting.Category.Length + 1];
				Array.Copy(setting.Category, newCat, setting.Category.Length);
				setting.Category[setting.Category.Length - 1] = setting.Name;
				addTreeViewItem(setting.Category);
			}

			tv_settings.EndUpdate();
		}

		private async void b_saveSettings_Click(object sender, EventArgs e)
		{
			SetCurrentSetting();
			try {
				await SDS.SetServerSettings(Settings);

				b_saveSettings.Enabled = false;
				await Task.Delay(1000).ContinueWith(x => Utils.InvokeOpt(() => b_saveSettings.Enabled = true));
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.WARN, "Failed to save settings of " + SDS.UniqueGUID);
				Logger.LogException(LOG_LEVEL.WARN, ex);
				MessageBox.Show("Failed to set server settings:" + Environment.NewLine + ex);
			}
		}

		void SetCurrentSetting()
		{
			if (tv_settings.SelectedNode == null || tv_settings.SelectedNode.Nodes.Count != 0)
				return;

			var setting = Settings.FirstOrDefault(x => x.Name == tv_settings.SelectedNode.Text);
			Debug.Assert(setting != null);

			if (!l_title.Visible)
				return;

			if (num_value.Visible)
				setting.CurrentValue = Convert.ChangeType(num_value.Value, setting.DefaultValue.GetType());
			else if (t_value.Visible)
				setting.CurrentValue = Convert.ChangeType(t_value.Text, setting.DefaultValue.GetType());
			else if (cb_value.Visible) {
				if (setting.DefaultValue is bool)
					setting.CurrentValue = cb_value.SelectedIndex == 0;
				else
					setting.CurrentValue = (int)cb_value.SelectedIndex;
			}
		}

		private void tv_settings_BeforeSelect(object sender, TreeViewCancelEventArgs e)
		{
			SetCurrentSetting();
		}

		private void tv_settings_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (tv_settings.SelectedNode.Nodes.Count != 0) {
				l_title.Visible = l_desc.Visible = l_defaultValue.Visible = t_value.Visible = num_value.Visible = cb_value.Visible = false;
				return;
			}

			l_title.Visible = l_desc.Visible = l_defaultValue.Visible = true;

			var setting = Settings.First(x => x.Name == tv_settings.SelectedNode.Text);

			l_title.Text = setting.Name;
			l_desc.Text = setting.Description;

			var isInt = setting.DefaultValue is int || setting.DefaultValue is uint || setting.DefaultValue is long ||
				setting.DefaultValue is ulong || setting.DefaultValue is short || setting.DefaultValue is ushort ||
				setting.DefaultValue is byte || setting.DefaultValue is sbyte || setting.DefaultValue is float ||
				setting.DefaultValue is double || setting.DefaultValue is decimal;

			cb_value.Visible = setting.DefaultValue is KeyValuePair<string, bool>[] || setting.DefaultValue is bool;
			num_value.Visible = isInt;
			t_value.Visible = !cb_value.Visible && !num_value.Visible;

			if (setting.DefaultValue is KeyValuePair<string, bool>[]) {
				var val = (KeyValuePair<string, bool>[])setting.DefaultValue;
				cb_value.Items.Clear();
				foreach (var v in val) {
					cb_value.Items.Add(v.Key);
					if (v.Value)
						l_defaultValue.Text = "Default value: " + v.Key;
				}

				cb_value.SelectedIndex = (int)setting.CurrentValue;
			} else if (setting.DefaultValue is bool) {
				cb_value.Items.Clear();
				cb_value.Items.Add("True");
				cb_value.Items.Add("False");
				cb_value.SelectedIndex = (bool)setting.CurrentValue ? 0 : 1;

				l_defaultValue.Text = "Default value: " + setting.DefaultValue.ToString();
			} else if (isInt) {
				num_value.Value = setting.CurrentValue;
				l_defaultValue.Text = "Default value: " + setting.DefaultValue.ToString();
			} else {
				t_value.Text = setting.CurrentValue.ToString();
				l_defaultValue.Text = "Default value: " + setting.DefaultValue.ToString();
			}
		}
	}
}
