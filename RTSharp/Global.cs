﻿using System;
using System.Collections.Generic;
using RTSharpIFace;
using System.Drawing;
using BrightIdeasSoftware;

namespace RTSharp
{
	class Global
	{
		public static List<PluginClass> Plugins = new List<PluginClass>();
		public static List<SystemDataSupplierClass> SDSS = new List<SystemDataSupplierClass>();
		public static Dictionary<Tuple<Guid, string>, Func<dynamic, string>> FormatFxs = new Dictionary<Tuple<Guid, string>, Func<dynamic, string>>();
		public static List<TORRENT> Torrents => Program.MainForm.Torrents;
		public static object TorrentsLock => Program.MainForm.TorrentsLock;

		[RTSPluginBridge]
		public static object SelectedTorrentsLock = new object();

		[RTSPluginBridge]
		public static IRTSharpImports RTSharpPluginImports;

		public static Func<float, Tuple<Color?, Color?>> RatioColoring;
		public static Action<float, OLVListSubItem> RatioColoringEx;
	}
}
