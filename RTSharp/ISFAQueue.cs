﻿using RTSharpIFace;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RTSharp
{
	static internal class ISFAQueue
	{
		public class DUP_TARGET
		{
			public SystemDataSupplierClass Target;
			public string RemotePath;
			public bool DuplicateFiles;

			public DUP_TARGET(SystemDataSupplierClass Target, string RemotePath, bool DuplicateFiles)
			{
				this.Target = Target;
				this.RemotePath = RemotePath;
				this.DuplicateFiles = DuplicateFiles;
			}
		}

		private static Dictionary<TORRENT, Tuple<List<DUP_TARGET>, TransferQueue>> Queue = new Dictionary<TORRENT, Tuple<List<DUP_TARGET>, TransferQueue>>();

		public static void HookEvents()
		{
			RTSEvents.TorrentFinished += async (sender, e) => {
				var torrent = (TORRENT)sender;

				if (torrent.ISFAState != TORRENT_ISFA.PROCESSING)
					return;

				// Target force rechecked
				torrent.ISFAState = TORRENT_ISFA.NONE;

				await torrent.Owner.Start(new[] { torrent });
				Program.MainForm.updateTorrents = f_main.UPDATE_TORRENTS.UPDATE;

				var q = Queue.Where(x => x.Key.Hash.SequenceEqual(torrent.Hash)).First();
				var dups = q.Value.Item1;

				dups.RemoveAll(x => x.Target.SDS.UniqueGUID.Equals(torrent.Owner.UniqueGUID));

				if (dups.Count == 0)
					Queue.Remove(q.Key);
			};

			RTSEvents.NewTorrent += async (sender, e) => {
				var q = Queue.Where(x => x.Key.Hash.SequenceEqual(e.Torrent.Hash));

				if (q.Count() == 0 || q.First().Key.Owner.UniqueGUID.Equals(e.Torrent.Owner.UniqueGUID))
					return;

				e.Torrent.ISFAState = TORRENT_ISFA.PROCESSING;

				// Target added
				await e.Torrent.Owner.ForceRecheck(new[] { e.Torrent });
			};
		}

		public static TransferQueue GetQueue(TORRENT In)
		{
			Tuple<List<DUP_TARGET>, TransferQueue> ret;
			if (!Queue.TryGetValue(In, out ret))
				return null;
			return ret.Item2;
		}

		public static void AddQueue(TORRENT In, byte[] InRaw, List<DUP_TARGET> DupTargets)
		{
			Debug.Assert(DupTargets.All(x => x.DuplicateFiles));

			In.ISFAState = TORRENT_ISFA.EXECUTING;
			Program.MainForm.lv_torrents.RefreshObject(In);

			Utils.InvokeOpt(async () => {
				var mup = new TransferQueue();
				Queue.Add(In, Tuple.Create(DupTargets, mup));

				await mup.Multi(
					Enumerable.Repeat(In.Owner, DupTargets.Count()).ToList(),
					DupTargets.Select(x => x.Target.SDS).ToList(),
					Enumerable.Repeat(In.RemotePath, DupTargets.Count()).ToList(),
					DupTargets.Select(x => x.RemotePath).ToList(),
					DupTargets.Select(x => (object)x).ToList(),
					async (p, total, single) => {
						DUP_TARGET dup = (DUP_TARGET)single;

						var token = await dup.Target.SDS.GetISFAToken(dup.RemotePath + (In.FileList.Count > 1 ? In.Name : ""));

						await In.Owner.SendISFA(token, In.RemotePath, p, total).ContinueWith(async x => {
							await dup.Target.SDS.LoadRaw(InRaw, false, dup.RemotePath);
						});
					},
					false
				).ContinueWith(x => {
					In.ISFAState = TORRENT_ISFA.NONE;
					Program.MainForm.lv_torrents.RefreshObject(In);
				});
			});
		}
	}
}
