﻿using MadMilkman.Ini;
using MaxMind.GeoIP2;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace RTSharpIFace
{
	public enum LOG_LEVEL
	{
		DEBUG,
		ERROR,
		FATAL,
		INFO,
		WARN
	}

	/// <summary>
	/// Core
	/// </summary>
	public interface IRTSharp
	{
		/// <summary>
		/// RT# (Core) version
		/// </summary>
		string Version { get; }

		/// <summary>
		/// RT# main form
		/// </summary>
		/// <remarks>Can be used to make direct modifications to core but will break on any core version update</remarks>
		Form MainForm { get; }

		/// <summary>
		/// Shows tray notification / message
		/// </summary>
		/// <param name="Title">Message title</param>
		/// <param name="Message">Message text</param>
		/// <param name="Icon">Message icon</param>
		void ShowTrayMessage(string Title, string Message, ToolTipIcon Icon);

		/// <summary>
		/// New log entry
		/// </summary>
		/// <param name="Level">Log level</param>
		/// <param name="Message">The message</param>
		/// <param name="Source">Function source. Should be empty in most cases</param>
		void Log(LOG_LEVEL Level, string Message, [CallerMemberName]string Source = "Unknown");

		/// <summary>
		/// New exception log entry
		/// </summary>
		/// <param name="Level">Exception level</param>
		/// <param name="Ex">The exception</param>
		/// <param name="Source">Function source. Should be empty in most cases</param>
		void LogException(LOG_LEVEL Level, Exception Ex, [CallerMemberName]string Source = "Unknown");

		/// <summary>
		/// List of system data suppliers
		/// </summary>
		/// <remarks>To search for specific type of SDS, use <see cref="ISDS.GUID"/>. To search for specific instance of loaded SDS, use <see cref="ISDS.UniqueGUID"/></remarks>
		List<ISDS> SDSS { get; }

		/// <summary>
		/// List of plugins
		/// </summary>
		/// <remarks>To search for specific type of plugin, use <see cref="IPlugin.GUID"/>. To search for specific instance of loaded plugin, use <see cref="IPlugin.UniqueGUID"/></remarks>
		List<IPlugin> Plugins { get; }

		/// <summary>
		/// Map of row names to functions which convert raw values field values to strings
		/// </summary>
		/// <remarks><para>Add your format function for your custom torrent listing column here</para><para>First tuple element is UniqueGUID, second is row text</para></remarks>
		Dictionary<Tuple<Guid, string>, Func<dynamic, string>> FormatFxs { get; }

		/// <summary>
		/// All torrents
		/// </summary>
		List<TORRENT> Torrents { get; }

		/// <summary>
		/// MaxMind Country database
		/// </summary>
		DatabaseReader CountryDB { get; }

		/// <summary>
		/// Unique settings file for the plugin
		/// </summary>
		IniFile GetPluginSettings();

		/// <summary>
		/// Save the settings
		/// </summary>
		void SavePluginSettings(IniFile cfg = null);

		/// <summary>
		/// GUI operations
		/// </summary>
		IGUI Gui { get; }

		/// <summary>
		/// Notifies of new torrent
		/// </summary>
		void NewTorrents(IEnumerable<TORRENT> New);

		/// <summary>
		/// Notified of removed torrent
		/// </summary>
		void TorrentsRemoved(IEnumerable<byte[]> Hash, ISDS This);
	}
}
