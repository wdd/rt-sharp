﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RTSharpIFace
{
	[Flags]
	public enum TORRENT_STATE
	{
		NONE = 0,
		PRIVATE = 1 << 0,
		DOWNLOADING = 1 << 1,
		SEEDING = 1 << 2,
		HASHING = 1 << 3,
		PAUSED = 1 << 4,
		STOPPED = 1 << 5,
		COMPLETE = 1 << 6,
		ACTIVE = 1 << 7,
		INACTIVE = 1 << 8,
		ERRORED = 1 << 9,
		ALLOCATING = 1 << 10
	}

	public enum TORRENT_ISFA
	{
		NONE = 0,
		PENDING = 1,
		EXECUTING = 2,
		PROCESSING = 3
	}

	public enum TORRENT_PRIORITY
	{
		/// <summary>
		/// Reserved
		/// </summary>
		NA = -1,
		/// <summary>
		/// High priority
		/// </summary>
		HIGH = 3,
		/// <summary>
		/// Default torrent priorty
		/// </summary>
		NORMAL = 2,
		/// <summary>
		/// Low priority
		/// </summary>
		LOW = 1,
		/// <summary>
		/// No priority
		/// </summary>
		OFF = 0
	}

	/// <summary>
	/// Hash comparer
	/// </summary>
	public class HashComparer : IComparer<byte[]>
	{
		public int Compare(byte[] x, byte[] y)
		{
			return x.SequenceEqual(y) ? 0 : -1;
		}
	}

	public class TORRENT : IEquatable<TORRENT>
	{
		/// <summary>
		/// Torrent info hash, 20 bytes
		/// </summary>
		public byte[] Hash { get; }

		/// <summary>
		/// Parent SDS
		/// </summary>
		public ISDS Owner { get; }

		/// <summary>
		/// Torrent name
		/// </summary>
		public string Name { get; set; }

		TORRENT_STATE _State;

		/// <summary>
		/// Torrent state
		/// </summary>
		public TORRENT_STATE State {
			get { return _State; }
			set {
				if (_State != value && _State != TORRENT_STATE.NONE) {
					_State = value;
					RTSEvents.Dispatcher.TorrentStateChangedEH(this, new RTSEvents.TorrentStateArgs(value));
				} else
					_State = value;
			}
		}

		/// <summary>
		/// Torrent ISFA state
		/// </summary>
		public TORRENT_ISFA ISFAState { get; set; }

		/// <summary>
		/// Size in bytes
		/// </summary>
		public ulong Size { get; set; }

		/// <summary>
		/// Size we want to download (in case of 0 priority files)
		/// </summary>
		public ulong WantedSize { get; set; }

		/// <summary>
		/// Chunk size
		/// </summary>
		public ulong ChunkSize { get; set; }

		/// <summary>
		/// Wasted bytes
		/// </summary>
		public ulong Wasted { get; set; }

		public float _done;
		/// <summary>
		/// Done percentage
		/// </summary>
		public float Done {
			get {
				return _done;
			}
			set {
				if (value == 100 && _done != 100) {
					_done = value;
					RTSEvents.Dispatcher.TorrentFinishedEH(this, new RTSEvents.TorrentFinishedArgs(State));
				} else
					_done = value;
			}
		}

		/// <summary>
		/// Downloaded bytes
		/// </summary>
		public ulong Downloaded { get; set; }

		/// <summary>
		/// Downloaded bytes this SDS sesstion
		/// </summary>
		public ulong DownloadedSession { get; set; }

		/// <summary>
		/// Uploaded bytes
		/// </summary>
		public ulong Uploaded { get; set; }

		/// <summary>
		/// Uploaded bytes this SDS session
		/// </summary>
		public ulong UploadedSession { get; set; }

		/// <summary>
		/// Share ratio
		/// </summary>
		public float Ratio { get; set; }

		/// <summary>
		/// Download speed, B/s
		/// </summary>
		public ulong DLSpeed { get; set; }

		/// <summary>
		/// Download speed history
		/// </summary>
		public Dictionary<DateTime, ulong> DLSpeedHistory { get; }

		/// <summary>
		/// Upload speed, B/S
		/// </summary>
		public ulong UPSpeed { get; set; }

		/// <summary>
		/// Upload speed history
		/// </summary>
		public Dictionary<DateTime, ulong> UPSpeedHistory { get; }

		/// <summary>
		/// ETA, <c>0</c> if already at 100%
		/// </summary>
		/// <seealso cref="Done"/>
		public ulong ETA { get; set; }

		/// <summary>
		/// Torrent label
		/// </summary>
		public string Label { get; set; }

		/// <summary>
		/// Torrent peers. Connected, Total
		/// </summary>
		public Tuple<uint, uint> Peers { get; set; }
		/// <summary>
		/// Torrent seeders. Connected, Total
		/// </summary>
		public Tuple<uint, uint> Seeders { get; set; }

		/// <summary>
		/// Torrent priority
		/// </summary>
		public TORRENT_PRIORITY Priority { get; set; }

		/// <summary>
		/// Unix timestamp of when .torrent file was created
		/// </summary>
		public ulong CreatedOnDate { get; set; }

		/// <summary>
		/// Remaining size to download
		/// </summary>
		/// <seealso cref="Size"/>
		/// <seealso cref="Downloaded"/>
		public ulong RemainingSize { get; set; }

		/// <summary>
		/// Unix timestamp of when torrent finished downloading
		/// </summary>
		public ulong FinishedOnDate { get; set; }

		/// <summary>
		/// Unix timestamp of when torrent was added
		/// </summary>
		public ulong AddedOnDate { get; set; }

		/// <summary>
		/// List of trackers
		/// </summary>
		public List<TRACKER> Trackers { get; set; }

		/// <summary>
		/// Primary tracker URI, not replaced by alias
		/// </summary>
		public Uri TrackerSingle { get; set; }

		/// <summary>
		/// Torrent status message
		/// </summary>
		public string StatusMsg { get; set; }

		/// <summary>
		/// Torrent comment
		/// </summary>
		public string Comment { get; set; }

		/// <summary>
		/// Remote path of torrent data
		/// </summary>
		public string RemotePath { get; set; }

		/// <summary>
		/// Is torrent a magnet link dummy waiting to be resolved?
		/// </summary>
		public bool MagnetDummy { get; set; }

		bool _CurrentlySelectedInUI;

		/// <summary>
		/// Is torrent currently selected in GUI
		/// </summary>
		public bool CurrentlySelectedInUI {
			get {
				lock (PluginBase.RTSharpUIExports.SelectedTorrentsLock)
					return _CurrentlySelectedInUI;
			}
			internal set {
				_CurrentlySelectedInUI = value;
			}
		}

		/// <summary>
		/// Transforms tracker URI to available alias if possible
		/// </summary>
		/// <param name="tracker">Tracker URI</param>
		public static string TrackerTransform(string tracker)
		{
			if (tracker == null)
				return "";

			if (Settings.TrackerMatches == null)
				return tracker;

			var match = Settings.TrackerMatches.FirstOrDefault(x => tracker.Contains(x.Key));
			return match.Value ?? tracker; // TODO: Strip passkey
		}

		/// <summary>
		/// Peer list
		/// </summary>
		public List<PEER> PeerList { get; set; }

		/// <summary>
		/// File list
		/// </summary>
		public List<FILES> FileList { get; set; }

		public override bool Equals(object x)
		{
			if (!(x is TORRENT))
				return false;

			var a = (TORRENT)x;

			if (!a.Hash.SequenceEqual(Hash) || !a.Owner.UniqueGUID.Equals(Owner.UniqueGUID))
				return false;
			return true;
		}

		public TORRENT Copy()
		{
			var cp = new TORRENT(Owner, Hash, DLSpeedHistory, UPSpeedHistory);
			cp.Name = Name;
			cp._State = _State;
			cp.ISFAState = ISFAState;
			cp.Size = Size;
			cp.WantedSize = WantedSize;
			cp.ChunkSize = ChunkSize;
			cp.Wasted = Wasted;
			cp._done = _done;
			cp.Downloaded = Downloaded;
			cp.DownloadedSession = DownloadedSession;
			cp.Uploaded = Uploaded;
			cp.UploadedSession = UploadedSession;
			cp.Ratio = Ratio;
			cp.DLSpeed = DLSpeed;
			cp.UPSpeed = UPSpeed;
			cp.ETA = ETA;
			cp.Label = Label;
			cp.Peers = Peers;
			cp.Seeders = Seeders;
			cp.Priority = Priority;
			cp.CreatedOnDate = CreatedOnDate;
			cp.RemainingSize = RemainingSize;
			cp.FinishedOnDate = FinishedOnDate;
			cp.AddedOnDate = AddedOnDate;
			cp.Trackers = Trackers; ///
			cp.TrackerSingle = TrackerSingle;
			cp.StatusMsg = StatusMsg;
			cp.Comment = Comment;
			cp.RemotePath = RemotePath;
			cp.MagnetDummy = MagnetDummy;
			Trackers?.ForEach(x => cp.Trackers.Add(x.Copy(cp)));
			PeerList?.ForEach(x => cp.PeerList.Add(x.Copy(cp)));
			FileList?.ForEach(x => cp.FileList.Add(x.Copy(cp)));
			return cp;
		}

		public override int GetHashCode() => Hash.GetHashCode() ^ Owner.UniqueGUID.GetHashCode();

		public bool Equals(TORRENT other)
		{
			return Hash.SequenceEqual(other.Hash) && Owner.UniqueGUID.Equals(other.Owner.UniqueGUID);
		}

		public TORRENT(ISDS Owner, byte[] Hash, Dictionary<DateTime, ulong> DLSH = null, Dictionary<DateTime, ulong> UPSH = null)
		{
			this.Owner = Owner;
			this.Hash = Hash;
			if (DLSH == null)
				this.DLSpeedHistory	= new Dictionary<DateTime, ulong>();
			else
				this.DLSpeedHistory = DLSH;
			if (UPSH == null)
				this.UPSpeedHistory = new Dictionary<DateTime, ulong>();
			else
				this.UPSpeedHistory = UPSH;
		}
	}
}
