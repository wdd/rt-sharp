﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTSharpIFace
{
	public partial class TransferQueue : Form
	{
		public class TRANSFER
		{
			public Task Task;
			public ISDS Source;
			public ISDS Target;
			public string SourcePath;
			public string TargetPath;
			public Stopwatch SW;
			public Stopwatch SWAll;
			public ulong LastBytes;
			public ulong Bytes;
			public ulong Total;
			public ulong Speed;
			public ListViewItem.ListViewSubItem ProgressItem;

			public TRANSFER(ISDS Source, ISDS Target, string SourcePath, string TargetPath)
			{
				this.Source = Source;
				this.Target = Target;
				this.SourcePath = SourcePath;
				this.TargetPath = TargetPath;
				Bytes = 0;
				Total = 0;
				SW = new Stopwatch();
				SWAll = new Stopwatch();
			}

			public ulong CalculateSpeed()
			{
				return (ulong)(((double)Bytes - LastBytes) / SW.Elapsed.TotalSeconds);
			}
		}

		List<TRANSFER> Downloads = new List<TRANSFER>();

		public TransferQueue()
		{
			InitializeComponent();
		}

		public async Task Single(
			ISDS Source,
			ISDS Target,
			string SourcePath,
			string TargetPath,
			object SinglePass,
			Func<IProgress<ulong>, IProgress<ulong>, object, Task> Fx,
			bool ShowForm = true)
		{
			await Multi(
				Source == null ? null : new[] { Source }.ToList(),
				Target == null ? null : new[] { Target }.ToList(),
				new[] { SourcePath }.ToList(),
				new[] { TargetPath }.ToList(),
				new[] { SinglePass }.ToList(),
				Fx,
				ShowForm);
		}

		/// <summary>
		/// Queue multiple path transfers
		/// </summary>
		/// <param name="Source"></param>
		/// <param name="Target"></param>
		/// <param name="SourcePath"></param>
		/// <param name="TargetPath"></param>
		/// <param name="Fx">Progress, total, SinglePass</param>
		/// <returns></returns>
		public async Task Multi(
			List<ISDS> Source,
			List<ISDS> Target,
			List<string> SourcePath,
			List<string> TargetPath,
			List<object> SinglePass,
			Func<IProgress<ulong>, IProgress<ulong>, object, Task> Fx,
			bool ShowForm = true)
		{
			if (Source != null)
				Debug.Assert(Source.Count == SourcePath.Count);
			if (Target != null)
				Debug.Assert(Target.Count == SourcePath.Count);

			Debug.Assert(SourcePath.Count == TargetPath.Count);

			if (ShowForm)
				Show();

			for (int x = 0; x < SourcePath.Count; x++)
				Downloads.Add(new TRANSFER(Source != null ? Source[x] : null, Target != null ? Target[x] : null, SourcePath[x], TargetPath[x]));

			for (int x = 0; x < Downloads.Count; x++) {
				var d = Downloads[x];

				ListViewItem lvi = new ListViewItem(d.Source == null ? "Local machine" : d.Source.FriendlyName);
				ProgressBar pb = new ProgressBar();

				lvi.SubItems.Add(d.Target == null ? "Local machine" : d.Target.FriendlyName);
				lvi.SubItems.Add(d.SourcePath);
				lvi.SubItems.Add(d.TargetPath);
				ListViewItem.ListViewSubItem lvsi = new ListViewItem.ListViewSubItem();
				lvi.SubItems.Add(lvsi);
				lv_downloads.Items.Add(lvi);
				d.ProgressItem = lvsi;
			}

			IProgress<Tuple<ListViewItem.ListViewSubItem, string>> pit = new Progress<Tuple<ListViewItem.ListViewSubItem, string>>((p) =>
			p.Item1.Text = p.Item2
			);

			await Task.Run(async () => {
				for (int x = 0; x < Downloads.Count; x++) {
					var d = Downloads[x];

					while (Downloads.Sum(i => i.Task != null ? 1 : 0) >= Environment.ProcessorCount)
						await Task.WhenAny(Downloads.Select(i => i.Task).Where(i => i != null));

					Debug.WriteLine(Downloads.Sum(i => i.Task != null ? 1 : 0));

					d.SW.Start();
					d.SWAll.Start();

					d.Task = Fx(new Progress<ulong>((bytes) => {
						d.Bytes = bytes;
						if (d.SW.Elapsed.TotalMilliseconds < 1000 && bytes != d.Total)
							return;

						if (d.Bytes == d.Total) {
							Debug.WriteLine("Upload complete: " + d.TargetPath);
							pit.Report(Tuple.Create(d.ProgressItem, "100% - " + Utils.GetSizeSI(d.Total) + " (" + Utils.GetSizeSI((ulong)((float)d.Total / d.SWAll.Elapsed.TotalSeconds)) + "/s avg.)"));
							d.Bytes = d.Total;
							d.Task = null;
							d.SW.Stop();
							d.SWAll.Stop();
						} else {
							d.Speed = d.CalculateSpeed();
							pit.Report(Tuple.Create(d.ProgressItem, Math.Round((float)d.Bytes / d.Total * 100, 2) + "% - " + Utils.GetSizeSI(d.Bytes) + " / " + Utils.GetSizeSI(d.Total) + " (" + Utils.GetSizeSI(d.Speed) + "/s)"));
							Debug.WriteLine(Math.Round((float)d.Bytes / d.Total * 100, 2) + "% - " + Utils.GetSizeSI(d.Bytes) + " / " + Utils.GetSizeSI(d.Total) + " (" + Utils.GetSizeSI(d.Speed) + "/s)");
							d.LastBytes = bytes;
							d.SW.Restart();
						}
					}), new Progress<ulong>((total) => {
						d.Total = total;
					}),
					SinglePass[x]);
				}

				var t = Downloads.Select(i => i.Task).Where(i => i != null);
				if (t.Count() != 0)
					await Task.WhenAll(t);
			});

			var a = 2;
		}

		private void tmr_globalSpeed_Tick(object sender, EventArgs e)
		{
			ulong speed = 0;
			foreach (var d in Downloads.Where(x => x.Bytes != x.Total))
				speed += d.Speed;
			
			l_dlSpeed.Text = "DL Speed: " + Utils.GetSizeSI(speed) + "/s";
		}

		private void TransferQueue_FormClosing(object sender, FormClosingEventArgs e)
		{
			Hide();
			e.Cancel = true;
		}
	}
}
