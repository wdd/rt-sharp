﻿using RTSharpIFace;
using static RTSharp.Global;
using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace RTSharp
{
	public partial class f_transferData : Form
	{
		ISDS CurSDS;

		public f_transferData()
		{
			InitializeComponent();
		}

		private async void b_transfer_Click(object sender, EventArgs e)
		{
			if (CurSDS == null) {
				MessageBox.Show("Please select a connection", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			/*var dbox = new DownloadBox();
			dbox.SetCloseOnCompletion(false);
			dbox.Show(0);
			ulong lastBytes = 0;*/
			
			/*IProgress<Tuple<ulong, string>> pData = new Progress<Tuple<ulong, string>>((inp) => dbox.UpdateData(inp.Item1, inp.Item2));
			IProgress<ulong> pProgress = new Progress<ulong>((inp) => dbox.UpdateProgress(inp));

			Action<string, ulong, ulong> inv = (msg, bytes, total) => {
				if (msg != null)
					pData.Report(Tuple.Create(total, msg));
				else {
					pProgress.Report(bytes - lastBytes);
					dbox.UpdateProgress(bytes - lastBytes);
					lastBytes = bytes;
				}
			};*/

			try {
				var dest = t_remoteDest.Text;
				if (!dest.EndsWith("/"))
					dest = dest + "/";

				if (rb_folder.Checked) {
					var files = RTSharpIFace.Utils.GetFilesWildcard(Path.Combine(t_folder.Text, "*"));

					List<Task> wT = new List<Task>();

					var mup = new TransferQueue();
					await mup.Multi(
						null,
						Enumerable.Repeat(CurSDS, files.Length).ToList(),
						files.ToList(),
						files.Select(x => dest + new DirectoryInfo(Path.GetDirectoryName(x)).Name + "/" + x.Replace(t_folder.Text, "").Replace('\\', '/')).ToList(),
						files.Select(x => (object)x).ToList(),
						async (p, total, single) => {
							var file = (string)single;
							await CurSDS.UploadFile(file, dest + new DirectoryInfo(Path.GetDirectoryName(file)).Name + "/" + file.Replace(t_folder.Text, "").Replace('\\', '/'), p, total);
						}
					);
				} else
					await CurSDS.UploadFile(t_file.Text, t_remoteDest.Text, null, null);
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.WARN, "Failed to transfer torrent data to server of " + CurSDS.UniqueGUID);
				Logger.LogException(LOG_LEVEL.WARN, ex);
				MessageBox.Show("Failed to transfer torrent data:" + Environment.NewLine + ex, "RT#", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private async void cmb_sdss_SelectedIndexChanged(object sender, EventArgs e)
		{
			foreach (var t in SDSS) {
				if ((string)cmb_sdss.SelectedItem != t.SDS.FriendlyName + " (" + t.SDS.UniqueGUID + ")")
					continue;

				CurSDS = t.SDS;

				var @default = "";
				try {
					@default = await CurSDS.GetDefaultSavePath();
				} catch {
					// ignored
				}

				t_remoteDest.Text = @default;

				break;
			}
		}

		private void f_transferData_Load(object sender, EventArgs e)
		{
			if (SDSS.Count == 0) {
				Logger.Log(LOG_LEVEL.INFO, "No SDSs");
				MessageBox.Show("No connections", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}

			cmb_sdss.Items.Clear();
			foreach (var sds in SDSS) {
				var ops = sds.SDS.GetSupportedOperations();
				if (ops.files.UploadFile)
					cmb_sdss.Items.Add(sds.SDS.FriendlyName + " (" + sds.SDS.UniqueGUID + ")");
			}

			CurSDS = null;
		}

		private void g_folder_MouseClick(object sender, EventArgs e) => rb_folder.Checked = true;

		private void g_file_MouseClick(object sender, EventArgs e) => rb_file.Checked = true;

		private void t_folder_MouseClick(object sender, MouseEventArgs e) => rb_folder.Checked = true;

		private void t_file_MouseClick(object sender, MouseEventArgs e) => rb_file.Checked = true;

		private void b_browse_folder_Click(object sender, EventArgs e)
		{
			rb_folder.Checked = true;
			var fbd = new FolderBrowserDialog();
			fbd.ShowDialog();
			t_folder.Text = fbd.SelectedPath;
		}

		private void b_browser_file_Click(object sender, EventArgs e)
		{
			rb_file.Checked = true;
			var ofd = new OpenFileDialog {
				Filter = "All Files (*.*)|*.*"
			};
			if (ofd.ShowDialog() == DialogResult.OK)
				t_file.Text = ofd.FileName;
		}

		private void b_remoteBrowse_Click(object sender, EventArgs e)
		{
			var res = new SelectRemoteFolder(CurSDS).ShowDialogWResult();
			if (res != "")
				t_remoteDest.Text = res;
		}
	}
}
