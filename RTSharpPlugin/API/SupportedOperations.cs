﻿using System;
using System.Collections.Generic;

namespace RTSharpIFace
{
	/// <summary>
	/// Supported operations
	/// </summary>
	public class SupportedOperations
	{

		/// <summary>
		/// Operations associated with files
		/// </summary>
		public class Files
		{

			/// <summary>
			/// <see cref="ISDS.DownloadFile(FILE, string, IProgress{ulong}, IProgress{ulong})"/>
			/// </summary>
			public bool DownloadFile;
			/// <summary>
			/// <see cref="ISDS.FileMediaInfo(IEnumerable{FILE})"/>
			/// </summary>
			public bool MediaInfo;
			/// <summary>
			/// <see cref="ISDS.SetFilePriority(IEnumerable{FILE}, FILE.PRIORITY)"/>
			/// </summary>
			public bool SetPriority;
			/// <summary>
			/// <see cref="ISDS.SetFileDownloadStrategy(IEnumerable{FILE}, FILE.DOWNLOAD_STRATEGY)"/>
			/// </summary>
			public bool SetDownloadStrategy;
			/// <summary>
			/// <see cref="FILE.DownloadStrategy"/>
			/// </summary>
			public bool GetDownloadStrategy;
			/// <summary>
			/// <see cref="ISDS.UploadFile(string, string, IProgress{ulong}, IProgress{ulong})"/>
			/// </summary>
			public bool UploadFile;
			/// <summary>
			/// <see cref="ISDS.GetRemoteDirectories(string)"/>
			/// </summary>
			public bool GetRemoteDirectories;
			/// <summary>
			/// <see cref="ISDS.CreateRemoteDirectory(string)"/> 
			/// </summary>
			public bool CreateRemoteDirectory;
			/// <summary>
			/// <see cref="ISDS.SendISFA(RTSharpIFace.ISFA.TOKEN, string, IProgress{ulong}, IProgress{ulong})"/>
			/// <see cref="ISDS.GetISFAToken(string)"/>
			/// </summary>
			public bool ISFA;
		}

		/// <summary>
		/// Operations associated with torrents
		/// </summary>
		public class Torrents
		{
			/// <summary>
			/// <see cref="ISDS.GetTorrentFiles(List{TORRENT})"/>
			/// </summary>
			public bool GetTorrentFile;
			/// <summary>
			/// <see cref="ISDS.SetPriority(IEnumerable{TORRENT}, TORRENT_PRIORITY)"/>, <see cref="TORRENT.Priority"/>
			/// </summary>
			public bool Priority;
			/// <summary>
			/// <see cref="ISDS.EditTorrent(IEnumerable{Tuple{TORRENT, IEnumerable{string}}})"/>
			/// </summary>
			public bool EditTorrent;
			/// <summary>
			/// Reserved. Must be set to <c>False</c>
			/// </summary>
			public bool FastResume;
			/// <summary>
			/// <see cref="ISDS.ReannounceAll(IEnumerable{TORRENT})"/> 
			/// </summary>
			public bool ReannounceAll;
			/// <summary>
			/// Implements <see cref="TORRENT.Wasted"/> field
			/// </summary>
			public bool Wasted;
			/// <summary>
			/// Implements <see cref="TORRENT.DownloadedSession"/> and <see cref="TORRENT.UploadedSession"/>
			/// </summary>
			public bool SessionDownloadUpload;
			/// <summary>
			/// <see cref="TORRENT_STATE.STOPPED"/>, <see cref="ISDS.Stop(IEnumerable{TORRENT})"/>
			/// </summary>
			public bool StopTorrent;
		}

		/// <summary>
		/// Operations associated with peers
		/// </summary>
		public class Peers
		{
			/// <summary>
			/// <see cref="ISDS.KickPeer(IEnumerable{PEER})"/>
			/// </summary>
			public bool Kick;
			/// <summary>
			/// <see cref="ISDS.BanPeer(IEnumerable{PEER})"/>
			/// </summary>
			public bool Ban;
			/// <summary>
			/// <see cref="ISDS.TogglePeerSnub(IEnumerable{PEER}, bool)"/>
			/// </summary>
			public bool SnubUnsnub;
		}

		/// <summary>
		/// Operations associated with peers
		/// </summary>
		public class Trackers
		{
			/// <summary>
			/// <see cref="ISDS.ToggleTracker(IEnumerable{TRACKER}, bool)"/>
			/// </summary>
			public bool EnableDisable;
			/// <summary>
			/// <see cref="ISDS.Reannounce(IEnumerable{TRACKER})"/> 
			/// </summary>
			public bool Reannounce;
			/// <summary>
			/// <see cref="TRACKER.Seeders"/>, <see cref="TRACKER.Peers"/>
			/// </summary>
			public bool SeedersPeersCount;
			/// <summary>
			/// <see cref="TRACKER.LastUpdated"/>, <see cref="TRACKER.Interval"/>
			/// </summary>
			public bool Update;
		}

		public Torrents torrents;
		public Files files;
		public Peers peers;
		public Trackers trackers;
		public SupportedOperations()
		{
			torrents = new Torrents();
			files = new Files();
			peers = new Peers();
			trackers = new Trackers();
		}
	}
}
