﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace RTSharpIFace
{
	[AttributeUsage(AttributeTargets.All)]
	public class RTSPluginBridge : Attribute { }

	[RTSPluginBridge]
	public interface IRTSharpUIExports
	{
		object SelectedTorrentsLock { get; }

		SynchronizationContext SyncCtxSTA { get; }

		void Log(LOG_LEVEL Level, string Message);
		void LogException(LOG_LEVEL Level, Exception Ex);
	}

	[RTSPluginBridge]
	public interface IRTSharpImports
	{
		void SetTorrentUISelect(IEnumerable<TORRENT> Torrents, bool val);

		void EvNewTorrentAdded(TORRENT In);

		void EvTorrentRemoved(ISDS SDS, byte[] Hash);

		void EvMagnetResolved(TORRENT In);
	}

	[RTSPluginBridge]
	class RTSharpImports : IRTSharpImports
	{
		public void SetTorrentUISelect(IEnumerable<TORRENT> Torrents, bool val)
		{
			foreach (var t in Torrents)
				t.CurrentlySelectedInUI = val;
		}

		public void EvNewTorrentAdded(TORRENT In) => RTSEvents.Dispatcher.NewTorrentAddedEH(In.Owner, new RTSEvents.NewTorrentArgs(In));
		public void EvTorrentRemoved(ISDS SDS, byte[] Hash) => RTSEvents.Dispatcher.TorrentRemovedEH(SDS, new RTSEvents.TorrentRemovedArgs(Hash));
		public void EvMagnetResolved(TORRENT In) => RTSEvents.Dispatcher.MagnetResolvedEH(new RTSEvents.MagnetResolvedArgs(In));
	}

	public class PluginBase
	{
		[RTSPluginBridge]
		public static IRTSharpUIExports RTSharpUIExports;

		public void Init(Assembly RTSharp)
		{
			bool initFound = false;
			foreach (Type t in RTSharp.GetTypes()) {
				if (t.GetInterface("IRTSharpUIExports") != null) {
					RTSharpUIExports = Activator.CreateInstance(t) as IRTSharpUIExports;
					initFound = true;
				}
			}

			Debug.Assert(initFound);
		}
	}
}
