﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RTSharpIFace
{
	public class FILES
	{
		/// <summary>
		/// Parent <see cref="ArrayIndex"/> of <see cref="FILES"/>.
		/// </summary>
		public int Parent { get; set; }

		/// <summary>
		/// Actual file
		/// </summary>
		public FILE Item { get; set; }

		/// <summary>
		/// Unique <see cref="FILES"/> ID. -1 for root element.
		/// </summary>
		public int ArrayIndex { get; set; }

		/// <summary>
		/// Children array of <see cref="ArrayIndex"/>
		/// </summary>
		public List<int> Children { get; set; }

		public FILES(int parent, FILE item, params int[] children)
		{
			Parent = parent;
			Item = item;
			Children = children[0] == -1 ? new List<int>() : children.ToList();
		}

		public FILES(int parent, FILE item, List<int> children)
		{
			Parent = parent;
			Item = item;
			Children = children;
		}

		public FILES Copy(TORRENT Parent)
		{
			var cp = new FILES();
			cp.Parent = this.Parent;
			cp.ArrayIndex = this.ArrayIndex;
			cp.Children = this.Children.ToList();
			cp.Item = this.Item.Copy(Parent);
			return cp;
		}

		public FILES() { }
	}

	public class FILE
	{
		public enum PRIORITY
		{
			/// <summary>
			/// Reserved.
			/// </summary>
			INTERNAL_NOT_SET = 4,
			/// <summary>
			/// Reserved.
			/// </summary>
			NA = 3,
			/// <summary>
			/// High priority
			/// </summary>
			HIGH = 2,
			/// <summary>
			/// Default priority
			/// </summary>
			NORMAL = 1,
			/// <summary>
			/// Don't download
			/// </summary>
			DONT_DOWNLOAD = 0
		}

		public enum DOWNLOAD_STRATEGY
		{
			/// <summary>
			/// Default
			/// </summary>
			NORMAL = 0,
			/// <summary>
			/// Download leading chunk first
			/// </summary>
			LEADING_CHUNK_FIRST = 1,
			/// <summary>
			/// Download trailing chunk first
			/// </summary>
			TRAILING_CHUCK_FIRST = 2,
			/// <summary>
			/// Reserved.
			/// </summary>
			NA = 3,
			/// <summary>
			/// Reserved.
			/// </summary>
			INTERNAL_NOT_SET = 4
		}

		/// <summary>
		/// Parent torrent
		/// </summary>
		public TORRENT Parent { get; set; }

		/// <summary>
		/// <see cref="ISDS.UniqueGUID"/>
		/// </summary>
		public Guid OwnerUniqGUID { get; }

		/// <summary>
		/// File ID internal to SDS. Must be serializable
		/// </summary>
		public object ID { get; }

		/// <summary>
		/// File name
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Size in bytes
		/// </summary>
		public ulong Size { get; set; }

		/// <summary>
		/// Downloaded chunks
		/// </summary>
		public ulong DownloadedChunks { get; set; }

		/// <summary>
		/// Percent done
		/// </summary>
		public float Done {
			get {
				var done = (float)Downloaded / Size * 100;
				return done > 100 ? 100 : done;
			}
		}

		/// <summary>
		/// Downloaded size in bytes
		/// </summary>
		public ulong Downloaded {
			get {
				var dl = DownloadedChunks * Parent.ChunkSize;
				return dl > Size ? Size : dl;
			}
		}

		/// <summary>
		/// Priority
		/// </summary>
		public PRIORITY Priority { get; set; }

		/// <summary>
		/// Download strategy
		/// </summary>
		public DOWNLOAD_STRATEGY DownloadStrategy { get; set; }

		/// <summary>
		/// Is a directory?
		/// </summary>
		public bool Directory { get; set; }

		public FILE(string DirectoryName, TORRENT Parent, Guid OwnerUniqGUID, object ID)
		{
			this.ID = ID;
			Directory = true;
			Name = DirectoryName;
			this.Parent = Parent;
			this.OwnerUniqGUID = OwnerUniqGUID;
		}

		public FILE(TORRENT Parent, Guid OwnerUniqGUID, object ID)
		{
			this.ID = ID;
			this.Parent = Parent;
			this.OwnerUniqGUID = OwnerUniqGUID;
		}

		public FILE Copy(TORRENT Parent)
		{
			var cp = new FILE(Parent, OwnerUniqGUID, ID);
			cp.Name = Name;
			cp.Size = Size;
			cp.DownloadedChunks = DownloadedChunks;
			cp.Priority = Priority;
			cp.DownloadStrategy = DownloadStrategy;
			cp.Directory = Directory;
			return cp;
		}
	}
}
