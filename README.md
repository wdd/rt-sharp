# RT# (alpha, all releases untested)
## Latest binaries: [Tag v0.5.0](https://gitlab.com/wdd/rt-sharp/tags/v0.5.0)

[![build status](https://gitlab.com/wdd/rt-sharp/badges/master/build.svg)](https://gitlab.com/wdd/rt-sharp/commits/master)

RT# is a desktop interface for torrent clients and it is written in C#. It relies on different [plugins](#plugins) that provide data, so RT# can unify more than one torrent client or daemon into one interface.

## Configuration
An up-to-date default configuration is included in the repository with comments. Each plugin (whether loaded multiple times or not) has it's own unique configuration file, so it may ask to edit the configuration file or will prompt a configuration form upon loading it first time.
### Loading plugins
When adding a new plugin, you must add it to `plugins.ini` file in following format:
```
ID [Plugin file name without extension]
```
In order to load same plugin many times (in case of different torrent daemon onfigurations, but same client), add same line multiple times. Upon starting RT#, it will reploace `ID` with an unique plugin ID.

## Plugins <a name="plugins"></a>
Each plugin can act either as a System Data Supplier (SDS) or a regular plugin.

Current list of System Data Suppliers:
* [RTSharp-rtorrent-SDS (rtorrent)](https://gitlab.com/wdd/rtsharp-rtorrent-sds)
* RTSharp-deluge-SDS (Deluge) (WIP)

### System Data Supplier <a name="SDS"></a>
A SDS must implement the `ISDS` interface where RT# communicates with target server. On how each method works and how to implement it, see [SDSs](https://gitlab.com/wdd/rt-sharp/blob/master/SDS.md). A SDS must implement the regular plugin `IPlugin` interface too.
### Plugin
A regular plugin must implement the `IPlugin` interface and it provides no torrent listings, but rather modifies default behavior of RT# such as highlighting torrent listing fields or adding one of its own.