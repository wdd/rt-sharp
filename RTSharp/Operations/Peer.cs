﻿using BrightIdeasSoftware;
using MaxMind.GeoIP2.Responses;
using NetTools;
using RTSharpIFace;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Whois.NET;
using static RTSharp.Utils;
using static RTSharp.Global;

namespace RTSharp
{
	internal class Peer
	{
		public static object CompanyCacheLock = new object();

		static readonly ToolTip companyToolTip = new ToolTip();

		public class COMPANY_CACHE
		{
			public IPAddressRange Range;
			public string Domain;
			public string Company;
			public string ImageKey;

			public COMPANY_CACHE(IPAddressRange range, string domain, string company, string imageKey)
			{
				Range = range;
				Domain = domain;
				Company = company;
				ImageKey = imageKey;
			}

			public COMPANY_CACHE(IPAddress PeerIP)
			{
				Range = new IPAddressRange { End = PeerIP };
			}
		}

		static readonly List<COMPANY_CACHE> CompanyCache = new List<COMPANY_CACHE>();

		public static void InitCompanyToolTip()
		{
			companyToolTip.AutoPopDelay = 0;
			companyToolTip.InitialDelay = 0;
			companyToolTip.ReshowDelay = 0;
			companyToolTip.AutomaticDelay = 0;
			companyToolTip.UseFading = false;
			companyToolTip.UseAnimation = false;
		}

		public static void LoadCompanyCache(WaitingBox wait)
		{
			int progress = 0;
			FileStream h;
			try {
				h = System.IO.File.Open("cache/company.dat", FileMode.OpenOrCreate);
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.FATAL, "FAILED TO OPEN OR CREATE cache/company.dat");
				Logger.LogException(LOG_LEVEL.FATAL, ex);
				return;
			}
			using (var companyCache = new StreamReader(h)) {
				string line;
				while ((line = companyCache.ReadLine()) != null) {
					try {
						var sp = line.Split(new[] { '\x01' }, 5);

						var begin = new IPAddress(Convert.FromBase64String(sp[3]));
						var end = new IPAddress(Convert.FromBase64String(sp[4]));
						var cc = new COMPANY_CACHE(new IPAddressRange(begin, end), sp[0] == "" ? "Unknown" : sp[0], sp[1], sp[2]);

						Program.MainForm.CompanyImgs.Images.Add(sp[2], Image.FromFile("cache/company/" + sp[2]));

						// ReSharper disable once InconsistentlySynchronizedField
						CompanyCache.Add(cc);

						if (progress % 100 == 0) {
							wait.UpdateText(cc.Range.ToCidrString());
							wait.UpdatePBarSingle((byte)((double)companyCache.BaseStream.Position / companyCache.BaseStream.Length * 100));
							Application.DoEvents();
						}

						progress++;
					} catch (Exception ex) {
						Logger.Log(LOG_LEVEL.ERROR, "Failed to parse company cache entry");
						Logger.Log(LOG_LEVEL.ERROR, "Line: " + line);
						Logger.LogException(LOG_LEVEL.ERROR, ex);
					}
				}
			}
		}

		static async void PeerWhoIsIcon(PEER Peer)
		{
			/*
			 * Peer IP range is not found in cache.
			 * Initially:
			 *		Peer image is flag
			 *		Peer company is country name
			 */
			WhoisResponse res;
			try {
				res = await WhoisClient.QueryAsync(Peer.IP.ToString());
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.WARN, "WhoIs failed for " + Peer.IP);
				Logger.LogException(LOG_LEVEL.DEBUG, ex);
				return;
			}

			if (res == null) {
				Logger.Log(LOG_LEVEL.WARN, "WhoIs failed for " + Peer.IP + " (2)");
				return;
			}

			try {
				try {
					var raw = res.Raw;
					Peer.Company = res.OrganizationName;

					var rDomain = new Regex(@"@(([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6})");
					var mDomain = rDomain.Match(raw);

					// Get domain and IP range
					if (mDomain.Groups[0].Captures.Count != 0) {
						Peer.Domain = mDomain.Groups[0].Captures[0].Value.Remove(0, 1);

						// Domain replacements
						if (Settings.DomainReplacements.ContainsKey(Peer.Domain))
							Peer.Domain = Settings.DomainReplacements[Peer.Domain];

						Peer.Company += " [" + Peer.Domain + "]";
					} else
						Peer.Domain = "Unknown";

					if (res.AddressRange != null) {
						res.AddressRange.Begin = res.AddressRange.Begin;
						res.AddressRange.End = res.AddressRange.End;

						if (!res.AddressRange.Contains(Peer.IP)) {
							// HACKHACK
							// Some addresses returned by WhoIs are like "181.168/14", fix this.
							
							foreach (var line in raw.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None)) {
								if (line.StartsWith("inetnum") || line.StartsWith("NetRange") || line.StartsWith("CIDR")) {
									var m1 = new Regex(@"(?<adr>[\da-f\.:]+)/(?<mask>\d+)").Match(line);
									if (m1.Success) {
										var mask = int.Parse(m1.Groups["mask"].Value);
										res.AddressRange = IPAddressRange.Parse(Peer.IP + "/" + mask);
									}
								}
							}
						}
					} else
						res.AddressRange = new IPAddressRange(Peer.IP);

					lock (CompanyCacheLock) {
						COMPANY_CACHE cc = null;

						foreach (var val in CompanyCache) {
							if (val.Domain == Peer.Domain && val.Domain != "Unknown") {
								cc = val;
								break;
							}
						}

						// If domain exists in cache
						if (cc != null) {
							Peer.Company = cc.Company;
							Peer.ImageKey = cc.ImageKey;

							// Add new cache entry with different IP range
							CompanyCache.Add(new COMPANY_CACHE(res.AddressRange, cc.Domain, cc.Company, cc.ImageKey));

							// Done
							return;
						} else
							CompanyCache.Add(new COMPANY_CACHE(res.AddressRange, Peer.Domain, Peer.Company, "qq"));
					}
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to parse WhoIs");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					return;
				}

				if (Peer.Domain == "Unknown")
					return;

				// Download logo
				byte[] img;
				try {
					img = await Favicons.DownloadFavicon(Peer.Domain);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.WARN, "Failed to download company icon of " + Peer.Domain);
					Logger.LogException(LOG_LEVEL.DEBUG, ex);
					img = null;
				}

				// Associate image with ImageKey
				if (img != null) {
					lock (CompanyCacheLock) {
						COMPANY_CACHE cc = null;

						foreach (var cache in CompanyCache) {
							if (cache.Range.Contains(Peer.IP)) {
								cc = cache;
							}
						}

						string imgKey;
						do {
							imgKey = RandomString(32);
						} while (Program.MainForm.CompanyImgs.Images.ContainsKey(imgKey));
						Peer.ImageKey = imgKey;

						Debug.Assert(cc != null);
						cc.ImageKey = imgKey;

						Logger.Log(LOG_LEVEL.DEBUG, "New image key for " + Peer.Domain);

						Program.MainForm.CompanyImgs.Images.Add(Peer.ImageKey, new Bitmap(Image.FromStream(new MemoryStream(img)), 16, 16));
					}
				}
			} finally {
				if (Peer.Parent.CurrentlySelectedInUI)
					Program.MainForm.lv_peers.UpdateObject(Peer);
			}
		}

		public static void PeerUpdate(PEER p, PEER repl)
		{
			p.Country = repl.Country ?? p.Country;
			p.Domain = repl.Domain ?? p.Domain;
			p.Company = repl.Company ?? p.Company;
			p.ImageKey = repl.ImageKey ?? p.ImageKey;
			p.Downloaded = repl.Downloaded;
			p.Uploaded = repl.Uploaded;
			p.DLSpeed = repl.DLSpeed;
			p.UPSpeed = repl.UPSpeed;
			p.Done = repl.Done;
		}

		public static async Task FetchPeers(TORRENT torrent)
		{
			if (!(torrent.State.HasFlag(TORRENT_STATE.ACTIVE) || torrent.State.HasFlag(TORRENT_STATE.DOWNLOADING))) {
				torrent.PeerList = null;

				if (torrent.CurrentlySelectedInUI)
					Program.MainForm.lv_peers.SetObjects(torrent.PeerList);

				return;
			}

			var sds = torrent.Owner;
			List<PEER> peers;
			try {
				peers = await sds.GetPeers(torrent);
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.ERROR, "Failed to get peers for " + torrent.Name);
				Logger.LogException(LOG_LEVEL.ERROR, ex);
				return;
			}

			if (torrent.PeerList == null)
				torrent.PeerList = new List<PEER>();

			/*
			 * 
			 */

			var removed = new List<PEER>();

			foreach (var p in torrent.PeerList) {
				if (peers.FirstOrDefault(x => x.Equals(p)) == null)
					removed.Add(p);
			}

			if (removed.Count != 0) {
				removed.ForEach(x => torrent.PeerList.Remove(x));
				if (torrent.CurrentlySelectedInUI)
					InvokeOpt(() => Program.MainForm.lv_peers.RemoveObjects(removed));
			}

			/*
			 * 
			 */

			if (torrent.CurrentlySelectedInUI)
				InvokeOpt(() => Program.MainForm.lv_peers.BeginUpdate());

			foreach (var p in peers) {
				var found = torrent.PeerList.FirstOrDefault(x => x.Equals(p));

				if (found != null) {
					PeerUpdate(found, p);
					Program.MainForm.lv_peers.UpdateObject(found);
					continue;
				}

				COMPANY_CACHE cache = null;

				lock (CompanyCacheLock) {
					foreach (var cc in CompanyCache) {
						if (cc.Range.Contains(p.IP)) {
							cache = cc;
							
							break;
						}
					}
				}

				if (cache != null) {
					// Company found in cache

					p.Domain = cache.Domain;
					p.Company = cache.Company;
					p.ImageKey = cache.ImageKey;

					torrent.PeerList.Add(p);
					if (torrent.CurrentlySelectedInUI)
						Program.MainForm.lv_peers.AddObject(p);

					continue;
				}

				// Fetch company info
				CountryResponse response = null;
				try {
					response = IniSettings.CountryDB.Country(p.IP);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.WARN, "Peer country DB failed for " + p.IP);
					Logger.LogException(LOG_LEVEL.DEBUG, ex);
				}

				if (response != null) {
					p.Country = response.Country.IsoCode;
					p.ImageKey = response.Country.IsoCode;
					p.Company = response.Country.Name;
				} else {
					p.Country = "??";
					p.ImageKey = "qq";
					p.Company = "N/A";
				}

				torrent.PeerList.Add(p);
				if (torrent.CurrentlySelectedInUI)
					Program.MainForm.lv_peers.AddObject(p);

				if (response != null) {
					var task = new Task(() => PeerWhoIsIcon(p));
					task.Start();
				}
			}

			if (torrent.CurrentlySelectedInUI)
				InvokeOpt(() => Program.MainForm.lv_peers.EndUpdate());
		}

		public static void FetchPeersForSelectedTorrent()
		{
			var sel = Torrent.GetSelectedTorrents();

			var tEnumer = sel as TORRENT[] ?? sel.ToArray();
			if (tEnumer.Length != 1)
				return;

			var torrent = tEnumer.First();

			if (torrent.State.HasFlag(TORRENT_STATE.ACTIVE) || torrent.State.HasFlag(TORRENT_STATE.DOWNLOADING)) {
				Task.Run(() => {
					FetchPeers(torrent).ContinueWith(x => {
						Logger.Log(LOG_LEVEL.ERROR, "Fetch peers failed for torrent " + torrent.Name);
						Logger.LogException(LOG_LEVEL.ERROR, x.Exception);
					}, TaskContinuationOptions.OnlyOnFaulted);
				});
			}
		}

		public static void SaveCompanyCache()
		{
			lock (CompanyCacheLock) {
				FileStream h;
				try {
					h = System.IO.File.Open("cache/company.dat", FileMode.Truncate);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to open cache/company.dat");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					Logger.Log(LOG_LEVEL.ERROR, "Company cache not saved");
					return;
				}
				using (var file = new StreamWriter(h)) {
					foreach (var val in CompanyCache) {
						try {
							if (val.ImageKey.Length == 2)
								continue;

							if (!System.IO.File.Exists("cache/company/" + val.ImageKey)) {
								// fuck GDI errors
								var ms = new MemoryStream();

								var companyImgsImage = Program.MainForm.CompanyImgs.Images[val.ImageKey];
								Debug.Assert(companyImgsImage != null);

								companyImgsImage.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
								System.IO.File.WriteAllBytes("cache/company/" + val.ImageKey, ms.ToArray());
							}

							file.WriteLine(
								"{0}\x01{1}\x01{2}\x01{3}\x01{4}",
								val.Domain == "Unknown" ? "" : val.Domain,
								val.Company,
								val.ImageKey,
								Convert.ToBase64String(val.Range.Begin.GetAddressBytes()),
								Convert.ToBase64String(val.Range.End.GetAddressBytes())
								);
						} catch (Exception ex) {
							Logger.Log(LOG_LEVEL.ERROR, "Failure saving company cache at " + val.Company);
							Logger.LogException(LOG_LEVEL.ERROR, ex);
						}
					}
				}
			}
		}

		public static Dictionary<ISDS, List<PEER>> MapPeerToSDSS(IEnumerable<PEER> Peers)
		{
			var list = new Dictionary<ISDS, List<PEER>>();

			foreach (var f in Peers) {
				var sds = f.Parent.Owner;
				if (!list.ContainsKey(sds))
					list.Add(sds, new List<PEER>());

				list[sds].Add(f);
			}

			return list;
		}

		public static void PopulateFxs()
		{
			FormatFxs.Add(Tuple.Create(Guid.Empty, "P_Done"), x => FToString.FormatPercent(x ?? 0f));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "P_Flags"), x => FToString.FormatPeerFlags(x ?? (PEER_FLAGS)0));
			new[] { "P_DL Speed", "P_UP Speed" }
				.ToList()
				.ForEach(t => FormatFxs.Add(Tuple.Create(Guid.Empty, t), x => FToString.FormatSpeed(x ?? 0ul)));
			new[] { "P_Downloaded", "P_Uploaded" }
				.ToList()
				.ForEach(t => FormatFxs.Add(Tuple.Create(Guid.Empty, t), x => FToString.FormatSize(x ?? 0ul)));
		}
	}

	public partial class f_main
	{
		private void lv_peers_CellToolTipShowing(object sender, ToolTipShowingEventArgs e)
		{
			e.Text = ((PEER)e.Item.RowObject).Company;
		}

		IEnumerable<PEER> GetSelectedPeers()
		{
			return lv_peers.SelectedObjects.Cast<PEER>();
		}

		async Task PeerAction(Func<KeyValuePair<ISDS, List<PEER>>, Task> Fx, Action<Dictionary<TORRENT, List<PEER>>> PostFx)
		{
			if (lv_peers.SelectedObjects == null)
				return;

			var peers = GetSelectedPeers();
			var pEnumer = peers as PEER[] ?? peers.ToArray();
			var sdss = Peer.MapPeerToSDSS(pEnumer);

			foreach (var kv in sdss) {
				try {
					await Fx(kv);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to execute peer action");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					return;
				}
			}

			if (PostFx != null) {
				var list = new Dictionary<TORRENT, List<PEER>>();

				foreach (var f in pEnumer) {
					var parent = f.Parent;
					if (!list.ContainsKey(parent))
						list.Add(parent, new List<PEER>());

					list[parent].Add(f);
				}

				try {
					PostFx(list);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to execute peer action postfx");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
			}
		}

		private async void b_ban_peer_Click(object sender, EventArgs e)
		{
			await PeerAction(async kv => await kv.Key.BanPeer(kv.Value), d => {
				foreach (var kv in d) {
					kv.Key.PeerList.RemoveAll(x => kv.Value.Any(y => y.Equals(x)));
					lv_peers.RemoveObjects(kv.Value);
				}
			});
		}

		private async void b_kick_peer_Click(object sender, EventArgs e)
		{
			await PeerAction(kv => kv.Key.KickPeer(kv.Value), d => {
				foreach (var kv in d) {
					kv.Key.PeerList.RemoveAll(x => kv.Value.Any(y => y.Equals(x)));
					lv_peers.RemoveObjects(kv.Value);
				}
			});
		}

		async void SnubPeer(bool Snub)
		{
			await PeerAction(kv => kv.Key.TogglePeerSnub(kv.Value, Snub), d => {
				foreach (var kv in d) {
					kv.Value.ForEach(x => {
						if (Snub)
							x.Flags |= PEER_FLAGS.S_SNUBBED;
						else
							x.Flags &= ~PEER_FLAGS.S_SNUBBED;
					});

					lv_peers.UpdateObjects(kv.Value);
				}
			});
		}

		private void b_snub_peer_Click(object sender, EventArgs e) => SnubPeer(true);

		private void b_unsnub_peer_Click(object sender, EventArgs e) => SnubPeer(false);

		private void ctxms_peer_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lv_peers.SelectedObjects?.Count == 0) {
				b_kick_peer.Enabled = b_ban_peer.Enabled = b_snub_peer.Enabled = b_unsnub_peer.Enabled = false;
				return;
			}

			var peers = GetSelectedPeers();
			var pEnumer = peers as PEER[] ?? peers.ToArray();

			b_kick_peer.Enabled = b_ban_peer.Enabled = true;

			
			b_snub_peer.Enabled = !pEnumer.All(x => x.Flags.HasFlag(PEER_FLAGS.S_SNUBBED));
			b_unsnub_peer.Enabled = !b_snub_peer.Enabled;

			var sdss = Peer.MapPeerToSDSS(pEnumer);

			var ops = sdss.Keys.Select(x => x.GetSupportedOperations());
			var opsEnumer = ops as SupportedOperations[] ?? ops.ToArray();

			if (!opsEnumer.All(x => x.peers.Kick))
				b_kick_peer.Enabled = false;
			if (!opsEnumer.All(x => x.peers.Ban))
				b_ban_peer.Enabled = false;
			if (!opsEnumer.All(x => x.peers.SnubUnsnub))
				b_snub_peer.Enabled = b_unsnub_peer.Enabled = false;
		}
	}
}
