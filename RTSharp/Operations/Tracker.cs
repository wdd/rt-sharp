﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Drawing.Imaging;
using System.Windows.Forms;
using RTSharpIFace;
using static RTSharp.Global;

namespace RTSharp
{
	class Tracker
	{
		public static Dictionary<string, Tuple<string, Bitmap>> TrackerImgs = new Dictionary<string, Tuple<string, Bitmap>>();

		public static object TrackerImgsLock = new object();


		public static void SaveTrackerCache()
		{
			lock (TrackerImgsLock) {
				FileStream h;
				try {
					h = System.IO.File.Open("cache/tracker.dat", FileMode.Truncate);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to open cache/tracker.dat");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					Logger.Log(LOG_LEVEL.ERROR, "Tracker cache not saved");
					return;
				}
				using (var file = new StreamWriter(h)) {
					foreach (var kv in TrackerImgs) {
						if (!System.IO.File.Exists(kv.Value.Item1)) {
							try {
								using (var memory = new MemoryStream()) {
									using (var fs = new FileStream("cache/tracker/" + kv.Value.Item1, FileMode.Create, FileAccess.ReadWrite)) {
										kv.Value.Item2.Save(memory, ImageFormat.Png);
										byte[] bytes = memory.ToArray();
										fs.Write(bytes, 0, bytes.Length);
									}
								}
							} catch (Exception ex) {
								Logger.Log(LOG_LEVEL.ERROR, "Failure saving tracker cache");
								Logger.LogException(LOG_LEVEL.ERROR, ex);
							}
						}

						file.WriteLine("{0}\x01{1}", kv.Key, kv.Value.Item1);
					}
				}
			}
		}

		public static void LoadTrackerCache(WaitingBox wait)
		{
			FileStream h;
			try {
				h = System.IO.File.Open("cache/tracker.dat", FileMode.OpenOrCreate);
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.FATAL, "FAILED TO OPEN OR CREATE cache/tracker.dat");
				Logger.LogException(LOG_LEVEL.FATAL, ex);
				return;
			}
			using (var trackerCache = new StreamReader(h)) {
				string line;
				var progress = 0;
				while ((line = trackerCache.ReadLine()) != null) {
					try {
						var sp = line.Split('\x01');

						var tracker = sp[0];
						var img = sp[1];
						Bitmap bm;

						using (var fs = new FileStream("cache/tracker/" + img, FileMode.Open, FileAccess.Read)) {
							bm = (Bitmap)Image.FromStream(fs);
						}

						Debug.Assert(TrackerImgs != null);

						// ReSharper disable once InconsistentlySynchronizedField
						TrackerImgs.Add(tracker, Tuple.Create(img, bm));

						if (progress % 25 == 0) {
							wait.UpdatePBarSingle((byte)((double)trackerCache.BaseStream.Position / trackerCache.BaseStream.Length * 100));
							Application.DoEvents();
						}
					} catch (Exception ex) {
						Logger.Log(LOG_LEVEL.ERROR, "Failure loading tracker cache (" + line + ")");
						Logger.LogException(LOG_LEVEL.ERROR, ex);
					}
				}
			}
		}

		public static Dictionary<ISDS, List<TRACKER>> MapTrackersToSDSS(IEnumerable<TRACKER> Trackers)
		{
			var list = new Dictionary<ISDS, List<TRACKER>>();

			foreach (var f in Trackers) {
				var sds = f.Parent.Owner;
				if (!list.ContainsKey(sds))
					list.Add(sds, new List<TRACKER>());

				list[sds].Add(f);
			}

			return list;
		}

		public static void PopulateFxs()
		{
			FormatFxs.Add(Tuple.Create(Guid.Empty, "TR_Downloaded"), x => (x ?? 0).ToString());
			FormatFxs.Add(Tuple.Create(Guid.Empty, "TR_Last updated"), x => FToString.FormatUnixTimeStamp((ulong)(x ?? 0ul), Settings.DateFormat));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "TR_Internal"), x => FToString.FormatAgo(x ?? 0f));
		}
	}

	public partial class f_main {
		private void b_set_alias_Click(object sender, EventArgs e)
		{
			var t = (TORRENT)CurSelectedObject;
			List<TORRENT> ts;

			string domain;
			var res = InputBox.Launch(
				"Enter tracker domain",
				"Enter tracker domain. System assigns an alias if tracker url string contains this domain.",
				false,
				out domain,
				t.TrackerSingle.OriginalString);
			if (res != DialogResult.OK)
				return;

			string alias;
			res = InputBox.Launch(
				"Enter tracker alias",
				"Enter tracker alias",
				false,
				out alias);
			if (res != DialogResult.OK)
				return;

			Settings.TrackerMatches.Add(domain, alias);
			IniSettings.SaveSettings();

			lock (TorrentsLock)
				ts = Torrents.Where(x => x.TrackerSingle == t.TrackerSingle).ToList();
			
			lv_torrents.RefreshObjects(ts);
		}

		private async void b_fetch_icon_Click(object sender, EventArgs e)
		{
			var t = (TORRENT)CurSelectedObject;

			if (!Settings.TrackerMatches.Keys.Any(t.TrackerSingle.OriginalString.Contains))
				b_set_alias_Click(sender, e);

			var alias = Settings.TrackerMatches.FirstOrDefault(x => t.TrackerSingle.OriginalString.Contains(x.Key)).Value;

			if (alias == null)
				return;

			var wb = new WaitingBox("Fetching tracker icon...", ProgressBarStyle.Marquee, ProgressBarStyle.Marquee);
			var domain = GetDomain.GetDomainFromUrl(t.TrackerSingle);

			if (Settings.DomainReplacements.ContainsKey(domain))
				domain = Settings.DomainReplacements[domain];

			wb.Show();

			byte[] img;
			try {
				img = await Favicons.DownloadFavicon(domain);
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.ERROR, "Failed to fetch icon for " + domain);
				Logger.LogException(LOG_LEVEL.ERROR, ex);
				wb.Close();
				return;
			}
			wb.Close();

			if (!Tracker.TrackerImgs.ContainsKey(alias))
				Tracker.TrackerImgs.Add(alias, Tuple.Create(Utils.RandomString(32), new Bitmap(Image.FromStream(new MemoryStream(img)), 16, 16)));

			lv_torrents.RefreshObjects(new[] { t });
		}

		private void ctxms_trackers_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lv_trackers.SelectedObjects == null)
				return;

			if (lv_trackers.SelectedObjects.Count == 0) {
				b_tracker_enable.Enabled = b_tracker_disable.Enabled = false;
				return;
			}

			var t = lv_trackers.SelectedObjects.Cast<TRACKER>();
			var tEnumer = t as TRACKER[] ?? t.ToArray();

			b_tracker_enable.Enabled = !tEnumer.All(x => x.Enabled);
			b_tracker_disable.Enabled = tEnumer.Any(x => x.Enabled);

			var sdss = Tracker.MapTrackersToSDSS(tEnumer);
			var ops = sdss.Keys.Select(x => x.GetSupportedOperations());

			if (!ops.All(x => x.trackers.EnableDisable))
				b_tracker_enable.Enabled = b_tracker_disable.Enabled = false;
		}

		async Task ToggleTracker(bool Enable)
		{
			if (lv_trackers.SelectedObjects == null)
				return;

			var torrents = Torrent.GetSelectedTorrents();
			var selTrackers = lv_trackers.SelectedObjects.Cast<TRACKER>();
			var selEnumer = selTrackers as TRACKER[] ?? selTrackers.ToArray();

			var trackers = new List<TRACKER>();
			var cmp = new TrackerComparer(false);
			foreach (var t in torrents) {
				trackers.AddRange(t.Trackers.Intersect(selEnumer, cmp));
			}

			var sdss = Tracker.MapTrackersToSDSS(trackers);

			foreach (var kv in sdss) {
				try {
					await kv.Key.ToggleTracker(kv.Value, Enable);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to switch tracker state for tracker(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					return;
				}
			}

			foreach (var t in trackers) {
				t.Status &= ~TRACKER.TRACKER_STATUS.ENABLED;
				t.Status |= TRACKER.TRACKER_STATUS.DISABLED;
			}

			lv_trackers.RefreshObjects(trackers.ToList());
		}

		private async void b_tracker_enable_Click(object sender, EventArgs e) => await ToggleTracker(true);

		private async void b_tracker_disable_Click(object sender, EventArgs e) => await ToggleTracker(false);

		private async void b_reannounceTracker_Click(object sender, EventArgs e)
		{
			if (lv_trackers.SelectedObjects == null)
				return;

			var torrents = Torrent.GetSelectedTorrents();
			var selTrackers = lv_trackers.SelectedObjects.Cast<TRACKER>();
			var selEnumer = selTrackers as TRACKER[] ?? selTrackers.ToArray();

			var trackers = new List<TRACKER>();
			var cmp = new TrackerComparer(false);
			foreach (var t in torrents) {
				trackers.AddRange(t.Trackers.Intersect(selEnumer, cmp));
			}

			var sdss = Tracker.MapTrackersToSDSS(trackers);

			foreach (var kv in sdss) {
				try {
					await kv.Key.Reannounce(kv.Value);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to switch reannounce for tracker(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					return;
				}
			}

			lv_trackers.RefreshObjects(trackers.ToList());
		}
	}
}
