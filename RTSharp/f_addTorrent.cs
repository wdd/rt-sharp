﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using RTSharpIFace;

using static RTSharp.Global;

namespace RTSharp {
	public partial class f_addTorrent : Form {

		public f_addTorrent()
		{
			InitializeComponent();
		}

		bool addSinglePreFill(string In)
		{
			try {
				if (new Uri(In).Scheme != "file") {
					t_uri.Text = In;
					rb_uri.Checked = true;
				} else {
					t_file.Text = In;
					rb_file.Checked = true;
				}
				return true;
			} catch {
				MessageBox.Show("Invalid input", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return false;
			}
		}

		public f_addTorrent(string[] PreFill)
		{
			InitializeComponent();

			var res = true;
			if (PreFill != null && PreFill.Length == 1)
				res = addSinglePreFill(PreFill.First());

			if (PreFill == null || !res)
				return;

			if (PreFill.Length != 1) {
				files = PreFill.ToList();
				rb_file.Checked = true;
				t_file.Text = "<Multiple items>";
			}
		}

		public f_addTorrent(string PreFill)
		{
			InitializeComponent();

			if (PreFill == null)
				return;

			addSinglePreFill(PreFill);
		}

		List<string> files;
		ISDS CurSDS;

		List<ISFAQueue.DUP_TARGET> DupTargets = new List<ISFAQueue.DUP_TARGET>();

		private void b_browse_Click(object sender, EventArgs e)
		{
			UncheckAll();
			rb_file.Checked = true;
			var ofd = new OpenFileDialog {
				Multiselect = true,
				CheckFileExists = true,
				Filter = "Torrent files (*.torrent)|*.torrent"
			};
			if (ofd.ShowDialog() != DialogResult.OK)
				return;

			if (ofd.FileNames.Length >= 2) {
				files = ofd.FileNames.ToList();
				t_file.Text = "<Multiple items>";
			} else {
				files = new List<string> { ofd.FileName };
				t_file.Text = ofd.FileName;
			}
		}

		private async void b_ok_Click(object sender, EventArgs e)
		{
			if (CurSDS == null) {
				MessageBox.Show("Please select a connection", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			Enabled = false;

			if (!chk_start.Checked && DupTargets.Count != 0) {
				if (MessageBox.Show(
					"Your torrent(s) will not be started after add, but you have one or more duplication targets. Proceed?",
					"RT#",
					MessageBoxButtons.YesNo,
					MessageBoxIcon.Exclamation) == DialogResult.No)
				{
					Enabled = true;
					return;
				}
			}

			var mode = rb_file.Checked ? 'f' : (rb_uri.Checked ? 'u' : 'c');

			DialogResult res;
			var wb = new WaitingBox("Adding...", ProgressBarStyle.Marquee, ProgressBarStyle.Blocks);
			wb.Show();

			switch (mode) {
				case 'f':
					res = MessageBox.Show("Are you sure you want to add " + (files.Count == 1 ? "1 torrent" : files.Count + " torrents") + "?", "RT#", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
					if (res != DialogResult.Yes) {
						Enabled = true;
						return;
					}

					if (!(await TorrentAddMulti(files, null, wb))) {
						wb.Close();
						Enabled = true;
						return;
					}
					break;
				case 'u':
					res = MessageBox.Show("Are you sure you want to add 1 URI?", "RT#", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
					if (res != DialogResult.Yes) {
						wb.Close();
						Enabled = true;
						return;
					}

					if (!(await TorrentAddMulti(null, new[] { t_uri.Text }.ToList(), wb))) {
						wb.Close();
						Enabled = true;
						return;
					}
					break;
				case 'c':

					var clip = Clipboard.GetText();

					res = MessageBox.Show(
						"Add" + (clip.Contains("\n") ? " multiple" : "") + " torrent" + (clip.Contains("\n") ? "s" : "") +
						" from clipboard?",
						"RT#",
						MessageBoxButtons.YesNo,
						MessageBoxIcon.Question);
					if (res != DialogResult.Yes) {
						wb.Close();
						Enabled = true;
						return;
					}

					var s = clip.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None).Select(x => x.Trim());
					if (!(await TorrentAddMulti(null, s, wb))) {
						wb.Close();
						Enabled = true;
						return;
					}
					break;
			}

			Enabled = true;
			wb.Close();
			Program.MainForm.updateTorrents = f_main.UPDATE_TORRENTS.UPDATE;
		}

		private void TorrentDoISFA(List<byte[]> files, bool StartOnAdd, List<byte[]> hashes)
		{
			EventHandler<RTSEvents.TorrentFinishedArgs> evFinished = null;
			EventHandler<RTSEvents.NewTorrentArgs> evNewTorrent = null;

			// Primary completes -> Load dup -> Force recheck on new dup torrent -> Start on complete dup torrent
			evFinished = async (sender, e) => {
				var torrent = (TORRENT)sender;

				var index = hashes.FindIndex(i => i.SequenceEqual(torrent.Hash));
				if (index == -1)
					return;

				if (torrent.ISFAState == TORRENT_ISFA.PENDING && torrent.Owner.UniqueGUID.Equals(CurSDS.UniqueGUID)) {
					// Primary completed

					if (files == null) {
						var file = (await torrent.Owner.GetTorrentFiles(new[] { torrent }.ToList())).First();
						ISFAQueue.AddQueue(torrent, file, DupTargets.Where(x => x.DuplicateFiles).ToList());
					} else
						ISFAQueue.AddQueue(torrent, files[index], DupTargets.Where(x => x.DuplicateFiles).ToList());

					hashes.RemoveAt(index);

					if (hashes.Count == 0) {
						RTSEvents.TorrentFinished -= evFinished;
						RTSEvents.NewTorrent -= evNewTorrent;
					}
				}
			};

			evNewTorrent = (sender, e) => {
				if (hashes.FindIndex(i => i.SequenceEqual(e.Torrent.Hash)) != -1 && e.Torrent.Owner.UniqueGUID.Equals(CurSDS.UniqueGUID))
					e.Torrent.ISFAState = TORRENT_ISFA.PENDING;
			};

			if (DupTargets.Count > 0 && StartOnAdd) {
				DupTargets.ForEach(x => {
					if (!x.RemotePath.EndsWith("/"))
						x.RemotePath = x.RemotePath + "/";
				});

				if (files != null)
					foreach (var dup in DupTargets.Where(x => !x.DuplicateFiles))
						foreach (var file in files)
							dup.Target.SDS.LoadRaw(file, false, dup.RemotePath);

				RTSEvents.TorrentFinished += evFinished;
				RTSEvents.NewTorrent += evNewTorrent;
			}
		}

		private async Task<bool> TorrentAddMultiBytes(List<byte[]> files, bool StartOnAdd, string RemotePath)
		{
			try {
				var tasks = new List<Task>();

				foreach (var file in files) {
					while (tasks.Count >= Environment.ProcessorCount)
						await Task.WhenAny(tasks);

					Task task = null;
					task = Task.Run(async () => {
						await CurSDS.LoadRaw(file, StartOnAdd, RemotePath);
						tasks.Remove(task);
					});

					tasks.Add(task);
				}

				if (tasks.Count != 0)
					await Task.WhenAll(tasks);
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.ERROR, "Failed to load raw torrent");
				Logger.LogException(LOG_LEVEL.ERROR, ex);
				MessageBox.Show("Failed to add torrent: " + ex, "RT#", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}

			return true;
		}

		public async Task<bool> TorrentAddMulti(IEnumerable<string> InFile, IEnumerable<string> Uri, WaitingBox WBox) {
			List<byte[]> files = new List<byte[]>();

			var startOnAdd = chk_start.Checked;
			var remotePath = t_remoteDir.Text;

			if (InFile == null) {
				try {

					IProgress<byte> progress = new Progress<byte>(percent => {
						WBox?.UpdatePBarAll(percent);
					});

					WBox?.Show();

					var tasks = new List<Task>();

					var magnets = Uri.Where(x => x.StartsWith("magnet"));
					
					if (magnets.Count() != 0) {
						IEnumerable<byte[]> magnetHashes;
						try {
							magnetHashes = magnets.Select(x => new MagnetManip(x).GetBtih()).SelectMany(x => x);

							if (magnetHashes.Count() == 0)
								throw new InvalidDataException("No magnet hashes extracted");
						} catch (Exception ex) {
							Logger.Log(LOG_LEVEL.ERROR, "Invalid magnet urls");
							Logger.LogException(LOG_LEVEL.ERROR, ex);
							MessageBox.Show("Invalid magnet url", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Error);
							return false;
						}

						TorrentDoISFA(null, startOnAdd, magnetHashes.ToList());

						foreach (var magnet in magnets) {
							while (tasks.Count >= Environment.ProcessorCount)
								await Task.WhenAny(tasks);

							Task task = null;
							task = Task.Run(async () => {
								await CurSDS.LoadMagnet(magnet, startOnAdd, remotePath);
								tasks.Remove(task);
							});

							tasks.Add(task);
						}

						if (tasks.Count != 0)
							await Task.WhenAll(tasks);
					}

					foreach (var uri in Uri.Where(x => !x.StartsWith("magnet"))) {
						while (tasks.Count >= Environment.ProcessorCount)
							await Task.WhenAny(tasks);

						Task task = null;
						task = Task.Run(async () => {
							var client = new WebClient();
							try {
								files.Add(await client.DownloadDataTaskAsync(new Uri(uri)));
								var a = 2;
							} catch (Exception ex) {
								Logger.Log(LOG_LEVEL.ERROR, "Failed to download torrent");
								throw ex;
							}

							tasks.Remove(task);
						});
						tasks.Add(task);
					}
					await Task.WhenAll(tasks);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to load URI");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					MessageBox.Show("Failed to load URI", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return false;
				}
			}

			try {
				if (InFile != null) {
					Parallel.ForEach(InFile, (x) => {
						files.Add(System.IO.File.ReadAllBytes(x));
					});
				}
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.ERROR, "Failed to read torrent file from local disk");
				Logger.LogException(LOG_LEVEL.ERROR, ex);
				MessageBox.Show("Unable to read file", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}

			if (await TorrentAddMultiBytes(files, startOnAdd, remotePath)) {
				List<byte[]> hashes = new List<byte[]>();

				try {
					Parallel.ForEach(files, (x) => {
						hashes.Add(new TorrentManip(x).GetInfoHash());
					});
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Invalid torrent");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					MessageBox.Show("Failed to load torrent:" + Environment.NewLine + ex);
					return false;
				}

				TorrentDoISFA(files, startOnAdd, hashes);
				return true;
			}
			return false;
		}

		private void b_cancel_Click(object sender, EventArgs e) {
			Close();
		}

		private void t_file_TextChanged(object sender, EventArgs e) {
			if (String.IsNullOrEmpty(t_file.Text))
				files = null;
			else {
				if (t_file.Text == "<Multiple items>")
					return;
				files = new List<string> {
					t_file.Text
				};
			}
		}

		private void f_addTorrent_Load(object sender, EventArgs e) {
			if (SDSS.Count == 0) {
				MessageBox.Show("No connections", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}

			cmb_sdss.Items.Clear();
			cmb_duplication_sdss.Items.Clear();
			foreach (var sds in SDSS) {
				cmb_sdss.Items.Add(sds);
				if (sds.SDS.GetSupportedOperations().files.ISFA)
					cmb_duplication_sdss.Items.Add(sds);
			}

			CurSDS = null;
			lv_dupTargets.SetObjects(DupTargets);
			l_duplication_primary.Text = "Not selected";
		}

		private async void cmb_sdss_SelectedIndexChanged(object sender, EventArgs e)
		{
			CurSDS = ((SystemDataSupplierClass)cmb_sdss.SelectedItem)?.SDS;

			if (CurSDS == null)
				return;
			
			var ops = CurSDS.GetSupportedOperations();

			t_remoteDir.Enabled = ops.files.GetRemoteDirectories;
			b_remoteBrowse.Enabled = ops.files.GetRemoteDirectories;
			chk_fastResume.Enabled = ops.torrents.FastResume;
			try {
				t_remoteDir.Text = await CurSDS.GetDefaultSavePath();
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.ERROR, "Failed to get default save path (" + CurSDS.FriendlyName + " (" + CurSDS.UniqueGUID + "))");
				Logger.LogException(LOG_LEVEL.ERROR, ex);
			}
			l_duplication_primary.Text = ((SystemDataSupplierClass)cmb_sdss.SelectedItem).ToString();

			DupTargets.Clear();
			lv_dupTargets.SetObjects(DupTargets);
		}

		void UncheckAll()
		{
			rb_clip.Checked = rb_file.Checked = rb_uri.Checked = false;
		}

		private void b_clipPreview_Click(object sender, EventArgs e)
		{
			UncheckAll();
			rb_clip.Checked = true;
			new ResultsBox().Show(Clipboard.GetText());
		}

		private void file_MouseClick(object sender, EventArgs e) { UncheckAll(); rb_file.Checked = true; }
		private void uri_MouseClick(object sender, EventArgs e) { UncheckAll(); rb_uri.Checked = true; }
		private void g_clip_MouseClick(object sender, EventArgs e) { UncheckAll(); rb_clip.Checked = true; }

		private void b_remoteBrowse_Click(object sender, EventArgs e)
		{
			if (CurSDS == null)
				return;

			var res = new SelectRemoteFolder(CurSDS, t_remoteDir.Text).ShowDialogWResult();

			if (!String.IsNullOrEmpty(res))
				t_remoteDir.Text = res;
		}

		private async void b_duplication_change_Click(object sender, EventArgs e)
		{
			var sds = ((SystemDataSupplierClass)cmb_duplication_sdss.SelectedItem)?.SDS;
			if (sds == null || CurSDS == null)
				return;

			if (sds.UniqueGUID.Equals(CurSDS.UniqueGUID))
				return;

			if (DupTargets.Any(x => x.Target.SDS.UniqueGUID.Equals(sds.UniqueGUID))) {
				DupTargets.RemoveAll(x => x.Target.SDS.UniqueGUID.Equals(sds.UniqueGUID));
				lv_dupTargets.SetObjects(DupTargets);
				return;
			}

			var defaultPath = await sds.GetDefaultSavePath();

			var dt = new ISFAQueue.DUP_TARGET((SystemDataSupplierClass)cmb_duplication_sdss.SelectedItem, defaultPath, true);

			DupTargets.Add(dt);
			lv_dupTargets.AddObject(dt);
		}

		private void lv_dupTargets_ButtonClick(object sender, BrightIdeasSoftware.CellClickEventArgs e)
		{
			var srf = new SelectRemoteFolder((((ISFAQueue.DUP_TARGET)e.Model).Target).SDS);
			var res = srf.ShowDialogWResult();

			if (String.IsNullOrEmpty(res))
				return;

			((ISFAQueue.DUP_TARGET)e.Model).RemotePath = res;
			lv_dupTargets.UpdateObject(e.Model);
		}
	}
}
