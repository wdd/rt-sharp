﻿namespace RTSharpIFace
{
	/// <summary>
	/// Server setting
	/// </summary>
	public class ServerSetting
	{
		/// <summary>
		/// Setting identifier
		/// </summary>
		public uint Id { get; set; }
		/// <summary>
		/// User-friendly setting name, must be unique
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Setting description
		/// </summary>
		public string Description { get; set; }
		/// <summary>
		/// Setting category, where preceding element is father category of subsequent element
		/// </summary>
		public string[] Category { get; set; }

		/// <summary>
		/// Default value of the setting, must be one of the supported types
		/// </summary>
		/// <remarks>
		///		<para>
		///			Setting type is determined by <see cref="DefaultValue"/>, and must be one of supported types:
		///			<list type="bullet">
		///				<item>
		///					<description><c>KeyValuePair&lt;string, bool&gt;[]</c> - generates a <c>ComboBox</c> of <c>Key</c> elements in array. Array must contain all possible setting values and the default value must be set <c>True</c></description>
		///				</item>
		///				<item>
		///					<description><c>bool</c> - generates a <c>ComboBox</c> of elements <c>True</c> and <c>False</c></description>
		///				</item>
		///				<item>
		///					<description><c>int, uint, long, ulong, short, ushort, byte, sbyte, float, double, decimal</c> - generates a <c>NumericUpDown</c></description>
		///				</item>
		///				<item>
		///					<description><c>string</c> (anything else) - generates a <c>TextBox</c>. Any other setting type than <c>string</c> must be convertable to <c>string</c> and vice-versa</description>
		///				</item>
		///			</list>
		///		</para>
		/// </remarks>
		public dynamic DefaultValue { get; set; }

		/// <summary>
		/// Must be one of supported types, see <see cref="DefaultValue"/>
		/// </summary>
		public dynamic CurrentValue { get; set; }

		/// <param name="Id"><see cref="Id"/></param>
		/// <param name="Name"><see cref="Name"/></param>
		/// <param name="Description"><see cref="Description"/></param>
		/// <param name="Category"><see cref="Category"/></param>
		/// <param name="DefaultValue"><see cref="DefaultValue"/></param>
		/// <param name="CurrentValue"><see cref="CurrentValue"/></param>
		public ServerSetting(uint Id, string Name, string Description, string[] Category, dynamic DefaultValue, dynamic CurrentValue)
		{
			this.Id = Id;
			this.Name = Name;
			this.Description = Description;
			this.Category = Category;
			this.DefaultValue = DefaultValue;
			this.CurrentValue = CurrentValue;
		}
	}
}
