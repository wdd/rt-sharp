﻿using System;
using System.Net;

namespace RTSharpIFace
{
	[Flags]
	public enum PEER_FLAGS : byte
	{
		I_INCOMING = 1 << 0,
		E_ENCRYPTED = 1 << 1,
		S_SNUBBED = 1 << 2,
		O_OBFUSCATED = 1 << 3
	}

	public class PEER/* : IComparable*/
	{
		/// <summary>
		/// Parent torrent
		/// </summary>
		public TORRENT Parent { get; }

		/// <summary>
		/// Peer ID internal to SDS
		/// </summary>
		public object ID { get; }

		/// <summary>
		/// Two-letter country code
		/// </summary>
		public string Country { get; set; }

		/// <summary>
		/// Company domain
		/// </summary>
		public string Domain { get; set; }

		/// <summary>
		/// Company
		/// </summary>
		public string Company { get; set; }

		/// <summary>
		/// Company image key
		/// </summary>
		/// <remarks>See also in core: Program.MainForm.CompanyImgs</remarks>
		public string ImageKey { get; set; }

		/// <summary>
		/// Peer IP address
		/// </summary>
		public IPAddress IP { get; set; }

		/// <summary>
		/// Peer port
		/// </summary>
		public ushort Port { get; set; }

		/// <summary>
		/// Peer IP:Port combination
		/// </summary>
		public string IPPort => IP + ":" + Port;

		/// <summary>
		/// Peer client
		/// </summary>
		public string Client { get; set; }

		/// <summary>
		/// Peer flags
		/// </summary>
		public PEER_FLAGS Flags { get; set; }

		/// <summary>
		/// Done percentage
		/// </summary>
		public float Done { get; set; }

		/// <summary>
		/// Downloaded bytes from peer
		/// </summary>
		public ulong Downloaded { get; set; }

		/// <summary>
		/// Uploaded bytes to peer
		/// </summary>
		public ulong Uploaded { get; set; }

		/// <summary>
		/// Download speed from peer
		/// </summary>
		public ulong DLSpeed { get; set; }

		/// <summary>
		/// Upload speed to peer
		/// </summary>
		public ulong UPSpeed { get; set; }

		public PEER(TORRENT Parent, object ID)
		{
			this.Parent = Parent;
			this.ID = ID;
		}

		public bool Equals(PEER a)
		{
			var r1 = a.IP.Equals(IP);
			var r2 = a.Client == Client;
			var r3 = a.Flags == Flags;
			return r1 && r2 && r3;
		}

		public PEER Copy(TORRENT Parent)
		{
			var cp = new PEER(Parent, ID);
			cp.Country = Country;
			cp.Domain = Domain;
			cp.Company = Company;
			cp.ImageKey = ImageKey;
			cp.IP = IP;
			cp.Port = Port;
			cp.Client = Client;
			cp.Flags = Flags;
			cp.Done = Done;
			cp.Downloaded = Downloaded;
			cp.Uploaded = Uploaded;
			cp.DLSpeed = DLSpeed;
			cp.UPSpeed = UPSpeed;
			return cp;
		}

		/*public int CompareTo(object obj)
		{
			if (!(obj is PEER))
				throw new ArgumentException();

			var p = (PEER)obj;

			int result = 0;
			var bytes = IP.GetAddressBytes();
			if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6) {
				var part1 = BitConverter.ToUInt64(bytes, 0);

				if (p.IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
					result = part1.CompareTo(BitConverter.ToUInt32(p.IP.GetAddressBytes(), 0));
					if (result != 0) return result;
				} else {
					var bytes2 = p.IP.GetAddressBytes();
					result = part1.CompareTo(BitConverter.ToUInt64(bytes2, 0));
					if (result != 0) return result;

					var part2 = BitConverter.ToUInt64(bytes, 8);
					result = part2.CompareTo(BitConverter.ToUInt64(bytes2, 8));
					if (result != 0) return result;
				}
			} else {
				var i = BitConverter.ToUInt32(IP.GetAddressBytes(), 0);
				result = i.CompareTo(BitConverter.ToUInt32(p.IP.GetAddressBytes(), 0));
				if (result != 0) return result;
			}

			result = Client.CompareTo(p.Client);
			if (result != 0) return result;

			result = Flags.CompareTo(p.Flags);
			if (result != 0) return result;

			return result;
		}*/
	}
}
