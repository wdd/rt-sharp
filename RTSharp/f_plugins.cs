﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using static RTSharp.Global;

namespace RTSharp {
	public partial class f_plugins : Form {
		readonly bool SDSSForm;
		readonly List<dynamic> List;

		public f_plugins(bool ShowSDSS) {
			SDSSForm = ShowSDSS;
			List = SDSSForm ? SDSS.Select(x => (dynamic)x).ToList() : Plugins.Select(x => (dynamic)x).ToList(); // OK_HAND
			InitializeComponent();
		}

		private void f_plugins_Load(object sender, EventArgs e) {

			if (SDSSForm && SDSS.Count == 0) {
				Logger.Log(RTSharpIFace.LOG_LEVEL.INFO, "No SDSs");
				MessageBox.Show("No System Data Suppliers", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				Close();
				return;
			} else if (!SDSSForm && Plugins.Count == 0) {
				Logger.Log(RTSharpIFace.LOG_LEVEL.INFO, "No plugins");
				MessageBox.Show("No Plugins", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				Close();
				return;
			}

			lv_plugins.HeaderStyle = ColumnHeaderStyle.None;
			var header = new ColumnHeader {
				Text = "",
				Name = "head"
			};
			lv_plugins.Columns.Add(header);

			Text = SDSSForm ? "Connections" : "Plugins";

			foreach (var plugin in List) {
				if (SDSSForm)
					lv_plugins.Items.Add(plugin.SDS.FriendlyName + " (" + plugin.SDS.UniqueGUID + ")");
				else
					lv_plugins.Items.Add(plugin.IFace.Name + " (" + plugin.IFace.UniqueGUID + ")");
			}

			lv_plugins.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
		}

		private void lv_plugins_SelectedIndexChanged(object sender, EventArgs e)
		{
		    if (lv_plugins.SelectedIndices.Count != 1) {
                t_uniqGuid.Text = "";
                t_name.Text = "";
                t_desc.Text = "";
                t_author.Text = "";
                t_version.Text = "";
                t_guid.Text = "";
                return;
		    }
		    var plugin = List[lv_plugins.SelectedIndices[0]];
			if (SDSSForm) {
				t_uniqGuid.Text = plugin.SDS.UniqueGUID.ToString();
				t_name.Text = plugin.SDS.FriendlyName;
				t_desc.Text = plugin.SDS.Description;
				t_author.Text = plugin.SDS.Author;
				t_version.Text = plugin.SDS.Version;
				t_guid.Text = plugin.SDS.GUID.ToString();
			} else {
				t_uniqGuid.Text = plugin.IFace.UniqueGUID.ToString();
				t_name.Text = plugin.IFace.Name;
				t_desc.Text = plugin.IFace.Description;
				t_author.Text = plugin.IFace.Author;
				t_version.Text = plugin.IFace.Version;
				t_guid.Text = plugin.IFace.GUID.ToString();
			}
		}

		private void b_settings_Click(object sender, EventArgs e)
		{
			var plugin = List[lv_plugins.SelectedIndices[0]];
			if (!SDSSForm)
				plugin.IFace.GetPluginSettings(((PluginClass)plugin).IFace).ShowDialog();
			else
				new f_serverSettings(((SystemDataSupplierClass)plugin).SDS).ShowDialog();
		}
	}
}
