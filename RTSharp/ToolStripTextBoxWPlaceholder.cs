﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace RTSharp
{
	public partial class ToolStripTextBoxWPlaceholder : ToolStripTextBox
	{
		string _Placeholder;
		public string Placeholder {
			get {
				return _Placeholder;
			}
			set {
				_Placeholder = value;
				ToolStripTextBoxWPlaceholder_LostFocus(null, null);
			}
		}

		public ToolStripTextBoxWPlaceholder()
		{
			GotFocus += ToolStripTextBoxWPlaceholder_GotFocus;
			LostFocus += ToolStripTextBoxWPlaceholder_LostFocus;
			KeyDown += ToolStripTextBoxWPlaceholder_KeyDown;
		}

		private void ToolStripTextBoxWPlaceholder_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control && e.KeyCode == Keys.A) {
				SelectAll();
				e.SuppressKeyPress = true;
			}
		}

		private void ToolStripTextBoxWPlaceholder_LostFocus(object sender, EventArgs e)
		{
			if (Text == "") {
				Text = Placeholder;
				ForeColor = SystemColors.GrayText;
			}
		}

		private void ToolStripTextBoxWPlaceholder_GotFocus(object sender, EventArgs e)
		{
			if (ForeColor == SystemColors.GrayText) {
				Text = "";
				ForeColor = SystemColors.WindowText;
			}
		}
	}
}