﻿using BrightIdeasSoftware;

namespace RTSharp
{
	partial class f_addTorrent
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(f_addTorrent));
			this.tab_addTorrent = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.chk_fastResume = new System.Windows.Forms.CheckBox();
			this.g_clip = new System.Windows.Forms.GroupBox();
			this.b_clipPreview = new System.Windows.Forms.Button();
			this.rb_clip = new System.Windows.Forms.RadioButton();
			this.chk_start = new System.Windows.Forms.CheckBox();
			this.b_cancel = new System.Windows.Forms.Button();
			this.b_ok = new System.Windows.Forms.Button();
			this.g_uri = new System.Windows.Forms.GroupBox();
			this.rb_uri = new System.Windows.Forms.RadioButton();
			this.t_uri = new System.Windows.Forms.TextBox();
			this.g_file = new System.Windows.Forms.GroupBox();
			this.b_browse = new System.Windows.Forms.Button();
			this.rb_file = new System.Windows.Forms.RadioButton();
			this.t_file = new System.Windows.Forms.TextBox();
			this.g_remoteDir = new System.Windows.Forms.GroupBox();
			this.b_remoteBrowse = new System.Windows.Forms.Button();
			this.t_remoteDir = new System.Windows.Forms.TextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.cmb_sdss = new System.Windows.Forms.ComboBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.lv_dupTargets = new BrightIdeasSoftware.ObjectListView();
			this.lvc_dupTargets_target = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_dupTargets_path = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_dupTargets_browse = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_dupTargets_dupFiles = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.g_dupAdd = new System.Windows.Forms.GroupBox();
			this.b_duplication_change = new System.Windows.Forms.Button();
			this.cmb_duplication_sdss = new System.Windows.Forms.ComboBox();
			this.g_dupPrimary = new System.Windows.Forms.GroupBox();
			this.l_duplication_primary = new System.Windows.Forms.Label();
			this.tab_addTorrent.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.g_clip.SuspendLayout();
			this.g_uri.SuspendLayout();
			this.g_file.SuspendLayout();
			this.g_remoteDir.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.lv_dupTargets)).BeginInit();
			this.g_dupAdd.SuspendLayout();
			this.g_dupPrimary.SuspendLayout();
			this.SuspendLayout();
			// 
			// tab_addTorrent
			// 
			this.tab_addTorrent.Controls.Add(this.tabPage1);
			this.tab_addTorrent.Controls.Add(this.tabPage2);
			this.tab_addTorrent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tab_addTorrent.Location = new System.Drawing.Point(0, 0);
			this.tab_addTorrent.Name = "tab_addTorrent";
			this.tab_addTorrent.SelectedIndex = 0;
			this.tab_addTorrent.Size = new System.Drawing.Size(453, 312);
			this.tab_addTorrent.TabIndex = 7;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.chk_fastResume);
			this.tabPage1.Controls.Add(this.g_clip);
			this.tabPage1.Controls.Add(this.chk_start);
			this.tabPage1.Controls.Add(this.b_cancel);
			this.tabPage1.Controls.Add(this.b_ok);
			this.tabPage1.Controls.Add(this.g_uri);
			this.tabPage1.Controls.Add(this.g_file);
			this.tabPage1.Controls.Add(this.g_remoteDir);
			this.tabPage1.Controls.Add(this.groupBox3);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(445, 286);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Source";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// chk_fastResume
			// 
			this.chk_fastResume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.chk_fastResume.AutoSize = true;
			this.chk_fastResume.Location = new System.Drawing.Point(10, 238);
			this.chk_fastResume.Name = "chk_fastResume";
			this.chk_fastResume.Size = new System.Drawing.Size(83, 17);
			this.chk_fastResume.TabIndex = 17;
			this.chk_fastResume.Text = "Fast resume";
			this.chk_fastResume.UseVisualStyleBackColor = true;
			// 
			// g_clip
			// 
			this.g_clip.Controls.Add(this.b_clipPreview);
			this.g_clip.Controls.Add(this.rb_clip);
			this.g_clip.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_clip.Location = new System.Drawing.Point(3, 182);
			this.g_clip.Name = "g_clip";
			this.g_clip.Size = new System.Drawing.Size(439, 49);
			this.g_clip.TabIndex = 16;
			this.g_clip.TabStop = false;
			this.g_clip.Text = "    From clipboard (URI\'s)";
			this.g_clip.MouseClick += g_clip_MouseClick;
			// 
			// b_clipPreview
			// 
			this.b_clipPreview.Dock = System.Windows.Forms.DockStyle.Top;
			this.b_clipPreview.Location = new System.Drawing.Point(3, 16);
			this.b_clipPreview.Name = "b_clipPreview";
			this.b_clipPreview.Size = new System.Drawing.Size(433, 23);
			this.b_clipPreview.TabIndex = 0;
			this.b_clipPreview.Text = "Preview";
			this.b_clipPreview.UseVisualStyleBackColor = true;
			this.b_clipPreview.Click += b_clipPreview_Click;
			// 
			// rb_clip
			// 
			this.rb_clip.AutoSize = true;
			this.rb_clip.Location = new System.Drawing.Point(5, 1);
			this.rb_clip.Name = "rb_clip";
			this.rb_clip.Size = new System.Drawing.Size(14, 13);
			this.rb_clip.TabIndex = 7;
			this.rb_clip.TabStop = true;
			this.rb_clip.UseVisualStyleBackColor = true;
			// 
			// chk_start
			// 
			this.chk_start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.chk_start.AutoSize = true;
			this.chk_start.Location = new System.Drawing.Point(10, 261);
			this.chk_start.Name = "chk_start";
			this.chk_start.Size = new System.Drawing.Size(81, 17);
			this.chk_start.TabIndex = 15;
			this.chk_start.Text = "Start torrent";
			this.chk_start.UseVisualStyleBackColor = true;
			// 
			// b_cancel
			// 
			this.b_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.b_cancel.Location = new System.Drawing.Point(364, 255);
			this.b_cancel.Name = "b_cancel";
			this.b_cancel.Size = new System.Drawing.Size(75, 23);
			this.b_cancel.TabIndex = 13;
			this.b_cancel.Text = "Cancel";
			this.b_cancel.UseVisualStyleBackColor = true;
			this.b_cancel.Click += new System.EventHandler(this.b_cancel_Click);
			// 
			// b_ok
			// 
			this.b_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.b_ok.Location = new System.Drawing.Point(283, 255);
			this.b_ok.Name = "b_ok";
			this.b_ok.Size = new System.Drawing.Size(75, 23);
			this.b_ok.TabIndex = 10;
			this.b_ok.Text = "Add";
			this.b_ok.UseVisualStyleBackColor = true;
			this.b_ok.Click += b_ok_Click;
			// 
			// g_uri
			// 
			this.g_uri.Controls.Add(this.rb_uri);
			this.g_uri.Controls.Add(this.t_uri);
			this.g_uri.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_uri.Location = new System.Drawing.Point(3, 133);
			this.g_uri.Name = "g_uri";
			this.g_uri.Size = new System.Drawing.Size(439, 49);
			this.g_uri.TabIndex = 14;
			this.g_uri.TabStop = false;
			this.g_uri.Text = "    From URI";
			this.g_uri.MouseClick += uri_MouseClick;
			// 
			// rb_uri
			// 
			this.rb_uri.AutoSize = true;
			this.rb_uri.Location = new System.Drawing.Point(5, 0);
			this.rb_uri.Name = "rb_uri";
			this.rb_uri.Size = new System.Drawing.Size(14, 13);
			this.rb_uri.TabIndex = 6;
			this.rb_uri.TabStop = true;
			this.rb_uri.UseVisualStyleBackColor = true;
			// 
			// t_uri
			// 
			this.t_uri.Dock = System.Windows.Forms.DockStyle.Fill;
			this.t_uri.Location = new System.Drawing.Point(3, 16);
			this.t_uri.Name = "t_uri";
			this.t_uri.Size = new System.Drawing.Size(433, 20);
			this.t_uri.TabIndex = 0;
			this.t_uri.MouseClick += uri_MouseClick;
			// 
			// g_file
			// 
			this.g_file.Controls.Add(this.b_browse);
			this.g_file.Controls.Add(this.rb_file);
			this.g_file.Controls.Add(this.t_file);
			this.g_file.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_file.Location = new System.Drawing.Point(3, 84);
			this.g_file.Name = "g_file";
			this.g_file.Size = new System.Drawing.Size(439, 49);
			this.g_file.TabIndex = 12;
			this.g_file.TabStop = false;
			this.g_file.Text = "    From file";
			this.g_file.MouseClick += file_MouseClick;
			// 
			// b_browse
			// 
			this.b_browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.b_browse.Location = new System.Drawing.Point(361, 14);
			this.b_browse.Name = "b_browse";
			this.b_browse.Size = new System.Drawing.Size(75, 23);
			this.b_browse.TabIndex = 4;
			this.b_browse.Text = "Browse...";
			this.b_browse.UseVisualStyleBackColor = true;
			this.b_browse.Click += b_browse_Click;
			// 
			// rb_file
			// 
			this.rb_file.AutoSize = true;
			this.rb_file.Location = new System.Drawing.Point(5, 0);
			this.rb_file.Name = "rb_file";
			this.rb_file.Size = new System.Drawing.Size(14, 13);
			this.rb_file.TabIndex = 5;
			this.rb_file.TabStop = true;
			this.rb_file.UseVisualStyleBackColor = true;
			// 
			// t_file
			// 
			this.t_file.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.t_file.Location = new System.Drawing.Point(3, 16);
			this.t_file.Name = "t_file";
			this.t_file.Size = new System.Drawing.Size(352, 20);
			this.t_file.TabIndex = 3;
			this.t_file.MouseClick += file_MouseClick;
			this.t_file.TextChanged += t_file_TextChanged;
			// 
			// g_remoteDir
			// 
			this.g_remoteDir.Controls.Add(this.b_remoteBrowse);
			this.g_remoteDir.Controls.Add(this.t_remoteDir);
			this.g_remoteDir.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_remoteDir.Location = new System.Drawing.Point(3, 43);
			this.g_remoteDir.Name = "g_remoteDir";
			this.g_remoteDir.Size = new System.Drawing.Size(439, 41);
			this.g_remoteDir.TabIndex = 18;
			this.g_remoteDir.TabStop = false;
			this.g_remoteDir.Text = "Remote directory";
			// 
			// b_remoteBrowse
			// 
			this.b_remoteBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.b_remoteBrowse.Location = new System.Drawing.Point(361, 13);
			this.b_remoteBrowse.Name = "b_remoteBrowse";
			this.b_remoteBrowse.Size = new System.Drawing.Size(75, 23);
			this.b_remoteBrowse.TabIndex = 1;
			this.b_remoteBrowse.Text = "Browse...";
			this.b_remoteBrowse.UseVisualStyleBackColor = true;
			this.b_remoteBrowse.Click += new System.EventHandler(this.b_remoteBrowse_Click);
			// 
			// t_remoteDir
			// 
			this.t_remoteDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.t_remoteDir.Location = new System.Drawing.Point(6, 15);
			this.t_remoteDir.Name = "t_remoteDir";
			this.t_remoteDir.Size = new System.Drawing.Size(349, 20);
			this.t_remoteDir.TabIndex = 0;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.cmb_sdss);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox3.Location = new System.Drawing.Point(3, 3);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(439, 40);
			this.groupBox3.TabIndex = 11;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Plugin";
			// 
			// cmb_sdss
			// 
			this.cmb_sdss.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.cmb_sdss.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmb_sdss.FormattingEnabled = true;
			this.cmb_sdss.Location = new System.Drawing.Point(3, 13);
			this.cmb_sdss.Name = "cmb_sdss";
			this.cmb_sdss.Size = new System.Drawing.Size(433, 21);
			this.cmb_sdss.TabIndex = 6;
			this.cmb_sdss.SelectedIndexChanged += new System.EventHandler(this.cmb_sdss_SelectedIndexChanged);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.lv_dupTargets);
			this.tabPage2.Controls.Add(this.g_dupAdd);
			this.tabPage2.Controls.Add(this.g_dupPrimary);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(445, 294);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Duplication";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// lv_dupTargets
			// 
			this.lv_dupTargets.AllColumns.Add(this.lvc_dupTargets_target);
			this.lv_dupTargets.AllColumns.Add(this.lvc_dupTargets_path);
			this.lv_dupTargets.AllColumns.Add(this.lvc_dupTargets_browse);
			this.lv_dupTargets.AllColumns.Add(this.lvc_dupTargets_dupFiles);
			this.lv_dupTargets.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.SingleClickAlways;
			this.lv_dupTargets.CellEditUseWholeCell = false;
			this.lv_dupTargets.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
			this.lvc_dupTargets_target,
			this.lvc_dupTargets_path,
			this.lvc_dupTargets_browse,
			this.lvc_dupTargets_dupFiles});
			this.lv_dupTargets.Cursor = System.Windows.Forms.Cursors.Default;
			this.lv_dupTargets.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lv_dupTargets.FullRowSelect = true;
			this.lv_dupTargets.GridLines = true;
			this.lv_dupTargets.HideSelection = false;
			this.lv_dupTargets.Location = new System.Drawing.Point(3, 81);
			this.lv_dupTargets.Name = "lv_dupTargets";
			this.lv_dupTargets.ShowFilterMenuOnRightClick = false;
			this.lv_dupTargets.ShowGroups = false;
			this.lv_dupTargets.Size = new System.Drawing.Size(439, 210);
			this.lv_dupTargets.TabIndex = 2;
			this.lv_dupTargets.UseCompatibleStateImageBehavior = false;
			this.lv_dupTargets.View = System.Windows.Forms.View.Details;
			this.lv_dupTargets.ButtonClick += new System.EventHandler<BrightIdeasSoftware.CellClickEventArgs>(this.lv_dupTargets_ButtonClick);
			// 
			// lvc_dupTargets_target
			// 
			this.lvc_dupTargets_target.AspectName = "Target";
			this.lvc_dupTargets_target.IsEditable = false;
			this.lvc_dupTargets_target.Text = "Target";
			this.lvc_dupTargets_target.Width = 123;
			// 
			// lvc_dupTargets_path
			// 
			this.lvc_dupTargets_path.AspectName = "RemotePath";
			this.lvc_dupTargets_path.Text = "Remote path";
			this.lvc_dupTargets_path.Width = 170;
			// 
			// lvc_dupTargets_browse
			// 
			this.lvc_dupTargets_browse.AspectName = "Target";
			this.lvc_dupTargets_browse.AspectToStringFormat = "Browse...";
			this.lvc_dupTargets_browse.ButtonSizing = BrightIdeasSoftware.OLVColumn.ButtonSizingMode.CellBounds;
			this.lvc_dupTargets_browse.IsButton = true;
			this.lvc_dupTargets_browse.IsEditable = false;
			this.lvc_dupTargets_browse.Text = "Browse...";
			this.lvc_dupTargets_browse.Width = 59;
			// 
			// lvc_dupTargets_dupFiles
			// 
			this.lvc_dupTargets_dupFiles.AspectName = "DuplicateFiles";
			this.lvc_dupTargets_dupFiles.CheckBoxes = true;
			this.lvc_dupTargets_dupFiles.Text = "Duplicate files";
			this.lvc_dupTargets_dupFiles.Width = 79;
			// 
			// g_dupAdd
			// 
			this.g_dupAdd.Controls.Add(this.b_duplication_change);
			this.g_dupAdd.Controls.Add(this.cmb_duplication_sdss);
			this.g_dupAdd.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_dupAdd.Location = new System.Drawing.Point(3, 35);
			this.g_dupAdd.Name = "g_dupAdd";
			this.g_dupAdd.Size = new System.Drawing.Size(439, 46);
			this.g_dupAdd.TabIndex = 1;
			this.g_dupAdd.TabStop = false;
			this.g_dupAdd.Text = "Add";
			// 
			// b_duplication_change
			// 
			this.b_duplication_change.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.b_duplication_change.Location = new System.Drawing.Point(374, 17);
			this.b_duplication_change.Name = "b_duplication_change";
			this.b_duplication_change.Size = new System.Drawing.Size(59, 23);
			this.b_duplication_change.TabIndex = 1;
			this.b_duplication_change.Text = "Change";
			this.b_duplication_change.UseVisualStyleBackColor = true;
			this.b_duplication_change.Click += new System.EventHandler(this.b_duplication_change_Click);
			// 
			// cmb_duplication_sdss
			// 
			this.cmb_duplication_sdss.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.cmb_duplication_sdss.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmb_duplication_sdss.FormattingEnabled = true;
			this.cmb_duplication_sdss.Location = new System.Drawing.Point(9, 19);
			this.cmb_duplication_sdss.Name = "cmb_duplication_sdss";
			this.cmb_duplication_sdss.Size = new System.Drawing.Size(359, 21);
			this.cmb_duplication_sdss.TabIndex = 0;
			// 
			// g_dupPrimary
			// 
			this.g_dupPrimary.Controls.Add(this.l_duplication_primary);
			this.g_dupPrimary.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_dupPrimary.Location = new System.Drawing.Point(3, 3);
			this.g_dupPrimary.Name = "g_dupPrimary";
			this.g_dupPrimary.Size = new System.Drawing.Size(439, 32);
			this.g_dupPrimary.TabIndex = 0;
			this.g_dupPrimary.TabStop = false;
			this.g_dupPrimary.Text = "Primary";
			// 
			// l_duplication_primary
			// 
			this.l_duplication_primary.AutoSize = true;
			this.l_duplication_primary.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.l_duplication_primary.Location = new System.Drawing.Point(6, 16);
			this.l_duplication_primary.Name = "l_duplication_primary";
			this.l_duplication_primary.Size = new System.Drawing.Size(15, 13);
			this.l_duplication_primary.TabIndex = 0;
			this.l_duplication_primary.Text = "[]";
			// 
			// f_addTorrent
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(453, 312);
			this.Controls.Add(this.tab_addTorrent);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "f_addTorrent";
			this.Text = "Add torrent...";
			this.Load += new System.EventHandler(this.f_addTorrent_Load);
			this.tab_addTorrent.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.g_clip.ResumeLayout(false);
			this.g_clip.PerformLayout();
			this.g_uri.ResumeLayout(false);
			this.g_uri.PerformLayout();
			this.g_file.ResumeLayout(false);
			this.g_file.PerformLayout();
			this.g_remoteDir.ResumeLayout(false);
			this.g_remoteDir.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.lv_dupTargets)).EndInit();
			this.g_dupAdd.ResumeLayout(false);
			this.g_dupPrimary.ResumeLayout(false);
			this.g_dupPrimary.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tab_addTorrent;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.CheckBox chk_fastResume;
		private System.Windows.Forms.GroupBox g_clip;
		private System.Windows.Forms.Button b_clipPreview;
		private System.Windows.Forms.RadioButton rb_clip;
		private System.Windows.Forms.CheckBox chk_start;
		private System.Windows.Forms.Button b_cancel;
		private System.Windows.Forms.Button b_ok;
		private System.Windows.Forms.GroupBox g_uri;
		private System.Windows.Forms.RadioButton rb_uri;
		private System.Windows.Forms.TextBox t_uri;
		private System.Windows.Forms.GroupBox g_file;
		private System.Windows.Forms.Button b_browse;
		private System.Windows.Forms.RadioButton rb_file;
		private System.Windows.Forms.TextBox t_file;
		private System.Windows.Forms.GroupBox g_remoteDir;
		private System.Windows.Forms.Button b_remoteBrowse;
		private System.Windows.Forms.TextBox t_remoteDir;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.ComboBox cmb_sdss;
		private System.Windows.Forms.TabPage tabPage2;
		private ObjectListView lv_dupTargets;
		private System.Windows.Forms.GroupBox g_dupPrimary;
		private System.Windows.Forms.GroupBox g_dupAdd;
		private System.Windows.Forms.Button b_duplication_change;
		private System.Windows.Forms.ComboBox cmb_duplication_sdss;
		private System.Windows.Forms.Label l_duplication_primary;
		private OLVColumn lvc_dupTargets_target;
		private OLVColumn lvc_dupTargets_path;
		private OLVColumn lvc_dupTargets_browse;
		private OLVColumn lvc_dupTargets_dupFiles;
	}
}
