﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Net;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using RTSharpIFace;
using static RTSharp.Global;
using static RTSharpIFace.Utils;

namespace RTSharp
{
	using System.Threading.Tasks;
	using static Torrent;

	static internal class Torrent
	{
		public static void TorrentUpdate(TORRENT orig, TORRENT repl)
		{
			orig.State = repl.State;
			orig.Done = repl.Done;
			orig.Downloaded = repl.Downloaded;
			orig.Uploaded = repl.Uploaded;
			orig.Ratio = repl.Ratio;

			if (repl.DLSpeed != orig.DLSpeed) {
				orig.DLSpeedHistory.Add(DateTime.Now, orig.DLSpeed);
				if (orig.DLSpeedHistory.Count > Settings.GraphPointsLimit)
					orig.DLSpeedHistory.Remove(orig.DLSpeedHistory.Min(x => x.Key));
			}
			if (repl.UPSpeed != orig.UPSpeed) {
				orig.UPSpeedHistory.Add(DateTime.Now, orig.UPSpeed);
				if (orig.UPSpeedHistory.Count > Settings.GraphPointsLimit)
					orig.UPSpeedHistory.Remove(orig.UPSpeedHistory.Min(x => x.Key));
			}

			orig.DLSpeed = repl.DLSpeed;
			orig.UPSpeed = repl.UPSpeed;
			orig.ETA = repl.ETA;
			orig.Label = repl.Label;
			orig.Peers = repl.Peers;
			orig.Seeders = repl.Seeders;
			orig.Priority = repl.Priority;
			orig.FinishedOnDate = repl.FinishedOnDate;
			orig.StatusMsg = repl.StatusMsg;
			orig.RemotePath = repl.RemotePath;
			orig.Name = repl.Name; // Magnet resolves
			orig.ChunkSize = repl.ChunkSize; // Magnet resolves
			orig.Size = repl.Size; // Magnet resolves
			orig.AddedOnDate = repl.AddedOnDate; // Magnet resolves

			if (orig.MagnetDummy != repl.MagnetDummy) {
				RTSharpPluginImports.EvMagnetResolved(repl);
				orig.MagnetDummy = repl.MagnetDummy;
			}
			
			if (repl.TrackerSingle != null)
				orig.TrackerSingle = repl.TrackerSingle;
		}

		public static IEnumerable<TORRENT> GetSelectedTorrents()
		{
			lock (TorrentsLock) {
				return Torrents.Where(x => x.CurrentlySelectedInUI);
			}
			//return Program.MainForm.lv_torrents.SelectedObjects.Cast<TORRENT>().ToArray();
		}

		public static Dictionary<ISDS, List<TORRENT>> MapTorrentsToSDSS(IEnumerable<TORRENT> Torrents)
		{
			return MapTorrentsToSDSS(Torrents.ToList());
		}

		public static Dictionary<ISDS, List<TORRENT>> MapTorrentsToSDSS(List<TORRENT> Torrents)
		{
			var list = new Dictionary<ISDS, List<TORRENT>>();

			foreach (var t in Torrents) {
				var sds = t.Owner;
				if (!list.ContainsKey(sds))
					list.Add(sds, new List<TORRENT>());

				list[sds].Add(t);
			}

			return list;
		}

		public static async Task TorrentActionWWaitingBox(string Text, Func<KeyValuePair<ISDS, List<TORRENT>>, Task> Fx, string TorrentsString = null, string TorrentsString2 = null)
		{
			var sel = GetSelectedTorrents();
			var sdss = MapTorrentsToSDSS(sel);

			var wait = new WaitingBox(
				Program.MainForm.lv_torrents.SelectedIndices.Count == 1 ?
					Text + " " + sdss.Values.ToArray()[0][0].Name + (TorrentsString ?? "") + "..." :
					Text + " " + sdss.Count + (TorrentsString2 ?? " torrents..."),
				ProgressBarStyle.Marquee, ProgressBarStyle.Marquee);
			wait.Show();

			foreach (var kv in sdss) {
				try {
					await Fx(kv);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Torrent action failed");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
			}

			wait.Close();
			Program.MainForm.updateTorrents = f_main.UPDATE_TORRENTS.UPDATE;
		}

		public static void PopulateFxs()
		{
			FormatFxs.Add(Tuple.Create(Guid.Empty, "T_State"), x => FToString.FormatState(x.Item1 ?? 0, x.Item2 ?? 0));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "T_Connection"), x => ((ISDS)x).FriendlyName);
			new[] { "T_Size", "T_Downloaded", "T_Uploaded", "T_Remaining" }
				.ToList()
				.ForEach(t => FormatFxs.Add(Tuple.Create(Guid.Empty, t), x => FToString.FormatSize(x ?? 0)));
			new[] { "T_DL Speed", "T_UP Speed" }
				.ToList()
				.ForEach(t => FormatFxs.Add(Tuple.Create(Guid.Empty, t), x => FToString.FormatSpeed(x ?? 0)));
			new[] { "T_Created On", "T_Added On", "T_Finished On" }
				.ToList()
				.ForEach(t => FormatFxs.Add(Tuple.Create(Guid.Empty, t), x => FToString.FormatUnixTimeStamp(x ?? 0, Settings.DateFormat)));
			new[] { "T_Seeders", "T_Peers" }
				.ToList()
				.ForEach(t => FormatFxs.Add(Tuple.Create(Guid.Empty, t), x => FToString.FormatConnectedTotal(x ?? 0)));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "T_Priority"), x => FToString.FormatTorrentPriority(x ?? 0));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "T_Tracker"), x => FToString.FormatTracker(x ?? "", Settings.TrackerMatches));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "T_Done"), x => FToString.FormatPercent(x ?? 0));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "T_Ratio"), x => FToString.FormatRatio(x ?? 0));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "T_ETA"), x => FToString.FormatAgo(x ?? 0));
		}
	}

	public partial class f_main {
		private void b_torrent_start_Click(object sender, EventArgs e)
		{
			foreach (var kv in MapTorrentsToSDSS(GetSelectedTorrents())) {
				try {
					kv.Key.Start(kv.Value.ToArray());
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to start torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
			}
			updateTorrents = UPDATE_TORRENTS.UPDATE;
		}

		private void b_torrent_pause_Click(object sender, EventArgs e)
		{
			foreach (var kv in MapTorrentsToSDSS(GetSelectedTorrents())) {
				try {
					kv.Key.Pause(kv.Value.ToArray());
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to pause torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
			}
			updateTorrents = UPDATE_TORRENTS.UPDATE;
		}

		private void b_torrent_stop_Click(object sender, EventArgs e)
		{
			foreach (var kv in MapTorrentsToSDSS(GetSelectedTorrents())) {
				try {
					kv.Key.Stop(kv.Value.ToArray());
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to stop torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
			}
			updateTorrents = UPDATE_TORRENTS.UPDATE;
		}

		private void b_torrent_force_recheck_Click(object sender, EventArgs e)
		{
			var res = MessageBox.Show(
				"Are you sure you want to force recheck selected torrents?",
				"RT#",
				MessageBoxButtons.YesNoCancel,
				MessageBoxIcon.Warning);

			if (res != DialogResult.Yes) {
				Logger.Log(LOG_LEVEL.INFO, "Refused to force recheck torrents");
				return;
			}

			foreach (var kv in MapTorrentsToSDSS(GetSelectedTorrents())) {
				try {
					kv.Key.ForceRecheck(kv.Value.ToArray());
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to force recheck torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
			}

			updateTorrents = UPDATE_TORRENTS.UPDATE;
		}

		private async void b_torrent_update_trackers_Click(object sender, EventArgs e)
		{
			var sel = GetSelectedTorrents();
			var selEnumer = sel as TORRENT[] ?? sel.ToArray();

			foreach (var kv in MapTorrentsToSDSS(selEnumer)) {
				try {
					await kv.Key.ReannounceAll(kv.Value.ToArray());
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to reannounce torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
				foreach (var x in kv.Value) {
					try {
						var t = (await kv.Key.GetTrackers(x));
						x.Trackers = t.Item1.ToList();
						x.TrackerSingle = t.Item2;
					} catch (Exception ex) {
						Logger.Log(LOG_LEVEL.ERROR, "Failed to reannounce torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ") (2)");
						Logger.LogException(LOG_LEVEL.ERROR, ex);
					}
				}
			}
			if (selEnumer.Length == 1)
				Program.MainForm.lv_trackers.SetObjects(selEnumer.First().Trackers);

			updateTorrents = UPDATE_TORRENTS.UPDATE;
		}

		private void b_torrent_add_peer_Click(object sender, EventArgs e)
		{
			var sel = GetSelectedTorrents();
			var selEnumer = sel as TORRENT[] ?? sel.ToArray();

			var sdss = MapTorrentsToSDSS(selEnumer);

			string ipPort;
			if (InputBox.Launch("RT#", "[ip]:[port]", false, out ipPort) != DialogResult.OK)
				return;

			if (ipPort.LastIndexOf(':') == -1) {
				MessageBox.Show("Invalid format", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			string sIp = ipPort.Remove(ipPort.LastIndexOf(':'));
			if (sIp.StartsWith("["))
				sIp = sIp.Substring(1, sIp.Length - 2);

			IPAddress ip;
			if (!IPAddress.TryParse(sIp, out ip)) {
				MessageBox.Show("Invalid format", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			ushort port;
			string sPort = ipPort.Remove(0, ipPort.LastIndexOf(':'));
			if (!UInt16.TryParse(sPort, out port)) {
				MessageBox.Show("Invalid format", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			bool @private = selEnumer.Any(x => x.State.HasFlag(TORRENT_STATE.PRIVATE));
			
			if (!@private) {
				foreach (var kv in sdss) {
					foreach (var kvv in kv.Value) {
						try {
							kv.Key.AddPeer(kvv, ip, port);
						} catch (Exception ex) {
							Logger.Log(LOG_LEVEL.ERROR, "Failed to add peer torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
							Logger.LogException(LOG_LEVEL.ERROR, ex);
						}
					}
				}
			} else {
				Logger.Log(LOG_LEVEL.WARN, "Blocked attempt to add peer to private torrent(s)");
			}
		}

		private void ctxms_torrent_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			b_torrent_add_peer.Enabled = !(GetSelectedTorrents().Any(t => (t.State & TORRENT_STATE.PRIVATE) == TORRENT_STATE.PRIVATE));

			var sdss = MapTorrentsToSDSS(GetSelectedTorrents());
			var opss = sdss.Select(x => x.Key.GetSupportedOperations());
			var opsEnumer = opss as SupportedOperations[] ?? opss.ToArray();

			if (!opsEnumer.All(x => x.torrents.GetTorrentFile))
				b_torrent_get_file.Enabled = false;
			if (!opsEnumer.All(x => x.torrents.Priority))
				l_set_priority.Enabled = false;
			if (!opsEnumer.All(x => x.torrents.EditTorrent))
				b_editTorrent.Enabled = false;
			if (!opsEnumer.All(x => x.files.ISFA))
				b_isfaTorrent.Enabled = false;

		}

		private void l_set_label_DropDownOpening(object sender, EventArgs e)
		{
			int erase = -1;
			int count = ((ToolStripMenuItem)sender).DropDown.Items.Count;
			for (int x = 0; x < count; x++) {
				if (erase != -1) {
					((ToolStripMenuItem)sender).DropDown.Items.RemoveAt(erase);
					continue;
				}
				if (((ToolStripMenuItem)sender).DropDown.Items[x].GetType() == typeof(ToolStripSeparator))
					erase = x + 1;
			}
			var labels = new Dictionary<string, int>();

			var sel = GetSelectedTorrents();

			IEnumerable<TORRENT> torrents;
			lock (TorrentsLock)
				torrents = Torrents.Where(x => sel.Any(y => y.Owner.UniqueGUID.Equals(x.Owner.UniqueGUID)));

			foreach (var t in torrents) {
				if (!String.IsNullOrEmpty(t.Label)) {
					if (!labels.ContainsKey(t.Label))
						labels.Add(t.Label, 0);
					labels[t.Label]++;
				}
			}

			labels.ToList().ForEach(x => {
				ToolStripItem ts = new ToolStripMenuItem();
				ts.Name = x.Key;
				ts.Text = x.Key + " (" + x.Value + ")";
				ts.Click += async (sender2, e2) => {
					await TorrentActionWWaitingBox("Settings label for", async kv => {
						await kv.Key.SetLabel(kv.Value, x.Key);
						kv.Value.ForEach(i => i.Label = x.Key);

						lv_torrents.RefreshObjects(kv.Value);
					});

					updateTorrents = UPDATE_TORRENTS.UPDATE;
				};

				((ToolStripMenuItem)sender).DropDown.Items.Add(ts);
			});
			var tstxt = new ToolStripTextBox();
			Debug.Assert(tstxt.TextBox != null);

			tstxt.TextBox.Text = "New...";
			tstxt.TextBox.ForeColor = SystemColors.InactiveCaptionText;
			tstxt.GotFocus += (sender2, e2) => {
				var box = ((ToolStripTextBox)sender2).TextBox;
				Debug.Assert(box != null);

				if (box.Text == "New..." || box.Text == "") {
					box.Text = "";
					box.ForeColor = SystemColors.ControlText;
				}
			};
			tstxt.LostFocus += (sender2, e2) => {
				var box = ((ToolStripTextBox)sender2).TextBox;
				Debug.Assert(box != null);

				if (box.Text != "")
					return;

				box.Text = "New...";
				box.ForeColor = SystemColors.InactiveCaptionText;
			};
			tstxt.KeyPress += async (sender2, e2) => {
				if (e2.KeyChar != '\r' && e2.KeyChar != '\n')
					return;

				await TorrentActionWWaitingBox("Settings label for", async kv => {
					var txt = ((ToolStripTextBox)sender2).TextBox;
					Debug.Assert(txt != null);

					await kv.Key.SetLabel(kv.Value, txt.Text);
					kv.Value.ForEach(x => x.Label = txt.Text);

					lv_torrents.RefreshObjects(kv.Value);
				});

				ctxms_torrent.Close(ToolStripDropDownCloseReason.ItemClicked);
				updateTorrents = UPDATE_TORRENTS.UPDATE;

				e2.Handled = true;
			};
			((ToolStripMenuItem)sender).DropDown.Items.Add(tstxt);
		}

		private async void b_torrent_remove_label_Click(object sender, EventArgs e)
		{
			await TorrentActionWWaitingBox("Removing label for", async kv => {
				await kv.Key.SetLabel(kv.Value, "");
				kv.Value.ForEach(x => x.Label = "");

				lv_torrents.RefreshObjects(kv.Value);
			});
		}

		async Task SetPriority(TORRENT_PRIORITY Priority)
		{
			await TorrentActionWWaitingBox("Setting " + Priority + " priority for", kv => kv.Key.SetPriority(kv.Value, Priority));
			updateTorrents = UPDATE_TORRENTS.UPDATE;
		}

		private async void b_torrent_set_prio_high_Click(object sender, EventArgs e) => await SetPriority(TORRENT_PRIORITY.HIGH);

		private async void b_torrent_set_prio_normal_Click(object sender, EventArgs e) => await SetPriority(TORRENT_PRIORITY.NORMAL);

		private async void b_torrent_set_prio_low_Click(object sender, EventArgs e) => await SetPriority(TORRENT_PRIORITY.LOW);

		private async void b_torrent_set_prio_off_Click(object sender, EventArgs e) => await SetPriority(TORRENT_PRIORITY.OFF);

		private async void b_torrent_remove_Click(object sender, EventArgs e)
		{
			var res = MessageBox.Show(
				"Are you sure you want to remove selected torrents?",
				"RT#",
				MessageBoxButtons.YesNoCancel,
				MessageBoxIcon.Warning);

			if (res != DialogResult.Yes)
				return;

			await TorrentActionWWaitingBox("Removing", kv => kv.Key.Remove(kv.Value));
			updateTorrents = UPDATE_TORRENTS.UPDATE;
		}

		private async void b_torrent_remove_data_Click(object sender, EventArgs e)
		{
			var res = MessageBox.Show(
				"Are you sure you want to remove selected torrents and their data?",
				"RT#",
				MessageBoxButtons.YesNoCancel,
				MessageBoxIcon.Warning);

			if (res != DialogResult.Yes)
				return;

			await TorrentActionWWaitingBox("Removing", kv => kv.Key.RemoveTorrentAndData(kv.Value), " torrent and data", " torrents and their data");
			updateTorrents = UPDATE_TORRENTS.UPDATE;
		}

		class TupleElementEqComparer<T, T2> : IEqualityComparer<Tuple<T, T2>>
		{
			readonly bool firstElement;
			public TupleElementEqComparer(bool FirstElement) {
				firstElement = FirstElement;
			}
			public bool Equals(Tuple<T, T2> x, Tuple<T, T2> y)
			{
				if (firstElement)
					return x.Item1.Equals(y.Item1);

				return x.Item2.Equals(y.Item2);
			}

			public int GetHashCode(Tuple<T, T2> obj)
			{
				if (firstElement)
					return obj.Item1.GetHashCode();

				return obj.Item2.GetHashCode();
			}
		}

		private async void b_torrent_get_file_Click(object sender, EventArgs e)
		{
			var t = GetSelectedTorrents();
			var selEnumer = t as TORRENT[] ?? t.ToArray();

			var sdss = MapTorrentsToSDSS(selEnumer);
			var errors = "";
			var sfd = new SaveFileDialog {
				Filter = selEnumer.Length == 1 ? "Torrent files (*.torrent)|*.torrent" : "Zip files (*.zip)|*.zip"
			};
			if (selEnumer.Length == 1)
				sfd.FileName = selEnumer.First().Name + ".torrent";

			var fbd = new FolderBrowserDialog();

			if (!Settings.SaveTorrentsInZip && selEnumer.Length != 1) {
				if (fbd.ShowDialog() != DialogResult.OK)
					return;
			} else {
				if (sfd.ShowDialog() != DialogResult.OK)
					return;
			}

			var wait = new WaitingBox(
				selEnumer.Length == 1 ?
					"Downloading .torrent..." :
					"Downloading " + selEnumer.Length + " torrents...",
				ProgressBarStyle.Blocks, ProgressBarStyle.Marquee);
			wait.Show();

			var files = new List<Tuple<byte[], string>>();

			foreach (var kv in sdss) {
				var sds = kv.Key;
				var torrents = kv.Value;
				
				try {
					var ts = await sds.GetTorrentFiles(torrents);
					files.AddRange(Enumerable.Range(0, ts.Count).Select(x => Tuple.Create(ts[x], torrents[x].Name)));
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to get torrent files of torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
					Logger.LogException(LOG_LEVEL.ERROR, ex);

					errors += "Failed to download files: " + ex;
				}
			}

			// Filter by hash
			files = files.Distinct(new TupleElementEqComparer<byte[], string>(true)).ToList();

			// Duplicate name, count
			var dups = files
				.Except(files.Distinct(new TupleElementEqComparer<byte[], string>(false)))
				.GroupBy(x => x.Item2)
				.Select(x => Tuple.Create(x.First().Item2, x.Count()));
			foreach (var kv in dups) {
				for (int x = 0;x < kv.Item2;x++) {
					var index = files.FindIndex(m => m.Item2 == kv.Item1);
					files[index] = Tuple.Create(files[index].Item1, files[index].Item2 + " (" + (x + 1) + ")");
				}
			}

			if (files.Count == 1) {
				try {
					System.IO.File.WriteAllBytes(sfd.FileName, files[0].Item1);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to save torrent file");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					errors += "Failed to save to file \"" + sfd.FileName + "\": " + ex;
				}
			} else {
				if (Settings.SaveTorrentsInZip) {
					try {
						using (var zipToOpen = new FileStream(sfd.FileName, FileMode.Create)) {
							using (var archive = new ZipArchive(zipToOpen, ZipArchiveMode.Create)) {
								for (int x = 0; x < files.Count; x++) {
									var name = files[x].Item2;
									var entry = archive.CreateEntry(name.Replace("\\", "_").Replace("/", "_") + ".torrent");

									using (var writer = new BinaryWriter(entry.Open()))
										writer.Write(files[x].Item1);

									wait.UpdatePBarSingle((byte)((double)x / files.Count * 100));
								}
							}
						}
					} catch (Exception ex) {
						Logger.Log(LOG_LEVEL.ERROR, "Failed to save zip file");
						Logger.LogException(LOG_LEVEL.ERROR, ex);
						errors += "Failed to save to file \"" + sfd.FileName + "\": " + ex;
					}
				} else {
					try {
						for (var x = 0;x < files.Count; x++) {
							Path.GetInvalidFileNameChars().ToList().ForEach(c => files[x] = Tuple.Create(files[x].Item1, files[x].Item2.Replace(c, '_')));
							System.IO.File.WriteAllBytes(files[x].Item2 + ".torrent", files[x].Item1);

							wait.UpdatePBarSingle((byte)((double)x / files.Count * 100));
						}
					} catch (Exception ex) {
						Logger.Log(LOG_LEVEL.ERROR, "Failed to save files");
						Logger.LogException(LOG_LEVEL.ERROR, ex);
						errors += "Failed to save files to \"" + fbd.SelectedPath + "\": " + ex;
					}
				}
			}

			wait.Close();

			if (!String.IsNullOrEmpty(errors)) {
				var resultsBox = new ResultsBox();
				resultsBox.Show(errors);
			}
		}

		private async void b_editTorrent_Click(object sender, EventArgs e)
		{
			var sel = GetSelectedTorrents();
			var selEnumer = sel as TORRENT[] ?? sel.ToArray();

			if (selEnumer.Length == 0 || selEnumer.First().Trackers?.Count == 0)
				return;

			var tComp = selEnumer.First().Trackers;
			var comp = new TrackerComparer(true);
			var valid = selEnumer.All(x => x.Trackers.SequenceEqual(tComp, comp));

			if (!valid) {
				MessageBox.Show("Selected torrents have different trackers", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			var edit = new f_editTorrent(selEnumer.First());
			var ok = edit.ShowDlg();
			if (ok) {
				var sdss = MapTorrentsToSDSS(selEnumer);
				foreach (var kv in sdss) {
					var trackers = edit.t_trackers.Text.Split(new[] { Environment.NewLine }, StringSplitOptions.None).AsEnumerable();
					try {
						await kv.Key.EditTorrent(kv.Value.Select(x => Tuple.Create(x, trackers)));
					} catch (Exception ex) {
						Logger.Log(LOG_LEVEL.ERROR, "Failed to edit torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
						Logger.LogException(LOG_LEVEL.ERROR, ex);
					}
				}
			}
		}

		private void lv_torrents_SelectionChanged(object sender, EventArgs e)
		{
			lock (SelectedTorrentsLock) {
				lock (TorrentsLock)
					RTSharpPluginImports.SetTorrentUISelect(Torrents, false);

				foreach(TORRENT t in lv_torrents.SelectedObjects) {
					var f = Torrents.Find(x => x == t);
					if (f == null) {
						Logger.Log(LOG_LEVEL.FATAL, "Loose torrent bug @ " + t.Owner.GUID + " [" + t.Owner.Name + "]");
					}
					Debug.Assert(Torrents.Find(x => x == t) != null);
				}

				RTSharpPluginImports.SetTorrentUISelect(lv_torrents.SelectedObjects.Cast<TORRENT>(), true);
			}

			var torrent = (TORRENT)lv_torrents.SelectedObject;

			if (torrent == null) {
				dtlv_files.DataSource = new List<FILES>();
				lv_peers.SetObjects(null);

				var sel = GetSelectedTorrents().Select(x => x.Trackers).ToList();
				var cmp = new TrackerComparer(true);
				var eq = sel.All(x => x.SequenceEqual(sel[0], cmp));
				if (eq && sel.Count != 0) {
					sel[0].ForEach(x => {
						x.Downloaded = x.Peers = x.Seeders = x.Interval = 0;
						x.LastUpdated = 0;
					});
					lv_trackers.SetObjects(sel[0]);
				}
				else
					lv_trackers.SetObjects(null);
				return;
			}

			lv_peers.SetObjects(torrent.PeerList);
			lv_trackers.SetObjects(torrent.Trackers);

			if (torrent.FileList == null)
				dtlv_files.DataSource = new List<FILES>();
			else {
				dtlv_files.DataSource = torrent.FileList;
				dtlv_files.Sort();
			}

			Program.MainForm.RefreshGeneralTab();
			Program.MainForm.RefreshGraphTab();
		}

		private async void b_torrent_set_data_directory_Click(object sender, EventArgs e)
		{
			var sel = GetSelectedTorrents();
			var sdss = MapTorrentsToSDSS(sel);

			foreach (var kv in sdss) {
				var selDir = new SelectRemoteFolder(kv.Key);
				var path = selDir.ShowDialogWResult();
				if (String.IsNullOrEmpty(path))
					continue;

				var wbox = new WaitingBox("Setting download directory...", ProgressBarStyle.Marquee, ProgressBarStyle.Marquee);
				wbox.Show();

				try {
					await kv.Key.SetTorrentDataDirectory(kv.Value, path);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to set data directory for torrent(s) in SDS " + kv.Key.FriendlyName + " (" + kv.Key.UniqueGUID + ")");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}

				wbox.Close();
			}
		}

		private async void b_isfaTorrent_Click(object sender, EventArgs e)
		{
			var torrents = GetSelectedTorrents();

			var sdsSelect = new SDSSelect(torrents.Select(x => x.Owner).Distinct());
			var resSelect = await sdsSelect.ShowDialog();
			if (resSelect != DialogResult.OK)
				return;
			
			var target = sdsSelect.SDS;

			if (target == null)
				return;

			var remoteSelect = new SelectRemoteFolder(target);
			var targetpath = remoteSelect.ShowDialogWResult();
			if (String.IsNullOrEmpty(targetpath))
				return;
			if (!targetpath.EndsWith("/"))
				targetpath += "/";

			foreach (var torrent in torrents)
				ISFAQueue.AddQueue(
					torrent,
					(await torrent.Owner.GetTorrentFiles(new[] { torrent }.ToList())).First(),
					new[] { new ISFAQueue.DUP_TARGET(SDSS.Where(x => x.SDS.UniqueGUID.Equals(target.UniqueGUID)).First(),
						targetpath,
						true)
					}.ToList()
				);
		}

		private void b_isfaQueue_Click(object sender, EventArgs e)
		{
			var torrent = GetSelectedTorrents().FirstOrDefault();

			if (torrent == null)
				return;

			var mup = ISFAQueue.GetQueue(torrent);

			if (mup != null && !mup.IsDisposed)
				Utils.InvokeOpt(() => mup.Show());
		}
	}
}
