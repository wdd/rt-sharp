﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;

namespace RTSharpIFace {
	public partial class Utils {
		public static byte[] StringToByteArray(string hex) {
			if (hex.Length % 2 == 1)
				throw new Exception("The binary key cannot have an odd number of digits");

			byte[] arr = new byte[hex.Length >> 1];

			for (int i = 0; i < (hex.Length >> 1); ++i) {
				arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
			}

			return arr;
		}

		public static string ByteArrayToHexString(byte[] bytes) {
			char[] c = new char[bytes.Length * 2];
			int b;
			for (int i = 0; i < bytes.Length; i++) {
				b = bytes[i] >> 4;
				c[i * 2] = (char)(55 + b + (((b-10)>>31)&-7));
				b = bytes[i] & 0xF;
				c[i * 2 + 1] = (char)(55 + b + (((b-10)>>31)&-7));
			}
			return new string(c);
		}

		public static int GetHexVal(char hex) {
			int val = hex;
			return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
		}

		public static string GetSizeSI(ulong In) {
			if (In < 1024)
				return In + " B";
			if (In < 1024 * 1024)
				return Math.Round((float)In / 1024, 3) + " KiB";
			if (In < 1024 * 1024 * 1024)
				return Math.Round((float)In / 1024 / 1024, 3) + " MiB";
			if (In < (ulong)1024 * 1024 * 1024 * 1024)
				return Math.Round((float)In / 1024 / 1024 / 1024, 3) + " GiB";
			if (In < (ulong)1024 * 1024 * 1024 * 1024 * 1024)
				return Math.Round((float)In / 1024 / 1024 / 1024 / 1024, 3) + " TiB";
			if (In < (ulong)1024 * 1024 * 1024 * 1024 * 1024 * 1024)
				return Math.Round((float)In / 1024 / 1024 / 1024 / 1024 / 1024, 3) + " PiB";
			return Math.Round((float)In / 1024 / 1024 / 1024 / 1024 / 1024 / 1024, 3) + " EiB";
		}

		public static string GetSizeSI(long In) {
			return GetSizeSI((ulong)In);
		}

		public class ByteArrayComparer : IEqualityComparer<byte[]>
		{
			public bool Equals(byte[] left, byte[] right)
			{
				if (left == null || right == null) {
					return left == right;
				}
				return left.SequenceEqual(right);
			}
			public int GetHashCode(byte[] key)
			{
				if (key == null)
					throw new ArgumentNullException(nameof(key));
				return key.Sum(x => x);
			}
		}

		public static DateTime UnixTimeStampToDateTime(ulong unixTimeStamp) {
			return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(unixTimeStamp).ToLocalTime();
		}

		public static ulong UnixTimeStamp() {
			return (ulong)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
		}

		public static string ToAgoString(TimeSpan In) {
			if (In.Days > 0)
				return $"{In.Days}d {In.Hours}h {In.Minutes}m {In.Seconds}s";
			if (In.Hours > 0)
				return $"{In.Hours}h {In.Minutes}m {In.Seconds}s";
			return In.Minutes > 0 ?
				$"{In.Minutes}m {In.Seconds:00}s" :
				$"{In.Seconds}s";
		}

		public static string GetFileWildcard(string In) {
			return GetFilesWildcard(In)[0];
		}

		public static string[] GetFilesWildcard(string In) {
			return Directory.GetFiles(Path.GetDirectoryName(In), Path.GetFileName(In));
		}

		public static string PathCombine(string Base, string Concat)
		{
			bool linux = Base.StartsWith("/");

			if (Base.EndsWith(linux ? "/" : "\\"))
				Base += Concat;
			else
				Base += (linux ? "/" : "\\") + Concat;

			return Base;
		}

		public static byte[] GetBytesASCII(string In)
		{
			byte[] ret = new byte[In.Length];
			var ca = In.ToCharArray();
			for (int x = 0;x < In.Length;x++)
				ret[x] = (byte)ca[x];
			return ret;
		}

		public static string GetStringASCII(byte[] In)
		{
			char[] chars = new char[In.Length];
			for (int x = 0;x < In.Length;x++)
				chars[x] = (char)In[x];
			return new string(chars);
		}
	}
}
