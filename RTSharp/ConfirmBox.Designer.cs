﻿namespace RTSharp {
	partial class f_confirmBox {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(f_confirmBox));
			this.l_text = new System.Windows.Forms.Label();
			this.t_details = new System.Windows.Forms.TextBox();
			this.b_yes = new System.Windows.Forms.Button();
			this.b_no = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// l_text
			// 
			this.l_text.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.l_text.Location = new System.Drawing.Point(12, 9);
			this.l_text.Name = "l_text";
			this.l_text.Size = new System.Drawing.Size(388, 43);
			this.l_text.TabIndex = 0;
			this.l_text.Text = "label1";
			this.l_text.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// t_details
			// 
			this.t_details.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_details.Location = new System.Drawing.Point(12, 55);
			this.t_details.Multiline = true;
			this.t_details.Name = "t_details";
			this.t_details.ReadOnly = true;
			this.t_details.Size = new System.Drawing.Size(388, 164);
			this.t_details.TabIndex = 1;
			// 
			// b_yes
			// 
			this.b_yes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.b_yes.Location = new System.Drawing.Point(244, 225);
			this.b_yes.Name = "b_yes";
			this.b_yes.Size = new System.Drawing.Size(75, 23);
			this.b_yes.TabIndex = 2;
			this.b_yes.Text = "Yes";
			this.b_yes.UseVisualStyleBackColor = true;
			this.b_yes.Click += new System.EventHandler(this.b_yes_Click);
			// 
			// b_no
			// 
			this.b_no.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.b_no.Location = new System.Drawing.Point(325, 225);
			this.b_no.Name = "b_no";
			this.b_no.Size = new System.Drawing.Size(75, 23);
			this.b_no.TabIndex = 3;
			this.b_no.Text = "No";
			this.b_no.UseVisualStyleBackColor = true;
			this.b_no.Click += new System.EventHandler(this.b_no_Click);
			// 
			// f_confirmBox
			// 
			this.AcceptButton = this.b_no;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(412, 260);
			this.Controls.Add(this.b_no);
			this.Controls.Add(this.b_yes);
			this.Controls.Add(this.t_details);
			this.Controls.Add(this.l_text);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(189, 201);
			this.Name = "f_confirmBox";
			this.Text = "RT# - Confirm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.f_confirmBox_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label l_text;
		private System.Windows.Forms.TextBox t_details;
		private System.Windows.Forms.Button b_yes;
		private System.Windows.Forms.Button b_no;
	}
}