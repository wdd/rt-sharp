﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using BrightIdeasSoftware;

namespace RTSharp {
	partial class f_main {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(f_main));
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 1000D);
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.041666666666666664D, 2000D);
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.20833333333333334D, 5000D);
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.25D, 25000D);
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.29166666666666669D, 2000D);
			System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, "5000,0,0,0");
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.125D, "8000,0,0,0");
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.16666666666666666D, "12000,0,0,0");
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.20833333333333334D, "50000,0,0,0");
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.29166666666666669D, "3000,0,0,0");
			this.menu_main = new System.Windows.Forms.MenuStrip();
			this.menu_file = new System.Windows.Forms.ToolStripMenuItem();
			this.b_addTorrent = new System.Windows.Forms.ToolStripMenuItem();
			this.b_transferTorrentData = new System.Windows.Forms.ToolStripMenuItem();
			this.sep_file_1 = new System.Windows.Forms.ToolStripSeparator();
			this.b_settings = new System.Windows.Forms.ToolStripMenuItem();
			this.sep_file_2 = new System.Windows.Forms.ToolStripSeparator();
			this.b_about = new System.Windows.Forms.ToolStripMenuItem();
			this.b_exit = new System.Windows.Forms.ToolStripMenuItem();
			this.b_sdss = new System.Windows.Forms.ToolStripMenuItem();
			this.b_plugins = new System.Windows.Forms.ToolStripMenuItem();
			this.b_refresh = new System.Windows.Forms.ToolStripMenuItem();
			this.b_pause = new System.Windows.Forms.ToolStripMenuItem();
			this.b_resume = new System.Windows.Forms.ToolStripMenuItem();
			this.t_filter = new RTSharp.ToolStripTextBoxWPlaceholder();
			this.status_main = new System.Windows.Forms.StatusStrip();
			this.l_status = new System.Windows.Forms.ToolStripStatusLabel();
			this.l_statusSeperator = new System.Windows.Forms.ToolStripStatusLabel();
			this.ctxms_torrent = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.b_isfaQueue = new System.Windows.Forms.ToolStripMenuItem();
			this.l_sep1 = new System.Windows.Forms.ToolStripSeparator();
			this.b_torrent_start = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_pause = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_stop = new System.Windows.Forms.ToolStripMenuItem();
			this.l_sep2 = new System.Windows.Forms.ToolStripSeparator();
			this.b_torrent_force_recheck = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_update_trackers = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_add_peer = new System.Windows.Forms.ToolStripMenuItem();
			this.l_sep3 = new System.Windows.Forms.ToolStripSeparator();
			this.l_set_label = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_remove_label = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.l_set_priority = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_set_prio_high = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_set_prio_normal = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_set_prio_low = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_set_prio_off = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_set_data_directory = new System.Windows.Forms.ToolStripMenuItem();
			this.l_sep4 = new System.Windows.Forms.ToolStripSeparator();
			this.b_torrent_remove = new System.Windows.Forms.ToolStripMenuItem();
			this.b_torrent_remove_data = new System.Windows.Forms.ToolStripMenuItem();
			this.l_sep5 = new System.Windows.Forms.ToolStripSeparator();
			this.b_torrent_get_file = new System.Windows.Forms.ToolStripMenuItem();
			this.b_copy_info_hash = new System.Windows.Forms.ToolStripMenuItem();
			this.b_editTorrent = new System.Windows.Forms.ToolStripMenuItem();
			this.b_isfaTorrent = new System.Windows.Forms.ToolStripMenuItem();
			this.ImgList = new System.Windows.Forms.ImageList(this.components);
			this.split_torrents = new System.Windows.Forms.SplitContainer();
			this.lv_torrents = new BrightIdeasSoftware.FastObjectListView();
			this.lvc_name = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_owner = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_state = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_size = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_done = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_downloaded = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_remaining_size = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_uploaded = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_dlspeed = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_upspeed = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_eta = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_ratio = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_created_on = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_added_on = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_finished_on = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_label = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_peers = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_seeders = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_priority = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lvc_tracker = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.tab_main = new System.Windows.Forms.TabControl();
			this.tab_general = new System.Windows.Forms.TabPage();
			this.g_info = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.l_remotePath = new System.Windows.Forms.Label();
			this.l_createdOn = new System.Windows.Forms.Label();
			this.l_addedOn = new System.Windows.Forms.Label();
			this.l_hash = new System.Windows.Forms.Label();
			this.l_valRemotePath = new System.Windows.Forms.Label();
			this.l_valCreatedOn = new System.Windows.Forms.Label();
			this.l_valAddedOn = new System.Windows.Forms.Label();
			this.l_valInfoHash = new System.Windows.Forms.Label();
			this.l_comment = new System.Windows.Forms.Label();
			this.l_valComment = new System.Windows.Forms.Label();
			this.l_lStatus = new System.Windows.Forms.Label();
			this.l_valStatus = new System.Windows.Forms.Label();
			this.g_transfer = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.l_timeElapsed = new System.Windows.Forms.Label();
			this.l_downloaded = new System.Windows.Forms.Label();
			this.l_uploaded = new System.Windows.Forms.Label();
			this.l_seeders = new System.Windows.Forms.Label();
			this.l_valTimeElapsed = new System.Windows.Forms.Label();
			this.l_valUploaded = new System.Windows.Forms.Label();
			this.l_valDownloaded = new System.Windows.Forms.Label();
			this.l_valSeeders = new System.Windows.Forms.Label();
			this.l_remaining = new System.Windows.Forms.Label();
			this.l_upSpeed = new System.Windows.Forms.Label();
			this.l_dlSpeed = new System.Windows.Forms.Label();
			this.l_peers = new System.Windows.Forms.Label();
			this.l_valRemaining = new System.Windows.Forms.Label();
			this.l_valDlSpeed = new System.Windows.Forms.Label();
			this.l_valUpSpeed = new System.Windows.Forms.Label();
			this.l_valPeers = new System.Windows.Forms.Label();
			this.l_ratio = new System.Windows.Forms.Label();
			this.l_wasted = new System.Windows.Forms.Label();
			this.l_valRatio = new System.Windows.Forms.Label();
			this.l_valWasted = new System.Windows.Forms.Label();
			this.tab_files = new System.Windows.Forms.TabPage();
			this.dtlv_files = new BrightIdeasSoftware.DataTreeListView();
			this.dtlv_files_name = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.dtlv_files_size = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.dtlv_files_downloaded = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.dtlv_files_done = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.dtlv_files_priority = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.ctxms_file = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.l_priority = new System.Windows.Forms.ToolStripMenuItem();
			this.b_setPriority_high = new System.Windows.Forms.ToolStripMenuItem();
			this.b_setPriority_normal = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
			this.b_setPriority_dontDownload = new System.Windows.Forms.ToolStripMenuItem();
			this.l_downloadStrategy = new System.Windows.Forms.ToolStripMenuItem();
			this.b_setDownloadStretegy_normal = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
			this.b_setDownloadStrategy_leading = new System.Windows.Forms.ToolStripMenuItem();
			this.b_setDownloadStrategy_trailing = new System.Windows.Forms.ToolStripMenuItem();
			this.l_sepFile1 = new System.Windows.Forms.ToolStripSeparator();
			this.b_mediaInfo = new System.Windows.Forms.ToolStripMenuItem();
			this.b_downloadFile = new System.Windows.Forms.ToolStripMenuItem();
			this.tab_trackers = new System.Windows.Forms.TabPage();
			this.lv_trackers = new BrightIdeasSoftware.FastObjectListView();
			this.lv_trackers_uri = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_trackers_status = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_trackers_seeders = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_trackers_peers = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_trackers_downloaded = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_trackers_last_updated = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_trackers_interval = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_trackers_message = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.ctxms_trackers = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.b_tracker_enable = new System.Windows.Forms.ToolStripMenuItem();
			this.b_tracker_disable = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
			this.b_reannounceTracker = new System.Windows.Forms.ToolStripMenuItem();
			this.tab_peers = new System.Windows.Forms.TabPage();
			this.lv_peers = new BrightIdeasSoftware.FastObjectListView();
			this.lv_peers_flag = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_peers_ip = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_peers_client = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_peers_flags = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_peers_done = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_peers_dlSpeed = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_peers_upSpeed = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_peers_downloaded = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.lv_peers_uploaded = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.ctxms_peer = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.b_ban_peer = new System.Windows.Forms.ToolStripMenuItem();
			this.b_kick_peer = new System.Windows.Forms.ToolStripMenuItem();
			this.b_snub_peer = new System.Windows.Forms.ToolStripMenuItem();
			this.b_unsnub_peer = new System.Windows.Forms.ToolStripMenuItem();
			this.CompanyImgs = new System.Windows.Forms.ImageList(this.components);
			this.tab_graph = new System.Windows.Forms.TabPage();
			this.graph = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.ni_global = new System.Windows.Forms.NotifyIcon(this.components);
			this.lv_sdss = new BrightIdeasSoftware.ObjectListView();
			this.olv_sdss_main = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			this.ctxms_tracker = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.b_set_alias = new System.Windows.Forms.ToolStripMenuItem();
			this.b_fetch_icon = new System.Windows.Forms.ToolStripMenuItem();
			this.t_animate = new System.Windows.Forms.Timer(this.components);
			this.ctxms_connection = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.b_setSDSHighlighting = new System.Windows.Forms.ToolStripMenuItem();
			this.tabs_right = new System.Windows.Forms.TabControl();
			this.tab_sdss = new System.Windows.Forms.TabPage();
			this.menu_main.SuspendLayout();
			this.status_main.SuspendLayout();
			this.ctxms_torrent.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.split_torrents)).BeginInit();
			this.split_torrents.Panel1.SuspendLayout();
			this.split_torrents.Panel2.SuspendLayout();
			this.split_torrents.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.lv_torrents)).BeginInit();
			this.tab_main.SuspendLayout();
			this.tab_general.SuspendLayout();
			this.g_info.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.g_transfer.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.tab_files.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dtlv_files)).BeginInit();
			this.ctxms_file.SuspendLayout();
			this.tab_trackers.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.lv_trackers)).BeginInit();
			this.ctxms_trackers.SuspendLayout();
			this.tab_peers.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.lv_peers)).BeginInit();
			this.ctxms_peer.SuspendLayout();
			this.tab_graph.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.graph)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lv_sdss)).BeginInit();
			this.ctxms_tracker.SuspendLayout();
			this.ctxms_connection.SuspendLayout();
			this.tabs_right.SuspendLayout();
			this.tab_sdss.SuspendLayout();
			this.SuspendLayout();
			// 
			// menu_main
			// 
			this.menu_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_file,
            this.b_sdss,
            this.b_plugins,
            this.b_refresh,
            this.b_pause,
            this.b_resume,
            this.t_filter});
			this.menu_main.Location = new System.Drawing.Point(0, 0);
			this.menu_main.Name = "menu_main";
			this.menu_main.Size = new System.Drawing.Size(1444, 27);
			this.menu_main.TabIndex = 2;
			this.menu_main.Text = "menuStrip1";
			// 
			// menu_file
			// 
			this.menu_file.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_addTorrent,
            this.b_transferTorrentData,
            this.sep_file_1,
            this.b_settings,
            this.sep_file_2,
            this.b_about,
            this.b_exit});
			this.menu_file.Name = "menu_file";
			this.menu_file.Size = new System.Drawing.Size(37, 23);
			this.menu_file.Text = "File";
			// 
			// b_addTorrent
			// 
			this.b_addTorrent.Name = "b_addTorrent";
			this.b_addTorrent.Size = new System.Drawing.Size(239, 22);
			this.b_addTorrent.Text = "Add torrent...";
			this.b_addTorrent.Click += new System.EventHandler(this.b_addTorrent_Click);
			// 
			// b_transferTorrentData
			// 
			this.b_transferTorrentData.Name = "b_transferTorrentData";
			this.b_transferTorrentData.Size = new System.Drawing.Size(239, 22);
			this.b_transferTorrentData.Text = "Transfer torrent data to server...";
			this.b_transferTorrentData.Click += new System.EventHandler(this.transferTorrentDataToServerToolStripMenuItem_Click);
			// 
			// sep_file_1
			// 
			this.sep_file_1.Name = "sep_file_1";
			this.sep_file_1.Size = new System.Drawing.Size(236, 6);
			// 
			// b_settings
			// 
			this.b_settings.Name = "b_settings";
			this.b_settings.Size = new System.Drawing.Size(239, 22);
			this.b_settings.Text = "Settings";
			this.b_settings.Click += new System.EventHandler(this.b_settings_Click);
			// 
			// sep_file_2
			// 
			this.sep_file_2.Name = "sep_file_2";
			this.sep_file_2.Size = new System.Drawing.Size(236, 6);
			// 
			// b_about
			// 
			this.b_about.Name = "b_about";
			this.b_about.Size = new System.Drawing.Size(239, 22);
			this.b_about.Text = "About";
			this.b_about.Click += new System.EventHandler(this.b_about_Click);
			// 
			// b_exit
			// 
			this.b_exit.Name = "b_exit";
			this.b_exit.Size = new System.Drawing.Size(239, 22);
			this.b_exit.Text = "Exit";
			this.b_exit.Click += new System.EventHandler(this.b_exit_Click);
			// 
			// b_sdss
			// 
			this.b_sdss.Name = "b_sdss";
			this.b_sdss.Size = new System.Drawing.Size(86, 23);
			this.b_sdss.Text = "Connections";
			this.b_sdss.Click += new System.EventHandler(this.b_sdss_Click);
			// 
			// b_plugins
			// 
			this.b_plugins.Name = "b_plugins";
			this.b_plugins.Size = new System.Drawing.Size(58, 23);
			this.b_plugins.Text = "Plugins";
			this.b_plugins.Click += new System.EventHandler(this.b_plugins_Click);
			// 
			// b_refresh
			// 
			this.b_refresh.Image = ((System.Drawing.Image)(resources.GetObject("b_refresh.Image")));
			this.b_refresh.Name = "b_refresh";
			this.b_refresh.Size = new System.Drawing.Size(74, 23);
			this.b_refresh.Text = "Refresh";
			this.b_refresh.Click += new System.EventHandler(this.b_refresh_Click);
			// 
			// b_pause
			// 
			this.b_pause.Image = ((System.Drawing.Image)(resources.GetObject("b_pause.Image")));
			this.b_pause.Name = "b_pause";
			this.b_pause.Size = new System.Drawing.Size(69, 23);
			this.b_pause.Text = "Pause ";
			this.b_pause.Click += new System.EventHandler(this.b_pause_Click);
			// 
			// b_resume
			// 
			this.b_resume.Image = ((System.Drawing.Image)(resources.GetObject("b_resume.Image")));
			this.b_resume.Name = "b_resume";
			this.b_resume.Size = new System.Drawing.Size(77, 23);
			this.b_resume.Text = "Resume";
			this.b_resume.Visible = false;
			this.b_resume.Click += new System.EventHandler(this.b_resume_Click);
			// 
			// t_filter
			// 
			this.t_filter.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.t_filter.BackColor = System.Drawing.SystemColors.Window;
			this.t_filter.ForeColor = System.Drawing.SystemColors.GrayText;
			this.t_filter.Name = "t_filter";
			this.t_filter.Placeholder = "Filter...";
			this.t_filter.Size = new System.Drawing.Size(100, 23);
			this.t_filter.Text = "Filter...";
			// 
			// status_main
			// 
			this.status_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.l_status,
            this.l_statusSeperator});
			this.status_main.Location = new System.Drawing.Point(0, 391);
			this.status_main.Name = "status_main";
			this.status_main.Size = new System.Drawing.Size(1444, 22);
			this.status_main.TabIndex = 3;
			this.status_main.Text = "statusStrip1";
			// 
			// l_status
			// 
			this.l_status.Name = "l_status";
			this.l_status.Size = new System.Drawing.Size(39, 17);
			this.l_status.Text = "Ready";
			// 
			// l_statusSeperator
			// 
			this.l_statusSeperator.Name = "l_statusSeperator";
			this.l_statusSeperator.Size = new System.Drawing.Size(1390, 17);
			this.l_statusSeperator.Spring = true;
			// 
			// ctxms_torrent
			// 
			this.ctxms_torrent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_isfaQueue,
            this.l_sep1,
            this.b_torrent_start,
            this.b_torrent_pause,
            this.b_torrent_stop,
            this.l_sep2,
            this.b_torrent_force_recheck,
            this.b_torrent_update_trackers,
            this.b_torrent_add_peer,
            this.l_sep3,
            this.l_set_label,
            this.l_set_priority,
            this.b_torrent_set_data_directory,
            this.l_sep4,
            this.b_torrent_remove,
            this.b_torrent_remove_data,
            this.l_sep5,
            this.b_torrent_get_file,
            this.b_copy_info_hash,
            this.b_editTorrent,
            this.b_isfaTorrent});
			this.ctxms_torrent.Name = "ctxms_torrent";
			this.ctxms_torrent.Size = new System.Drawing.Size(214, 386);
			this.ctxms_torrent.Opening += new System.ComponentModel.CancelEventHandler(this.ctxms_torrent_Opening);
			// 
			// b_isfaQueue
			// 
			this.b_isfaQueue.Name = "b_isfaQueue";
			this.b_isfaQueue.Size = new System.Drawing.Size(213, 22);
			this.b_isfaQueue.Text = "Show duplication &queue";
			this.b_isfaQueue.Click += new System.EventHandler(this.b_isfaQueue_Click);
			// 
			// l_sep1
			// 
			this.l_sep1.Name = "l_sep1";
			this.l_sep1.Size = new System.Drawing.Size(210, 6);
			// 
			// b_torrent_start
			// 
			this.b_torrent_start.Name = "b_torrent_start";
			this.b_torrent_start.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_start.Text = "&Start";
			this.b_torrent_start.Click += new System.EventHandler(this.b_torrent_start_Click);
			// 
			// b_torrent_pause
			// 
			this.b_torrent_pause.Name = "b_torrent_pause";
			this.b_torrent_pause.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_pause.Text = "&Pause";
			this.b_torrent_pause.Click += new System.EventHandler(this.b_torrent_pause_Click);
			// 
			// b_torrent_stop
			// 
			this.b_torrent_stop.Name = "b_torrent_stop";
			this.b_torrent_stop.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_stop.Text = "S&top";
			this.b_torrent_stop.Click += new System.EventHandler(this.b_torrent_stop_Click);
			// 
			// l_sep2
			// 
			this.l_sep2.Name = "l_sep2";
			this.l_sep2.Size = new System.Drawing.Size(210, 6);
			// 
			// b_torrent_force_recheck
			// 
			this.b_torrent_force_recheck.Name = "b_torrent_force_recheck";
			this.b_torrent_force_recheck.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_force_recheck.Text = "Force &recheck";
			this.b_torrent_force_recheck.Click += new System.EventHandler(this.b_torrent_force_recheck_Click);
			// 
			// b_torrent_update_trackers
			// 
			this.b_torrent_update_trackers.Name = "b_torrent_update_trackers";
			this.b_torrent_update_trackers.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_update_trackers.Text = "&Reannounce to all trackers";
			this.b_torrent_update_trackers.Click += new System.EventHandler(this.b_torrent_update_trackers_Click);
			// 
			// b_torrent_add_peer
			// 
			this.b_torrent_add_peer.Name = "b_torrent_add_peer";
			this.b_torrent_add_peer.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_add_peer.Text = "Add &peer...";
			this.b_torrent_add_peer.Click += new System.EventHandler(this.b_torrent_add_peer_Click);
			// 
			// l_sep3
			// 
			this.l_sep3.Name = "l_sep3";
			this.l_sep3.Size = new System.Drawing.Size(210, 6);
			// 
			// l_set_label
			// 
			this.l_set_label.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_torrent_remove_label,
            this.toolStripSeparator1});
			this.l_set_label.Name = "l_set_label";
			this.l_set_label.Size = new System.Drawing.Size(213, 22);
			this.l_set_label.Text = "Set &label...";
			this.l_set_label.DropDownOpening += new System.EventHandler(this.l_set_label_DropDownOpening);
			// 
			// b_torrent_remove_label
			// 
			this.b_torrent_remove_label.Name = "b_torrent_remove_label";
			this.b_torrent_remove_label.Size = new System.Drawing.Size(145, 22);
			this.b_torrent_remove_label.Text = "Remove label";
			this.b_torrent_remove_label.Click += new System.EventHandler(this.b_torrent_remove_label_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(142, 6);
			// 
			// l_set_priority
			// 
			this.l_set_priority.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_torrent_set_prio_high,
            this.b_torrent_set_prio_normal,
            this.b_torrent_set_prio_low,
            this.b_torrent_set_prio_off});
			this.l_set_priority.Name = "l_set_priority";
			this.l_set_priority.Size = new System.Drawing.Size(213, 22);
			this.l_set_priority.Text = "Set priorit&y...";
			// 
			// b_torrent_set_prio_high
			// 
			this.b_torrent_set_prio_high.Name = "b_torrent_set_prio_high";
			this.b_torrent_set_prio_high.Size = new System.Drawing.Size(114, 22);
			this.b_torrent_set_prio_high.Text = "High";
			this.b_torrent_set_prio_high.Click += new System.EventHandler(this.b_torrent_set_prio_high_Click);
			// 
			// b_torrent_set_prio_normal
			// 
			this.b_torrent_set_prio_normal.Name = "b_torrent_set_prio_normal";
			this.b_torrent_set_prio_normal.Size = new System.Drawing.Size(114, 22);
			this.b_torrent_set_prio_normal.Text = "Normal";
			this.b_torrent_set_prio_normal.Click += new System.EventHandler(this.b_torrent_set_prio_normal_Click);
			// 
			// b_torrent_set_prio_low
			// 
			this.b_torrent_set_prio_low.Name = "b_torrent_set_prio_low";
			this.b_torrent_set_prio_low.Size = new System.Drawing.Size(114, 22);
			this.b_torrent_set_prio_low.Text = "Low";
			this.b_torrent_set_prio_low.Click += new System.EventHandler(this.b_torrent_set_prio_low_Click);
			// 
			// b_torrent_set_prio_off
			// 
			this.b_torrent_set_prio_off.Name = "b_torrent_set_prio_off";
			this.b_torrent_set_prio_off.Size = new System.Drawing.Size(114, 22);
			this.b_torrent_set_prio_off.Text = "Off";
			this.b_torrent_set_prio_off.Click += new System.EventHandler(this.b_torrent_set_prio_off_Click);
			// 
			// b_torrent_set_data_directory
			// 
			this.b_torrent_set_data_directory.Name = "b_torrent_set_data_directory";
			this.b_torrent_set_data_directory.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_set_data_directory.Text = "Set download directory...";
			this.b_torrent_set_data_directory.Click += new System.EventHandler(this.b_torrent_set_data_directory_Click);
			// 
			// l_sep4
			// 
			this.l_sep4.Name = "l_sep4";
			this.l_sep4.Size = new System.Drawing.Size(210, 6);
			// 
			// b_torrent_remove
			// 
			this.b_torrent_remove.Name = "b_torrent_remove";
			this.b_torrent_remove.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_remove.Text = "Remo&ve torrent";
			this.b_torrent_remove.Click += new System.EventHandler(this.b_torrent_remove_Click);
			// 
			// b_torrent_remove_data
			// 
			this.b_torrent_remove_data.Name = "b_torrent_remove_data";
			this.b_torrent_remove_data.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_remove_data.Text = "Remove torrent && &data";
			this.b_torrent_remove_data.Click += new System.EventHandler(this.b_torrent_remove_data_Click);
			// 
			// l_sep5
			// 
			this.l_sep5.Name = "l_sep5";
			this.l_sep5.Size = new System.Drawing.Size(210, 6);
			// 
			// b_torrent_get_file
			// 
			this.b_torrent_get_file.Name = "b_torrent_get_file";
			this.b_torrent_get_file.Size = new System.Drawing.Size(213, 22);
			this.b_torrent_get_file.Text = "Get .torrent";
			this.b_torrent_get_file.Click += new System.EventHandler(this.b_torrent_get_file_Click);
			// 
			// b_copy_info_hash
			// 
			this.b_copy_info_hash.Name = "b_copy_info_hash";
			this.b_copy_info_hash.Size = new System.Drawing.Size(213, 22);
			this.b_copy_info_hash.Text = "Copy info hash";
			this.b_copy_info_hash.Click += new System.EventHandler(this.b_copy_info_hash_Click);
			// 
			// b_editTorrent
			// 
			this.b_editTorrent.Name = "b_editTorrent";
			this.b_editTorrent.Size = new System.Drawing.Size(213, 22);
			this.b_editTorrent.Text = "Edit torrent...";
			this.b_editTorrent.Click += new System.EventHandler(this.b_editTorrent_Click);
			// 
			// b_isfaTorrent
			// 
			this.b_isfaTorrent.Name = "b_isfaTorrent";
			this.b_isfaTorrent.Size = new System.Drawing.Size(213, 22);
			this.b_isfaTorrent.Text = "Duplicate torrent to...";
			this.b_isfaTorrent.Click += new System.EventHandler(this.b_isfaTorrent_Click);
			// 
			// ImgList
			// 
			this.ImgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgList.ImageStream")));
			this.ImgList.TransparentColor = System.Drawing.Color.Transparent;
			this.ImgList.Images.SetKeyName(0, "ac.menuaro[1].gif");
			this.ImgList.Images.SetKeyName(1, "wtable_deselect_all[1].gif");
			this.ImgList.Images.SetKeyName(2, "IMG_UPLOAD");
			this.ImgList.Images.SetKeyName(3, "IMG_DOWNLOAD");
			this.ImgList.Images.SetKeyName(4, "IMG_STOPPED");
			this.ImgList.Images.SetKeyName(5, "IMG_ACTIVE");
			this.ImgList.Images.SetKeyName(6, "IMG_INACTIVE");
			this.ImgList.Images.SetKeyName(7, "IMG_ERRORED");
			this.ImgList.Images.SetKeyName(8, "label[1].png");
			this.ImgList.Images.SetKeyName(9, "722456[1].png");
			this.ImgList.Images.SetKeyName(10, "IMG_CONNECTED");
			this.ImgList.Images.SetKeyName(11, "IMG_DISCONNECTED");
			// 
			// split_torrents
			// 
			this.split_torrents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.split_torrents.Location = new System.Drawing.Point(0, 27);
			this.split_torrents.Name = "split_torrents";
			this.split_torrents.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// split_torrents.Panel1
			// 
			this.split_torrents.Panel1.Controls.Add(this.lv_torrents);
			// 
			// split_torrents.Panel2
			// 
			this.split_torrents.Panel2.Controls.Add(this.tab_main);
			this.split_torrents.Size = new System.Drawing.Size(1420, 364);
			this.split_torrents.SplitterDistance = 142;
			this.split_torrents.TabIndex = 2;
			// 
			// lv_torrents
			// 
			this.lv_torrents.AllColumns.Add(this.lvc_name);
			this.lv_torrents.AllColumns.Add(this.lvc_owner);
			this.lv_torrents.AllColumns.Add(this.lvc_state);
			this.lv_torrents.AllColumns.Add(this.lvc_size);
			this.lv_torrents.AllColumns.Add(this.lvc_done);
			this.lv_torrents.AllColumns.Add(this.lvc_downloaded);
			this.lv_torrents.AllColumns.Add(this.lvc_remaining_size);
			this.lv_torrents.AllColumns.Add(this.lvc_uploaded);
			this.lv_torrents.AllColumns.Add(this.lvc_dlspeed);
			this.lv_torrents.AllColumns.Add(this.lvc_upspeed);
			this.lv_torrents.AllColumns.Add(this.lvc_eta);
			this.lv_torrents.AllColumns.Add(this.lvc_ratio);
			this.lv_torrents.AllColumns.Add(this.lvc_created_on);
			this.lv_torrents.AllColumns.Add(this.lvc_added_on);
			this.lv_torrents.AllColumns.Add(this.lvc_finished_on);
			this.lv_torrents.AllColumns.Add(this.lvc_label);
			this.lv_torrents.AllColumns.Add(this.lvc_peers);
			this.lv_torrents.AllColumns.Add(this.lvc_seeders);
			this.lv_torrents.AllColumns.Add(this.lvc_priority);
			this.lv_torrents.AllColumns.Add(this.lvc_tracker);
			this.lv_torrents.AllowColumnReorder = true;
			this.lv_torrents.AutoArrange = false;
			this.lv_torrents.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lv_torrents.CausesValidation = false;
			this.lv_torrents.CellEditUseWholeCell = false;
			this.lv_torrents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvc_name,
            this.lvc_owner,
            this.lvc_state,
            this.lvc_size,
            this.lvc_done,
            this.lvc_downloaded,
            this.lvc_remaining_size,
            this.lvc_uploaded,
            this.lvc_dlspeed,
            this.lvc_upspeed,
            this.lvc_eta,
            this.lvc_ratio,
            this.lvc_created_on,
            this.lvc_added_on,
            this.lvc_finished_on,
            this.lvc_label,
            this.lvc_peers,
            this.lvc_seeders,
            this.lvc_priority,
            this.lvc_tracker});
			this.lv_torrents.CopySelectionOnControlCUsesDragSource = false;
			this.lv_torrents.Cursor = System.Windows.Forms.Cursors.Default;
			this.lv_torrents.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lv_torrents.EmptyListMsg = "No torrents";
			this.lv_torrents.FullRowSelect = true;
			this.lv_torrents.HasCollapsibleGroups = false;
			this.lv_torrents.HideSelection = false;
			this.lv_torrents.IsSearchOnSortColumn = false;
			this.lv_torrents.LabelWrap = false;
			this.lv_torrents.Location = new System.Drawing.Point(0, 0);
			this.lv_torrents.Name = "lv_torrents";
			this.lv_torrents.PersistentCheckBoxes = false;
			this.lv_torrents.ShowGroups = false;
			this.lv_torrents.ShowHeaderInAllViews = false;
			this.lv_torrents.Size = new System.Drawing.Size(1420, 142);
			this.lv_torrents.SmallImageList = this.ImgList;
			this.lv_torrents.Sorting = System.Windows.Forms.SortOrder.Descending;
			this.lv_torrents.TabIndex = 0;
			this.lv_torrents.TabStop = false;
			this.lv_torrents.TriggerCellOverEventsWhenOverHeader = false;
			this.lv_torrents.UpdateSpaceFillingColumnsWhenDraggingColumnDivider = false;
			this.lv_torrents.UseCellFormatEvents = true;
			this.lv_torrents.UseCompatibleStateImageBehavior = false;
			this.lv_torrents.UseFilterIndicator = true;
			this.lv_torrents.UseFiltering = true;
			this.lv_torrents.UseHotControls = false;
			this.lv_torrents.UseNotifyPropertyChanged = true;
			this.lv_torrents.UseOverlays = false;
			this.lv_torrents.View = System.Windows.Forms.View.Details;
			this.lv_torrents.VirtualMode = true;
			this.lv_torrents.CellRightClick += new System.EventHandler<BrightIdeasSoftware.CellRightClickEventArgs>(this.lv_torrents_CellRightClick);
			this.lv_torrents.Filter += new System.EventHandler<BrightIdeasSoftware.FilterEventArgs>(this.OLVFilter);
			this.lv_torrents.FormatCell += new System.EventHandler<BrightIdeasSoftware.FormatCellEventArgs>(this.lv_torrents_FormatCell);
			this.lv_torrents.FormatRow += new System.EventHandler<BrightIdeasSoftware.FormatRowEventArgs>(this.lv_torrents_FormatRow);
			this.lv_torrents.SelectionChanged += new System.EventHandler(this.lv_torrents_SelectionChanged);
			// 
			// lvc_name
			// 
			this.lvc_name.AspectName = "";
			this.lvc_name.Text = "Name";
			this.lvc_name.Width = 150;
			// 
			// lvc_owner
			// 
			this.lvc_owner.Text = "Connection";
			// 
			// lvc_state
			// 
			this.lvc_state.AspectName = "";
			this.lvc_state.Text = "State";
			this.lvc_state.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// lvc_size
			// 
			this.lvc_size.AspectName = "";
			this.lvc_size.Searchable = false;
			this.lvc_size.Text = "Size";
			this.lvc_size.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lvc_done
			// 
			this.lvc_done.AspectName = "";
			this.lvc_done.Text = "Done";
			// 
			// lvc_downloaded
			// 
			this.lvc_downloaded.AspectName = "";
			this.lvc_downloaded.Searchable = false;
			this.lvc_downloaded.Text = "Downloaded";
			this.lvc_downloaded.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lvc_remaining_size
			// 
			this.lvc_remaining_size.AspectName = "";
			this.lvc_remaining_size.Searchable = false;
			this.lvc_remaining_size.Text = "Remaining";
			// 
			// lvc_uploaded
			// 
			this.lvc_uploaded.AspectName = "";
			this.lvc_uploaded.Searchable = false;
			this.lvc_uploaded.Text = "Uploaded";
			this.lvc_uploaded.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lvc_dlspeed
			// 
			this.lvc_dlspeed.AspectName = "";
			this.lvc_dlspeed.Searchable = false;
			this.lvc_dlspeed.Text = "DL Speed";
			this.lvc_dlspeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lvc_upspeed
			// 
			this.lvc_upspeed.AspectName = "";
			this.lvc_upspeed.Searchable = false;
			this.lvc_upspeed.Text = "UP Speed";
			this.lvc_upspeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lvc_eta
			// 
			this.lvc_eta.AspectName = "";
			this.lvc_eta.Searchable = false;
			this.lvc_eta.Text = "ETA";
			// 
			// lvc_ratio
			// 
			this.lvc_ratio.AspectName = "";
			this.lvc_ratio.Text = "Ratio";
			// 
			// lvc_created_on
			// 
			this.lvc_created_on.AspectName = "";
			this.lvc_created_on.Searchable = false;
			this.lvc_created_on.Text = "Created On";
			// 
			// lvc_added_on
			// 
			this.lvc_added_on.AspectName = "";
			this.lvc_added_on.Searchable = false;
			this.lvc_added_on.Text = "Added On";
			// 
			// lvc_finished_on
			// 
			this.lvc_finished_on.AspectName = "";
			this.lvc_finished_on.Searchable = false;
			this.lvc_finished_on.Text = "Finished On";
			// 
			// lvc_label
			// 
			this.lvc_label.AspectName = "";
			this.lvc_label.Text = "Label";
			// 
			// lvc_peers
			// 
			this.lvc_peers.AspectName = "";
			this.lvc_peers.Searchable = false;
			this.lvc_peers.Text = "Peers";
			this.lvc_peers.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// lvc_seeders
			// 
			this.lvc_seeders.AspectName = "";
			this.lvc_seeders.Searchable = false;
			this.lvc_seeders.Text = "Seeders";
			this.lvc_seeders.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// lvc_priority
			// 
			this.lvc_priority.AspectName = "";
			this.lvc_priority.Text = "Priority";
			// 
			// lvc_tracker
			// 
			this.lvc_tracker.AspectName = "";
			this.lvc_tracker.Text = "Tracker";
			// 
			// tab_main
			// 
			this.tab_main.Controls.Add(this.tab_general);
			this.tab_main.Controls.Add(this.tab_files);
			this.tab_main.Controls.Add(this.tab_trackers);
			this.tab_main.Controls.Add(this.tab_peers);
			this.tab_main.Controls.Add(this.tab_graph);
			this.tab_main.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tab_main.Location = new System.Drawing.Point(0, 0);
			this.tab_main.Name = "tab_main";
			this.tab_main.SelectedIndex = 0;
			this.tab_main.Size = new System.Drawing.Size(1420, 218);
			this.tab_main.TabIndex = 5;
			// 
			// tab_general
			// 
			this.tab_general.Controls.Add(this.g_info);
			this.tab_general.Controls.Add(this.g_transfer);
			this.tab_general.Location = new System.Drawing.Point(4, 22);
			this.tab_general.Name = "tab_general";
			this.tab_general.Size = new System.Drawing.Size(1412, 192);
			this.tab_general.TabIndex = 3;
			this.tab_general.Text = "General";
			this.tab_general.UseVisualStyleBackColor = true;
			// 
			// g_info
			// 
			this.g_info.Controls.Add(this.tableLayoutPanel2);
			this.g_info.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_info.Location = new System.Drawing.Point(0, 83);
			this.g_info.Name = "g_info";
			this.g_info.Size = new System.Drawing.Size(1412, 208);
			this.g_info.TabIndex = 2;
			this.g_info.TabStop = false;
			this.g_info.Text = "Torrent info";
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 4;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Controls.Add(this.l_remotePath, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.l_createdOn, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.l_addedOn, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.l_hash, 0, 3);
			this.tableLayoutPanel2.Controls.Add(this.l_valRemotePath, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.l_valCreatedOn, 1, 1);
			this.tableLayoutPanel2.Controls.Add(this.l_valAddedOn, 1, 2);
			this.tableLayoutPanel2.Controls.Add(this.l_valInfoHash, 1, 3);
			this.tableLayoutPanel2.Controls.Add(this.l_comment, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.l_valComment, 3, 0);
			this.tableLayoutPanel2.Controls.Add(this.l_lStatus, 2, 1);
			this.tableLayoutPanel2.Controls.Add(this.l_valStatus, 3, 1);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 5;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.Size = new System.Drawing.Size(1406, 189);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// l_remotePath
			// 
			this.l_remotePath.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_remotePath.Location = new System.Drawing.Point(3, 0);
			this.l_remotePath.Name = "l_remotePath";
			this.l_remotePath.Size = new System.Drawing.Size(114, 16);
			this.l_remotePath.TabIndex = 0;
			this.l_remotePath.Text = "Remote path: ";
			// 
			// l_createdOn
			// 
			this.l_createdOn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_createdOn.Location = new System.Drawing.Point(3, 16);
			this.l_createdOn.Name = "l_createdOn";
			this.l_createdOn.Size = new System.Drawing.Size(114, 16);
			this.l_createdOn.TabIndex = 1;
			this.l_createdOn.Text = "Created on: ";
			// 
			// l_addedOn
			// 
			this.l_addedOn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_addedOn.Location = new System.Drawing.Point(3, 32);
			this.l_addedOn.Name = "l_addedOn";
			this.l_addedOn.Size = new System.Drawing.Size(114, 16);
			this.l_addedOn.TabIndex = 2;
			this.l_addedOn.Text = "Added On: ";
			// 
			// l_hash
			// 
			this.l_hash.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_hash.Location = new System.Drawing.Point(3, 48);
			this.l_hash.Name = "l_hash";
			this.l_hash.Size = new System.Drawing.Size(114, 16);
			this.l_hash.TabIndex = 3;
			this.l_hash.Text = "Info hash: ";
			// 
			// l_valRemotePath
			// 
			this.l_valRemotePath.AutoSize = true;
			this.l_valRemotePath.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valRemotePath.Location = new System.Drawing.Point(123, 0);
			this.l_valRemotePath.Name = "l_valRemotePath";
			this.l_valRemotePath.Size = new System.Drawing.Size(577, 16);
			this.l_valRemotePath.TabIndex = 5;
			// 
			// l_valCreatedOn
			// 
			this.l_valCreatedOn.AutoSize = true;
			this.l_valCreatedOn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valCreatedOn.Location = new System.Drawing.Point(123, 16);
			this.l_valCreatedOn.Name = "l_valCreatedOn";
			this.l_valCreatedOn.Size = new System.Drawing.Size(577, 16);
			this.l_valCreatedOn.TabIndex = 6;
			// 
			// l_valAddedOn
			// 
			this.l_valAddedOn.AutoSize = true;
			this.l_valAddedOn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valAddedOn.Location = new System.Drawing.Point(123, 32);
			this.l_valAddedOn.Name = "l_valAddedOn";
			this.l_valAddedOn.Size = new System.Drawing.Size(577, 16);
			this.l_valAddedOn.TabIndex = 7;
			// 
			// l_valInfoHash
			// 
			this.l_valInfoHash.AutoSize = true;
			this.l_valInfoHash.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valInfoHash.Location = new System.Drawing.Point(123, 48);
			this.l_valInfoHash.Name = "l_valInfoHash";
			this.l_valInfoHash.Size = new System.Drawing.Size(577, 16);
			this.l_valInfoHash.TabIndex = 8;
			// 
			// l_comment
			// 
			this.l_comment.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_comment.Location = new System.Drawing.Point(706, 0);
			this.l_comment.Name = "l_comment";
			this.l_comment.Size = new System.Drawing.Size(114, 16);
			this.l_comment.TabIndex = 12;
			this.l_comment.Text = "Comment: ";
			// 
			// l_valComment
			// 
			this.l_valComment.AutoSize = true;
			this.l_valComment.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valComment.Location = new System.Drawing.Point(826, 0);
			this.l_valComment.Name = "l_valComment";
			this.l_valComment.Size = new System.Drawing.Size(577, 16);
			this.l_valComment.TabIndex = 13;
			// 
			// l_lStatus
			// 
			this.l_lStatus.AutoSize = true;
			this.l_lStatus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_lStatus.Location = new System.Drawing.Point(706, 16);
			this.l_lStatus.Name = "l_lStatus";
			this.l_lStatus.Size = new System.Drawing.Size(114, 16);
			this.l_lStatus.TabIndex = 14;
			this.l_lStatus.Text = "Status: ";
			// 
			// l_valStatus
			// 
			this.l_valStatus.AutoSize = true;
			this.l_valStatus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valStatus.Location = new System.Drawing.Point(826, 16);
			this.l_valStatus.Name = "l_valStatus";
			this.l_valStatus.Size = new System.Drawing.Size(577, 16);
			this.l_valStatus.TabIndex = 15;
			// 
			// g_transfer
			// 
			this.g_transfer.Controls.Add(this.tableLayoutPanel1);
			this.g_transfer.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_transfer.Location = new System.Drawing.Point(0, 0);
			this.g_transfer.Name = "g_transfer";
			this.g_transfer.Size = new System.Drawing.Size(1412, 83);
			this.g_transfer.TabIndex = 1;
			this.g_transfer.TabStop = false;
			this.g_transfer.Text = "Transfer";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AllowDrop = true;
			this.tableLayoutPanel1.ColumnCount = 6;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.Controls.Add(this.l_timeElapsed, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.l_downloaded, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.l_uploaded, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.l_seeders, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.l_valTimeElapsed, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.l_valUploaded, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.l_valDownloaded, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.l_valSeeders, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.l_remaining, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.l_upSpeed, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.l_dlSpeed, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.l_peers, 2, 3);
			this.tableLayoutPanel1.Controls.Add(this.l_valRemaining, 3, 0);
			this.tableLayoutPanel1.Controls.Add(this.l_valDlSpeed, 3, 1);
			this.tableLayoutPanel1.Controls.Add(this.l_valUpSpeed, 3, 2);
			this.tableLayoutPanel1.Controls.Add(this.l_valPeers, 3, 3);
			this.tableLayoutPanel1.Controls.Add(this.l_ratio, 4, 0);
			this.tableLayoutPanel1.Controls.Add(this.l_wasted, 4, 1);
			this.tableLayoutPanel1.Controls.Add(this.l_valRatio, 5, 0);
			this.tableLayoutPanel1.Controls.Add(this.l_valWasted, 5, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 4;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1406, 64);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// l_timeElapsed
			// 
			this.l_timeElapsed.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_timeElapsed.Location = new System.Drawing.Point(3, 0);
			this.l_timeElapsed.Name = "l_timeElapsed";
			this.l_timeElapsed.Size = new System.Drawing.Size(114, 16);
			this.l_timeElapsed.TabIndex = 0;
			this.l_timeElapsed.Text = "Time elapsed: ";
			// 
			// l_downloaded
			// 
			this.l_downloaded.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_downloaded.Location = new System.Drawing.Point(3, 16);
			this.l_downloaded.Name = "l_downloaded";
			this.l_downloaded.Size = new System.Drawing.Size(114, 16);
			this.l_downloaded.TabIndex = 1;
			this.l_downloaded.Text = "Downloaded: ";
			// 
			// l_uploaded
			// 
			this.l_uploaded.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_uploaded.Location = new System.Drawing.Point(3, 32);
			this.l_uploaded.Name = "l_uploaded";
			this.l_uploaded.Size = new System.Drawing.Size(114, 16);
			this.l_uploaded.TabIndex = 2;
			this.l_uploaded.Text = "Uploaded: ";
			// 
			// l_seeders
			// 
			this.l_seeders.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_seeders.Location = new System.Drawing.Point(3, 48);
			this.l_seeders.Name = "l_seeders";
			this.l_seeders.Size = new System.Drawing.Size(114, 16);
			this.l_seeders.TabIndex = 3;
			this.l_seeders.Text = "Seeders: ";
			// 
			// l_valTimeElapsed
			// 
			this.l_valTimeElapsed.AutoSize = true;
			this.l_valTimeElapsed.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valTimeElapsed.Location = new System.Drawing.Point(123, 0);
			this.l_valTimeElapsed.Name = "l_valTimeElapsed";
			this.l_valTimeElapsed.Size = new System.Drawing.Size(342, 16);
			this.l_valTimeElapsed.TabIndex = 4;
			// 
			// l_valUploaded
			// 
			this.l_valUploaded.AutoSize = true;
			this.l_valUploaded.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valUploaded.Location = new System.Drawing.Point(123, 32);
			this.l_valUploaded.Name = "l_valUploaded";
			this.l_valUploaded.Size = new System.Drawing.Size(342, 16);
			this.l_valUploaded.TabIndex = 6;
			// 
			// l_valDownloaded
			// 
			this.l_valDownloaded.AutoSize = true;
			this.l_valDownloaded.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valDownloaded.Location = new System.Drawing.Point(123, 16);
			this.l_valDownloaded.Name = "l_valDownloaded";
			this.l_valDownloaded.Size = new System.Drawing.Size(342, 16);
			this.l_valDownloaded.TabIndex = 5;
			// 
			// l_valSeeders
			// 
			this.l_valSeeders.AutoSize = true;
			this.l_valSeeders.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valSeeders.Location = new System.Drawing.Point(123, 48);
			this.l_valSeeders.Name = "l_valSeeders";
			this.l_valSeeders.Size = new System.Drawing.Size(342, 16);
			this.l_valSeeders.TabIndex = 7;
			// 
			// l_remaining
			// 
			this.l_remaining.AutoSize = true;
			this.l_remaining.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_remaining.Location = new System.Drawing.Point(471, 0);
			this.l_remaining.Name = "l_remaining";
			this.l_remaining.Size = new System.Drawing.Size(114, 16);
			this.l_remaining.TabIndex = 8;
			this.l_remaining.Text = "Remaining: ";
			// 
			// l_upSpeed
			// 
			this.l_upSpeed.AutoSize = true;
			this.l_upSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_upSpeed.Location = new System.Drawing.Point(471, 32);
			this.l_upSpeed.Name = "l_upSpeed";
			this.l_upSpeed.Size = new System.Drawing.Size(114, 16);
			this.l_upSpeed.TabIndex = 10;
			this.l_upSpeed.Text = "UP Speed: ";
			// 
			// l_dlSpeed
			// 
			this.l_dlSpeed.AutoSize = true;
			this.l_dlSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_dlSpeed.Location = new System.Drawing.Point(471, 16);
			this.l_dlSpeed.Name = "l_dlSpeed";
			this.l_dlSpeed.Size = new System.Drawing.Size(114, 16);
			this.l_dlSpeed.TabIndex = 9;
			this.l_dlSpeed.Text = "DL Speed: ";
			// 
			// l_peers
			// 
			this.l_peers.AutoSize = true;
			this.l_peers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_peers.Location = new System.Drawing.Point(471, 48);
			this.l_peers.Name = "l_peers";
			this.l_peers.Size = new System.Drawing.Size(114, 16);
			this.l_peers.TabIndex = 11;
			this.l_peers.Text = "Peers: ";
			// 
			// l_valRemaining
			// 
			this.l_valRemaining.AutoSize = true;
			this.l_valRemaining.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valRemaining.Location = new System.Drawing.Point(591, 0);
			this.l_valRemaining.Name = "l_valRemaining";
			this.l_valRemaining.Size = new System.Drawing.Size(342, 16);
			this.l_valRemaining.TabIndex = 12;
			// 
			// l_valDlSpeed
			// 
			this.l_valDlSpeed.AutoSize = true;
			this.l_valDlSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valDlSpeed.Location = new System.Drawing.Point(591, 16);
			this.l_valDlSpeed.Name = "l_valDlSpeed";
			this.l_valDlSpeed.Size = new System.Drawing.Size(342, 16);
			this.l_valDlSpeed.TabIndex = 13;
			// 
			// l_valUpSpeed
			// 
			this.l_valUpSpeed.AutoSize = true;
			this.l_valUpSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valUpSpeed.Location = new System.Drawing.Point(591, 32);
			this.l_valUpSpeed.Name = "l_valUpSpeed";
			this.l_valUpSpeed.Size = new System.Drawing.Size(342, 16);
			this.l_valUpSpeed.TabIndex = 14;
			// 
			// l_valPeers
			// 
			this.l_valPeers.AutoSize = true;
			this.l_valPeers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valPeers.Location = new System.Drawing.Point(591, 48);
			this.l_valPeers.Name = "l_valPeers";
			this.l_valPeers.Size = new System.Drawing.Size(342, 16);
			this.l_valPeers.TabIndex = 15;
			// 
			// l_ratio
			// 
			this.l_ratio.AutoSize = true;
			this.l_ratio.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_ratio.Location = new System.Drawing.Point(939, 0);
			this.l_ratio.Name = "l_ratio";
			this.l_ratio.Size = new System.Drawing.Size(114, 16);
			this.l_ratio.TabIndex = 16;
			this.l_ratio.Text = "Ratio: ";
			// 
			// l_wasted
			// 
			this.l_wasted.AutoSize = true;
			this.l_wasted.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_wasted.Location = new System.Drawing.Point(939, 16);
			this.l_wasted.Name = "l_wasted";
			this.l_wasted.Size = new System.Drawing.Size(114, 16);
			this.l_wasted.TabIndex = 17;
			this.l_wasted.Text = "Wasted: ";
			// 
			// l_valRatio
			// 
			this.l_valRatio.AutoSize = true;
			this.l_valRatio.Location = new System.Drawing.Point(1059, 0);
			this.l_valRatio.Name = "l_valRatio";
			this.l_valRatio.Size = new System.Drawing.Size(0, 13);
			this.l_valRatio.TabIndex = 18;
			// 
			// l_valWasted
			// 
			this.l_valWasted.AutoSize = true;
			this.l_valWasted.Dock = System.Windows.Forms.DockStyle.Fill;
			this.l_valWasted.Location = new System.Drawing.Point(1059, 16);
			this.l_valWasted.Name = "l_valWasted";
			this.l_valWasted.Size = new System.Drawing.Size(344, 16);
			this.l_valWasted.TabIndex = 19;
			// 
			// tab_files
			// 
			this.tab_files.Controls.Add(this.dtlv_files);
			this.tab_files.Location = new System.Drawing.Point(4, 22);
			this.tab_files.Name = "tab_files";
			this.tab_files.Padding = new System.Windows.Forms.Padding(3);
			this.tab_files.Size = new System.Drawing.Size(1412, 192);
			this.tab_files.TabIndex = 0;
			this.tab_files.Text = "Files";
			this.tab_files.UseVisualStyleBackColor = true;
			// 
			// dtlv_files
			// 
			this.dtlv_files.AllColumns.Add(this.dtlv_files_name);
			this.dtlv_files.AllColumns.Add(this.dtlv_files_size);
			this.dtlv_files.AllColumns.Add(this.dtlv_files_downloaded);
			this.dtlv_files.AllColumns.Add(this.dtlv_files_done);
			this.dtlv_files.AllColumns.Add(this.dtlv_files_priority);
			this.dtlv_files.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dtlv_files.CellEditUseWholeCell = false;
			this.dtlv_files.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dtlv_files_name,
            this.dtlv_files_size,
            this.dtlv_files_downloaded,
            this.dtlv_files_done,
            this.dtlv_files_priority});
			this.dtlv_files.ContextMenuStrip = this.ctxms_file;
			this.dtlv_files.Cursor = System.Windows.Forms.Cursors.Default;
			this.dtlv_files.DataSource = null;
			this.dtlv_files.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dtlv_files.FullRowSelect = true;
			this.dtlv_files.Location = new System.Drawing.Point(3, 3);
			this.dtlv_files.Name = "dtlv_files";
			this.dtlv_files.RootKeyValueString = "";
			this.dtlv_files.ShowGroups = false;
			this.dtlv_files.Size = new System.Drawing.Size(1406, 186);
			this.dtlv_files.TabIndex = 0;
			this.dtlv_files.UseCellFormatEvents = true;
			this.dtlv_files.UseCompatibleStateImageBehavior = false;
			this.dtlv_files.View = System.Windows.Forms.View.Details;
			this.dtlv_files.VirtualMode = true;
			this.dtlv_files.FormatCell += new System.EventHandler<BrightIdeasSoftware.FormatCellEventArgs>(this.dtlv_files_FormatCell);
			this.dtlv_files.DoubleClick += new System.EventHandler(this.dtlv_files_DoubleClick);
			// 
			// dtlv_files_name
			// 
			this.dtlv_files_name.AspectName = "";
			this.dtlv_files_name.Text = "Name";
			// 
			// dtlv_files_size
			// 
			this.dtlv_files_size.AspectName = "";
			this.dtlv_files_size.Text = "Size";
			// 
			// dtlv_files_downloaded
			// 
			this.dtlv_files_downloaded.AspectName = "";
			this.dtlv_files_downloaded.Text = "Downloaded";
			// 
			// dtlv_files_done
			// 
			this.dtlv_files_done.AspectName = "";
			this.dtlv_files_done.Text = "Done";
			// 
			// dtlv_files_priority
			// 
			this.dtlv_files_priority.AspectName = "";
			this.dtlv_files_priority.Text = "Priority";
			// 
			// ctxms_file
			// 
			this.ctxms_file.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.l_priority,
            this.l_downloadStrategy,
            this.l_sepFile1,
            this.b_mediaInfo,
            this.b_downloadFile});
			this.ctxms_file.Name = "ctxms_file";
			this.ctxms_file.Size = new System.Drawing.Size(174, 98);
			this.ctxms_file.Opening += new System.ComponentModel.CancelEventHandler(this.ctxms_file_Opening);
			// 
			// l_priority
			// 
			this.l_priority.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_setPriority_high,
            this.b_setPriority_normal,
            this.toolStripSeparator7,
            this.b_setPriority_dontDownload});
			this.l_priority.Name = "l_priority";
			this.l_priority.Size = new System.Drawing.Size(173, 22);
			this.l_priority.Text = "Priority";
			// 
			// b_setPriority_high
			// 
			this.b_setPriority_high.Name = "b_setPriority_high";
			this.b_setPriority_high.Size = new System.Drawing.Size(160, 22);
			this.b_setPriority_high.Text = "High";
			this.b_setPriority_high.Click += new System.EventHandler(this.b_setPriority_high_Click);
			// 
			// b_setPriority_normal
			// 
			this.b_setPriority_normal.Name = "b_setPriority_normal";
			this.b_setPriority_normal.Size = new System.Drawing.Size(160, 22);
			this.b_setPriority_normal.Text = "Normal";
			this.b_setPriority_normal.Click += new System.EventHandler(this.b_setPriority_normal_Click);
			// 
			// toolStripSeparator7
			// 
			this.toolStripSeparator7.Name = "toolStripSeparator7";
			this.toolStripSeparator7.Size = new System.Drawing.Size(157, 6);
			// 
			// b_setPriority_dontDownload
			// 
			this.b_setPriority_dontDownload.Name = "b_setPriority_dontDownload";
			this.b_setPriority_dontDownload.Size = new System.Drawing.Size(160, 22);
			this.b_setPriority_dontDownload.Text = "Don\'t Download";
			this.b_setPriority_dontDownload.Click += new System.EventHandler(this.b_setPriority_dontDownload_Click);
			// 
			// l_downloadStrategy
			// 
			this.l_downloadStrategy.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_setDownloadStretegy_normal,
            this.toolStripSeparator8,
            this.b_setDownloadStrategy_leading,
            this.b_setDownloadStrategy_trailing});
			this.l_downloadStrategy.Name = "l_downloadStrategy";
			this.l_downloadStrategy.Size = new System.Drawing.Size(173, 22);
			this.l_downloadStrategy.Text = "Download strategy";
			// 
			// b_setDownloadStretegy_normal
			// 
			this.b_setDownloadStretegy_normal.Name = "b_setDownloadStretegy_normal";
			this.b_setDownloadStretegy_normal.Size = new System.Drawing.Size(175, 22);
			this.b_setDownloadStretegy_normal.Text = "Normal";
			this.b_setDownloadStretegy_normal.Click += new System.EventHandler(this.b_setDownloadStretegy_normal_Click);
			// 
			// toolStripSeparator8
			// 
			this.toolStripSeparator8.Name = "toolStripSeparator8";
			this.toolStripSeparator8.Size = new System.Drawing.Size(172, 6);
			// 
			// b_setDownloadStrategy_leading
			// 
			this.b_setDownloadStrategy_leading.Name = "b_setDownloadStrategy_leading";
			this.b_setDownloadStrategy_leading.Size = new System.Drawing.Size(175, 22);
			this.b_setDownloadStrategy_leading.Text = "Leading chunk first";
			this.b_setDownloadStrategy_leading.Click += new System.EventHandler(this.b_setDownloadStrategy_leading_Click);
			// 
			// b_setDownloadStrategy_trailing
			// 
			this.b_setDownloadStrategy_trailing.Name = "b_setDownloadStrategy_trailing";
			this.b_setDownloadStrategy_trailing.Size = new System.Drawing.Size(175, 22);
			this.b_setDownloadStrategy_trailing.Text = "Trailing chunk first";
			this.b_setDownloadStrategy_trailing.Click += new System.EventHandler(this.b_setDownloadStrategy_trailing_Click);
			// 
			// l_sepFile1
			// 
			this.l_sepFile1.Name = "l_sepFile1";
			this.l_sepFile1.Size = new System.Drawing.Size(170, 6);
			// 
			// b_mediaInfo
			// 
			this.b_mediaInfo.Name = "b_mediaInfo";
			this.b_mediaInfo.Size = new System.Drawing.Size(173, 22);
			this.b_mediaInfo.Text = "MediaInfo";
			this.b_mediaInfo.Click += new System.EventHandler(this.b_mediaInfo_Click);
			// 
			// b_downloadFile
			// 
			this.b_downloadFile.Name = "b_downloadFile";
			this.b_downloadFile.Size = new System.Drawing.Size(173, 22);
			this.b_downloadFile.Text = "Download";
			this.b_downloadFile.Click += new System.EventHandler(this.b_downloadFile_Click);
			// 
			// tab_trackers
			// 
			this.tab_trackers.Controls.Add(this.lv_trackers);
			this.tab_trackers.Location = new System.Drawing.Point(4, 22);
			this.tab_trackers.Name = "tab_trackers";
			this.tab_trackers.Padding = new System.Windows.Forms.Padding(3);
			this.tab_trackers.Size = new System.Drawing.Size(1412, 192);
			this.tab_trackers.TabIndex = 1;
			this.tab_trackers.Text = "Trackers";
			this.tab_trackers.UseVisualStyleBackColor = true;
			// 
			// lv_trackers
			// 
			this.lv_trackers.AllColumns.Add(this.lv_trackers_uri);
			this.lv_trackers.AllColumns.Add(this.lv_trackers_status);
			this.lv_trackers.AllColumns.Add(this.lv_trackers_seeders);
			this.lv_trackers.AllColumns.Add(this.lv_trackers_peers);
			this.lv_trackers.AllColumns.Add(this.lv_trackers_downloaded);
			this.lv_trackers.AllColumns.Add(this.lv_trackers_last_updated);
			this.lv_trackers.AllColumns.Add(this.lv_trackers_interval);
			this.lv_trackers.AllColumns.Add(this.lv_trackers_message);
			this.lv_trackers.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lv_trackers.CellEditUseWholeCell = false;
			this.lv_trackers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lv_trackers_uri,
            this.lv_trackers_status,
            this.lv_trackers_seeders,
            this.lv_trackers_peers,
            this.lv_trackers_downloaded,
            this.lv_trackers_last_updated,
            this.lv_trackers_interval,
            this.lv_trackers_message});
			this.lv_trackers.ContextMenuStrip = this.ctxms_trackers;
			this.lv_trackers.Cursor = System.Windows.Forms.Cursors.Default;
			this.lv_trackers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lv_trackers.FullRowSelect = true;
			this.lv_trackers.Location = new System.Drawing.Point(3, 3);
			this.lv_trackers.Name = "lv_trackers";
			this.lv_trackers.ShowGroups = false;
			this.lv_trackers.Size = new System.Drawing.Size(1406, 186);
			this.lv_trackers.TabIndex = 0;
			this.lv_trackers.UseCompatibleStateImageBehavior = false;
			this.lv_trackers.View = System.Windows.Forms.View.Details;
			this.lv_trackers.VirtualMode = true;
			// 
			// lv_trackers_uri
			// 
			this.lv_trackers_uri.AspectName = "";
			this.lv_trackers_uri.Text = "URI";
			// 
			// lv_trackers_status
			// 
			this.lv_trackers_status.AspectName = "";
			this.lv_trackers_status.Text = "Status";
			// 
			// lv_trackers_seeders
			// 
			this.lv_trackers_seeders.AspectName = "";
			this.lv_trackers_seeders.Text = "Seeders";
			// 
			// lv_trackers_peers
			// 
			this.lv_trackers_peers.AspectName = "";
			this.lv_trackers_peers.Text = "Peers";
			// 
			// lv_trackers_downloaded
			// 
			this.lv_trackers_downloaded.AspectName = "";
			this.lv_trackers_downloaded.Text = "Downloaded";
			this.lv_trackers_downloaded.Width = 59;
			// 
			// lv_trackers_last_updated
			// 
			this.lv_trackers_last_updated.AspectName = "";
			this.lv_trackers_last_updated.Text = "Last updated";
			// 
			// lv_trackers_interval
			// 
			this.lv_trackers_interval.AspectName = "";
			this.lv_trackers_interval.Text = "Interval";
			// 
			// lv_trackers_message
			// 
			this.lv_trackers_message.Text = "Message";
			// 
			// ctxms_trackers
			// 
			this.ctxms_trackers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_tracker_enable,
            this.b_tracker_disable,
            this.toolStripSeparator10,
            this.b_reannounceTracker});
			this.ctxms_trackers.Name = "contextMenuStrip2";
			this.ctxms_trackers.Size = new System.Drawing.Size(141, 76);
			this.ctxms_trackers.Opening += new System.ComponentModel.CancelEventHandler(this.ctxms_trackers_Opening);
			// 
			// b_tracker_enable
			// 
			this.b_tracker_enable.Name = "b_tracker_enable";
			this.b_tracker_enable.Size = new System.Drawing.Size(140, 22);
			this.b_tracker_enable.Text = "Enable";
			this.b_tracker_enable.Click += new System.EventHandler(this.b_tracker_enable_Click);
			// 
			// b_tracker_disable
			// 
			this.b_tracker_disable.Name = "b_tracker_disable";
			this.b_tracker_disable.Size = new System.Drawing.Size(140, 22);
			this.b_tracker_disable.Text = "Disable";
			this.b_tracker_disable.Click += new System.EventHandler(this.b_tracker_disable_Click);
			// 
			// toolStripSeparator10
			// 
			this.toolStripSeparator10.Name = "toolStripSeparator10";
			this.toolStripSeparator10.Size = new System.Drawing.Size(137, 6);
			// 
			// b_reannounceTracker
			// 
			this.b_reannounceTracker.Name = "b_reannounceTracker";
			this.b_reannounceTracker.Size = new System.Drawing.Size(140, 22);
			this.b_reannounceTracker.Text = "Reannounce";
			this.b_reannounceTracker.Click += new System.EventHandler(this.b_reannounceTracker_Click);
			// 
			// tab_peers
			// 
			this.tab_peers.Controls.Add(this.lv_peers);
			this.tab_peers.Location = new System.Drawing.Point(4, 22);
			this.tab_peers.Name = "tab_peers";
			this.tab_peers.Size = new System.Drawing.Size(1412, 192);
			this.tab_peers.TabIndex = 2;
			this.tab_peers.Text = "Peers";
			this.tab_peers.UseVisualStyleBackColor = true;
			// 
			// lv_peers
			// 
			this.lv_peers.AllColumns.Add(this.lv_peers_flag);
			this.lv_peers.AllColumns.Add(this.lv_peers_ip);
			this.lv_peers.AllColumns.Add(this.lv_peers_client);
			this.lv_peers.AllColumns.Add(this.lv_peers_flags);
			this.lv_peers.AllColumns.Add(this.lv_peers_done);
			this.lv_peers.AllColumns.Add(this.lv_peers_dlSpeed);
			this.lv_peers.AllColumns.Add(this.lv_peers_upSpeed);
			this.lv_peers.AllColumns.Add(this.lv_peers_downloaded);
			this.lv_peers.AllColumns.Add(this.lv_peers_uploaded);
			this.lv_peers.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lv_peers.CausesValidation = false;
			this.lv_peers.CellEditUseWholeCell = false;
			this.lv_peers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lv_peers_flag,
            this.lv_peers_ip,
            this.lv_peers_client,
            this.lv_peers_flags,
            this.lv_peers_done,
            this.lv_peers_dlSpeed,
            this.lv_peers_upSpeed,
            this.lv_peers_downloaded,
            this.lv_peers_uploaded});
			this.lv_peers.ContextMenuStrip = this.ctxms_peer;
			this.lv_peers.Cursor = System.Windows.Forms.Cursors.Default;
			this.lv_peers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lv_peers.EmptyListMsg = "No peers";
			this.lv_peers.FullRowSelect = true;
			this.lv_peers.HideSelection = false;
			this.lv_peers.Location = new System.Drawing.Point(0, 0);
			this.lv_peers.Name = "lv_peers";
			this.lv_peers.ShowGroups = false;
			this.lv_peers.ShowHeaderInAllViews = false;
			this.lv_peers.Size = new System.Drawing.Size(1412, 192);
			this.lv_peers.SmallImageList = this.CompanyImgs;
			this.lv_peers.TabIndex = 0;
			this.lv_peers.UseCompatibleStateImageBehavior = false;
			this.lv_peers.View = System.Windows.Forms.View.Details;
			this.lv_peers.VirtualMode = true;
			this.lv_peers.BeforeSorting += new System.EventHandler<BrightIdeasSoftware.BeforeSortingEventArgs>(this.OLVBeforeSorting);
			this.lv_peers.CellToolTipShowing += new System.EventHandler<BrightIdeasSoftware.ToolTipShowingEventArgs>(this.lv_peers_CellToolTipShowing);
			this.lv_peers.Filter += new System.EventHandler<BrightIdeasSoftware.FilterEventArgs>(this.OLVFilter);
			// 
			// lv_peers_flag
			// 
			this.lv_peers_flag.Text = "";
			this.lv_peers_flag.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.lv_peers_flag.Width = 22;
			// 
			// lv_peers_ip
			// 
			this.lv_peers_ip.AspectName = "";
			this.lv_peers_ip.Text = "IP";
			// 
			// lv_peers_client
			// 
			this.lv_peers_client.AspectName = "";
			this.lv_peers_client.Text = "Client";
			// 
			// lv_peers_flags
			// 
			this.lv_peers_flags.AspectName = "";
			this.lv_peers_flags.Text = "Flags";
			this.lv_peers_flags.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// lv_peers_done
			// 
			this.lv_peers_done.AspectName = "";
			this.lv_peers_done.Text = "Done";
			// 
			// lv_peers_dlSpeed
			// 
			this.lv_peers_dlSpeed.AspectName = "";
			this.lv_peers_dlSpeed.Text = "DL Speed";
			this.lv_peers_dlSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lv_peers_upSpeed
			// 
			this.lv_peers_upSpeed.AspectName = "";
			this.lv_peers_upSpeed.Text = "UP Speed";
			this.lv_peers_upSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lv_peers_downloaded
			// 
			this.lv_peers_downloaded.AspectName = "";
			this.lv_peers_downloaded.Text = "Downloaded";
			this.lv_peers_downloaded.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lv_peers_uploaded
			// 
			this.lv_peers_uploaded.AspectName = "";
			this.lv_peers_uploaded.Text = "Uploaded";
			this.lv_peers_uploaded.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// ctxms_peer
			// 
			this.ctxms_peer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_ban_peer,
            this.b_kick_peer,
            this.b_snub_peer,
            this.b_unsnub_peer});
			this.ctxms_peer.Name = "ctxms_peer";
			this.ctxms_peer.Size = new System.Drawing.Size(116, 92);
			this.ctxms_peer.Opening += new System.ComponentModel.CancelEventHandler(this.ctxms_peer_Opening);
			// 
			// b_ban_peer
			// 
			this.b_ban_peer.Name = "b_ban_peer";
			this.b_ban_peer.Size = new System.Drawing.Size(115, 22);
			this.b_ban_peer.Text = "Ban";
			this.b_ban_peer.Click += new System.EventHandler(this.b_ban_peer_Click);
			// 
			// b_kick_peer
			// 
			this.b_kick_peer.Name = "b_kick_peer";
			this.b_kick_peer.Size = new System.Drawing.Size(115, 22);
			this.b_kick_peer.Text = "Kick";
			this.b_kick_peer.Click += new System.EventHandler(this.b_kick_peer_Click);
			// 
			// b_snub_peer
			// 
			this.b_snub_peer.Name = "b_snub_peer";
			this.b_snub_peer.Size = new System.Drawing.Size(115, 22);
			this.b_snub_peer.Text = "Snub";
			this.b_snub_peer.Click += new System.EventHandler(this.b_snub_peer_Click);
			// 
			// b_unsnub_peer
			// 
			this.b_unsnub_peer.Name = "b_unsnub_peer";
			this.b_unsnub_peer.Size = new System.Drawing.Size(115, 22);
			this.b_unsnub_peer.Text = "Unsnub";
			this.b_unsnub_peer.Click += new System.EventHandler(this.b_unsnub_peer_Click);
			// 
			// CompanyImgs
			// 
			this.CompanyImgs.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this.CompanyImgs.ImageSize = new System.Drawing.Size(16, 16);
			this.CompanyImgs.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// tab_graph
			// 
			this.tab_graph.Controls.Add(this.graph);
			this.tab_graph.Location = new System.Drawing.Point(4, 22);
			this.tab_graph.Name = "tab_graph";
			this.tab_graph.Size = new System.Drawing.Size(1412, 192);
			this.tab_graph.TabIndex = 4;
			this.tab_graph.Text = "Graph";
			this.tab_graph.UseVisualStyleBackColor = true;
			// 
			// graph
			// 
			chartArea1.AxisY.LabelAutoFitMinFontSize = 7;
			chartArea1.InnerPlotPosition.Auto = false;
			chartArea1.InnerPlotPosition.Height = 82.51407F;
			chartArea1.InnerPlotPosition.Width = 92F;
			chartArea1.InnerPlotPosition.X = 4F;
			chartArea1.InnerPlotPosition.Y = 5.0266F;
			chartArea1.Name = "graph_chartArea";
			this.graph.ChartAreas.Add(chartArea1);
			this.graph.Cursor = System.Windows.Forms.Cursors.Default;
			this.graph.Dock = System.Windows.Forms.DockStyle.Fill;
			legend1.BackColor = System.Drawing.Color.White;
			legend1.Name = "graph_legend";
			legend1.Position.Auto = false;
			legend1.Position.Height = 19.89529F;
			legend1.Position.Width = 6F;
			legend1.Position.X = 94F;
			this.graph.Legends.Add(legend1);
			this.graph.Location = new System.Drawing.Point(0, 0);
			this.graph.Name = "graph";
			series1.BorderWidth = 3;
			series1.ChartArea = "graph_chartArea";
			series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series1.Legend = "graph_legend";
			series1.Name = "UP Speed";
			series1.Points.Add(dataPoint1);
			series1.Points.Add(dataPoint2);
			series1.Points.Add(dataPoint3);
			series1.Points.Add(dataPoint4);
			series1.Points.Add(dataPoint5);
			series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
			series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.UInt64;
			series2.BorderWidth = 3;
			series2.ChartArea = "graph_chartArea";
			series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series2.Legend = "graph_legend";
			series2.Name = "DL Speed";
			series2.Points.Add(dataPoint6);
			series2.Points.Add(dataPoint7);
			series2.Points.Add(dataPoint8);
			series2.Points.Add(dataPoint9);
			series2.Points.Add(dataPoint10);
			series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
			series2.YValuesPerPoint = 4;
			series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.UInt64;
			this.graph.Series.Add(series1);
			this.graph.Series.Add(series2);
			this.graph.Size = new System.Drawing.Size(1412, 192);
			this.graph.TabIndex = 0;
			this.graph.Text = "chart1";
			this.graph.Customize += new System.EventHandler(this.graph_Customize);
			this.graph.MouseMove += new System.Windows.Forms.MouseEventHandler(this.graph_MouseMove);
			// 
			// ni_global
			// 
			this.ni_global.Icon = ((System.Drawing.Icon)(resources.GetObject("ni_global.Icon")));
			this.ni_global.Text = "RT#";
			this.ni_global.Visible = true;
			// 
			// lv_sdss
			// 
			this.lv_sdss.AllColumns.Add(this.olv_sdss_main);
			this.lv_sdss.CellEditUseWholeCell = false;
			this.lv_sdss.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olv_sdss_main});
			this.lv_sdss.Cursor = System.Windows.Forms.Cursors.Default;
			this.lv_sdss.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lv_sdss.FullRowSelect = true;
			this.lv_sdss.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.lv_sdss.Location = new System.Drawing.Point(3, 3);
			this.lv_sdss.Name = "lv_sdss";
			this.lv_sdss.RowHeight = 54;
			this.lv_sdss.ShowGroups = false;
			this.lv_sdss.Size = new System.Drawing.Size(0, 350);
			this.lv_sdss.SmallImageList = this.ImgList;
			this.lv_sdss.TabIndex = 1;
			this.lv_sdss.UseCompatibleStateImageBehavior = false;
			this.lv_sdss.View = System.Windows.Forms.View.Details;
			this.lv_sdss.CellRightClick += new System.EventHandler<BrightIdeasSoftware.CellRightClickEventArgs>(this.lv_sdss_CellRightClick);
			this.lv_sdss.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lv_sdss_MouseDoubleClick);
			// 
			// olv_sdss_main
			// 
			this.olv_sdss_main.AspectName = "Title";
			this.olv_sdss_main.FillsFreeSpace = true;
			this.olv_sdss_main.Text = "Connections";
			this.olv_sdss_main.Width = 25;
			// 
			// ctxms_tracker
			// 
			this.ctxms_tracker.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_set_alias,
            this.b_fetch_icon});
			this.ctxms_tracker.Name = "ctxms_tracker";
			this.ctxms_tracker.Size = new System.Drawing.Size(130, 48);
			// 
			// b_set_alias
			// 
			this.b_set_alias.Name = "b_set_alias";
			this.b_set_alias.Size = new System.Drawing.Size(129, 22);
			this.b_set_alias.Text = "Set alias";
			this.b_set_alias.Click += new System.EventHandler(this.b_set_alias_Click);
			// 
			// b_fetch_icon
			// 
			this.b_fetch_icon.Name = "b_fetch_icon";
			this.b_fetch_icon.Size = new System.Drawing.Size(129, 22);
			this.b_fetch_icon.Text = "Fetch icon";
			this.b_fetch_icon.Click += new System.EventHandler(this.b_fetch_icon_Click);
			// 
			// t_animate
			// 
			this.t_animate.Enabled = true;
			this.t_animate.Interval = 500;
			this.t_animate.Tick += new System.EventHandler(this.t_animate_Tick);
			// 
			// ctxms_connection
			// 
			this.ctxms_connection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b_setSDSHighlighting});
			this.ctxms_connection.Name = "ctxms_connection";
			this.ctxms_connection.Size = new System.Drawing.Size(189, 26);
			// 
			// b_setSDSHighlighting
			// 
			this.b_setSDSHighlighting.Name = "b_setSDSHighlighting";
			this.b_setSDSHighlighting.Size = new System.Drawing.Size(188, 22);
			this.b_setSDSHighlighting.Text = "Set highlighting color";
			this.b_setSDSHighlighting.Click += new System.EventHandler(this.b_setSDSHighlighting_Click);
			// 
			// tabs_right
			// 
			this.tabs_right.Alignment = System.Windows.Forms.TabAlignment.Right;
			this.tabs_right.Controls.Add(this.tab_sdss);
			this.tabs_right.Dock = System.Windows.Forms.DockStyle.Right;
			this.tabs_right.Location = new System.Drawing.Point(1420, 27);
			this.tabs_right.Multiline = true;
			this.tabs_right.Name = "tabs_right";
			this.tabs_right.SelectedIndex = 0;
			this.tabs_right.Size = new System.Drawing.Size(24, 364);
			this.tabs_right.TabIndex = 6;
			this.tabs_right.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabs_right_MouseClick);
			// 
			// tab_sdss
			// 
			this.tab_sdss.Controls.Add(this.lv_sdss);
			this.tab_sdss.Location = new System.Drawing.Point(4, 4);
			this.tab_sdss.Name = "tab_sdss";
			this.tab_sdss.Padding = new System.Windows.Forms.Padding(3);
			this.tab_sdss.Size = new System.Drawing.Size(0, 356);
			this.tab_sdss.TabIndex = 0;
			this.tab_sdss.Text = "Connections";
			this.tab_sdss.UseVisualStyleBackColor = true;
			// 
			// f_main
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1444, 413);
			this.Controls.Add(this.tabs_right);
			this.Controls.Add(this.split_torrents);
			this.Controls.Add(this.status_main);
			this.Controls.Add(this.menu_main);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menu_main;
			this.Name = "f_main";
			this.Text = "RT#";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.f_main_FormClosing);
			this.Load += new System.EventHandler(this.f_main_Load);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.f_main_DragDrop);
			this.DragOver += new System.Windows.Forms.DragEventHandler(this.f_main_DragOver);
			this.menu_main.ResumeLayout(false);
			this.menu_main.PerformLayout();
			this.status_main.ResumeLayout(false);
			this.status_main.PerformLayout();
			this.ctxms_torrent.ResumeLayout(false);
			this.split_torrents.Panel1.ResumeLayout(false);
			this.split_torrents.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.split_torrents)).EndInit();
			this.split_torrents.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.lv_torrents)).EndInit();
			this.tab_main.ResumeLayout(false);
			this.tab_general.ResumeLayout(false);
			this.g_info.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.g_transfer.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.tab_files.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dtlv_files)).EndInit();
			this.ctxms_file.ResumeLayout(false);
			this.tab_trackers.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.lv_trackers)).EndInit();
			this.ctxms_trackers.ResumeLayout(false);
			this.tab_peers.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.lv_peers)).EndInit();
			this.ctxms_peer.ResumeLayout(false);
			this.tab_graph.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.graph)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lv_sdss)).EndInit();
			this.ctxms_tracker.ResumeLayout(false);
			this.ctxms_connection.ResumeLayout(false);
			this.tabs_right.ResumeLayout(false);
			this.tab_sdss.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		public System.Windows.Forms.ToolStripStatusLabel l_status;
		public System.Windows.Forms.ToolStripMenuItem b_refresh;
		public System.Windows.Forms.ToolStripMenuItem b_pause;
		public System.Windows.Forms.ToolStripMenuItem b_resume;
		public ToolStripTextBoxWPlaceholder t_filter;
		public ImageList ImgList;
		public ToolStripMenuItem b_addTorrent;
		public ToolStripMenuItem b_torrent_start;
		public ToolStripMenuItem b_torrent_pause;
		public ToolStripMenuItem b_torrent_stop;
		public ToolStripSeparator l_sep2;
		public ToolStripMenuItem b_torrent_force_recheck;
		public ToolStripMenuItem b_torrent_update_trackers;
		public ToolStripMenuItem b_torrent_add_peer;
		public ToolStripSeparator l_sep3;
		public ToolStripMenuItem l_set_label;
		public ToolStripMenuItem b_torrent_remove_label;
		public ToolStripSeparator toolStripSeparator1;
		public ToolStripMenuItem l_set_priority;
		public ToolStripMenuItem b_torrent_set_prio_high;
		public ToolStripMenuItem b_torrent_set_prio_normal;
		public ToolStripMenuItem b_torrent_set_prio_low;
		public ToolStripMenuItem b_torrent_set_prio_off;
		public ToolStripSeparator l_sep4;
		public ToolStripMenuItem b_torrent_remove;
		public ToolStripMenuItem b_torrent_remove_data;
		public ToolStripSeparator l_sep5;
		public ToolStripMenuItem b_torrent_get_file;
		public ToolStripMenuItem toolStripMenuItem1;
		public MenuStrip menu_main;
		public ToolStripMenuItem menu_file;
		public StatusStrip status_main;
		public NotifyIcon ni_global;
		public ContextMenuStrip ctxms_torrent;
		private TabControl tab_main;
		private TabPage tab_files;
		private TabPage tab_trackers;
		private TabPage tab_peers;
		public FastObjectListView lv_torrents;
		private OLVColumn lvc_state;
		private OLVColumn lvc_size;
		private OLVColumn lvc_downloaded;
		private OLVColumn lvc_uploaded;
		private OLVColumn lvc_dlspeed;
		private OLVColumn lvc_upspeed;
		private OLVColumn lvc_created_on;
		private OLVColumn lvc_added_on;
		private OLVColumn lvc_finished_on;
		private OLVColumn lvc_label;
		private OLVColumn lvc_peers;
		private OLVColumn lvc_seeders;
		private OLVColumn lvc_priority;
		private OLVColumn lvc_tracker;
        public FastObjectListView lv_peers;
        private OLVColumn lv_peers_flag;
        private OLVColumn lv_peers_flags;
        private OLVColumn lv_peers_dlSpeed;
        private OLVColumn lv_peers_upSpeed;
        private OLVColumn lv_peers_downloaded;
        private OLVColumn lv_peers_uploaded;
        public ImageList CompanyImgs;
		private ToolStripStatusLabel l_statusSeperator;
		private ObjectListView lv_sdss;
		private OLVColumn olv_sdss_main;
		private OLVColumn lv_peers_client;
		private OLVColumn lvc_ratio;
		private OLVColumn lvc_eta;
		private OLVColumn lvc_remaining_size;
		private OLVColumn lvc_done;
		private OLVColumn lv_peers_done;
		private ContextMenuStrip ctxms_tracker;
		private ToolStripMenuItem b_set_alias;
		private ToolStripMenuItem b_fetch_icon;
		public OLVColumn lv_peers_ip;
		public OLVColumn lvc_name;
		private ToolStripMenuItem b_copy_info_hash;
		private OLVColumn dtlv_files_name;
		private OLVColumn dtlv_files_size;
		private OLVColumn dtlv_files_downloaded;
		private OLVColumn dtlv_files_done;
		private OLVColumn dtlv_files_priority;
		private Timer t_animate;
		private ToolStripMenuItem b_transferTorrentData;
		private OLVColumn lv_trackers_uri;
		private OLVColumn lv_trackers_status;
		private OLVColumn lv_trackers_seeders;
		private OLVColumn lv_trackers_peers;
		private OLVColumn lv_trackers_downloaded;
		private OLVColumn lv_trackers_last_updated;
		private OLVColumn lv_trackers_interval;
		private ContextMenuStrip ctxms_peer;
		private ToolStripMenuItem b_ban_peer;
		private ToolStripMenuItem b_kick_peer;
		private ToolStripMenuItem b_snub_peer;
		private ToolStripMenuItem b_unsnub_peer;
		private ContextMenuStrip ctxms_trackers;
		private ToolStripMenuItem b_tracker_enable;
		private ToolStripMenuItem b_tracker_disable;
		private ContextMenuStrip ctxms_file;
		private ToolStripMenuItem l_priority;
		private ToolStripMenuItem b_setPriority_high;
		private ToolStripMenuItem b_setPriority_normal;
		private ToolStripSeparator toolStripSeparator7;
		private ToolStripMenuItem b_setPriority_dontDownload;
		private ToolStripMenuItem l_downloadStrategy;
		private ToolStripMenuItem b_setDownloadStretegy_normal;
		private ToolStripSeparator toolStripSeparator8;
		private ToolStripMenuItem b_setDownloadStrategy_leading;
		private ToolStripMenuItem b_setDownloadStrategy_trailing;
		private ToolStripSeparator l_sepFile1;
		private ToolStripMenuItem b_mediaInfo;
		private ToolStripMenuItem b_downloadFile;
		private ToolStripMenuItem b_editTorrent;
		private ToolStripMenuItem b_sdss;
		private ToolStripMenuItem b_plugins;
		public SplitContainer split_torrents;
		public DataTreeListView dtlv_files;
		public FastObjectListView lv_trackers;
		private ToolStripSeparator sep_file_2;
		private ToolStripMenuItem b_about;
		private ToolStripMenuItem b_exit;
		private OLVColumn lvc_owner;
		private ToolStripSeparator toolStripSeparator10;
		private ToolStripMenuItem b_reannounceTracker;
		private ToolStripMenuItem b_torrent_set_data_directory;
		private ContextMenuStrip ctxms_connection;
		private ToolStripMenuItem b_setSDSHighlighting;
		private TabPage tab_general;
		private GroupBox g_info;
		private TableLayoutPanel tableLayoutPanel2;
		private Label l_remotePath;
		private Label l_createdOn;
		private Label l_addedOn;
		private GroupBox g_transfer;
		private TableLayoutPanel tableLayoutPanel1;
		private Label l_timeElapsed;
		private Label l_downloaded;
		private Label l_uploaded;
		private Label l_seeders;
		private Label l_valTimeElapsed;
		private Label l_valUploaded;
		private Label l_valDownloaded;
		private Label l_valSeeders;
		private Label l_remaining;
		private Label l_upSpeed;
		private Label l_dlSpeed;
		private Label l_peers;
		private Label l_valRemaining;
		private Label l_valDlSpeed;
		private Label l_valUpSpeed;
		private Label l_valPeers;
		private Label l_ratio;
		private Label l_wasted;
		private Label l_valRatio;
		private Label l_valWasted;
		private Label l_valRemotePath;
		private Label l_valCreatedOn;
		private Label l_valAddedOn;
		private Label l_valInfoHash;
		private Label l_hash;
		private Label l_comment;
		private Label l_valComment;
		private Label l_lStatus;
		private Label l_valStatus;
		private OLVColumn lv_trackers_message;
		private ToolStripMenuItem b_isfaTorrent;
		private ToolStripMenuItem b_isfaQueue;
		private ToolStripSeparator l_sep1;
		private TabControl tabs_right;
		private TabPage tab_sdss;
		private ToolStripSeparator sep_file_1;
		private ToolStripMenuItem b_settings;
		private TabPage tab_graph;
		private System.Windows.Forms.DataVisualization.Charting.Chart graph;
	}
}

