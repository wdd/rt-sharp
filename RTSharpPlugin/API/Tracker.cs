﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RTSharpIFace
{
	/// <summary>
	/// Compares trackers
	/// </summary>
	public class TrackerComparer : IEqualityComparer<TRACKER>
	{
		readonly bool CompareStatus;
		public TrackerComparer(bool CompareStatus)
		{
			this.CompareStatus = CompareStatus;
		}
		public bool Equals(TRACKER x, TRACKER y)
		{
			bool ret;
			if (CompareStatus)
				ret = x.Status == y.Status;
			else
				ret = true;
			if (ret)
				ret = x.Uri.Equals(y.Uri);
			return ret;
		}

		public int GetHashCode(TRACKER obj) => obj.Uri.GetHashCode() + (CompareStatus ? (int)obj.Status : 0);
	}

	/// <summary>
	/// Tracker
	/// </summary>
	/// <seealso cref="TORRENT.Trackers"/>
	public class TRACKER
	{
		public enum TRACKER_STATUS : byte {
			ACTIVE = 1 << 0,
			NOT_ACTIVE = 1 << 1,
			NOT_CONTACTED_YET = 1 << 2,
			DISABLED = 1 << 3,
			ENABLED = 1 << 4
		}
		/// <summary>
		/// Parent torrent
		/// </summary>
		public TORRENT Parent { get; }

		/// <summary>
		/// Tracker ID internal to SDS
		/// </summary>
		public object ID { get; }

		/// <summary>
		/// Tracker Uri
		/// </summary>
		public Uri Uri { get; set; }

		/// <summary>
		/// Tracker status
		/// </summary>
		public TRACKER_STATUS Status { get; set; }

		public string StatusStr {
			get {
				List<string> ret = new List<string>(5);
				if (Status.HasFlag(TRACKER_STATUS.ACTIVE))
					ret.Add("active");
				if (Status.HasFlag(TRACKER_STATUS.NOT_ACTIVE))
					ret.Add("inactive");
				if (Status.HasFlag(TRACKER_STATUS.NOT_CONTACTED_YET))
					ret.Add("not contacted yet");
				if (Status.HasFlag(TRACKER_STATUS.DISABLED))
					ret.Add("disabled");
				if (Status.HasFlag(TRACKER_STATUS.ENABLED))
					ret.Add("enabled");

				if (ret.Count == 0)
					return "";

				string sret = ret.Aggregate((i, j) => i + ", " + j);
				return Char.ToUpper(sret[0]) + sret.Remove(0, 1);
			}
		}

		/// <summary>
		/// Is tracker enabled
		/// </summary>
		public bool Enabled => Status.HasFlag(TRACKER_STATUS.ENABLED) && !Status.HasFlag(TRACKER_STATUS.DISABLED);

		/// <summary>
		/// Seeders
		/// </summary>
		public uint Seeders { get; set; }

		/// <summary>
		/// Peers
		/// </summary>
		public uint Peers { get; set; }

		/// <summary>
		/// How many peers downloaded so far
		/// </summary>
		/// <remarks>???</remarks>
		public uint Downloaded { get; set; }

		/// <summary>
		/// Unix timestamp of last tracker announce
		/// </summary>
		public ulong LastUpdated { get; set; }

		/// <summary>
		/// Announce interval, in seconds
		/// </summary>
		public uint Interval { get; set; }

		/// <summary>
		/// Tracker status message
		/// </summary>
		public string StatusMsg { get; set; }

		public TRACKER(TORRENT Parent, object ID)
		{
			this.Parent = Parent;
			this.ID = ID;
		}

		public TRACKER Copy(TORRENT Parent)
		{
			var cp = new TRACKER(Parent, ID);
			cp.Uri = Uri;
			cp.Status = Status;
			cp.Seeders = Seeders;
			cp.Peers = Peers;
			cp.Downloaded = Downloaded;
			cp.LastUpdated = LastUpdated;
			cp.Interval = Interval;
			cp.StatusMsg = StatusMsg;
			return cp;
		}
	}
}
