﻿using System;
using System.Windows.Forms;

namespace RTSharp {
	public partial class f_confirmBox : Form {

		bool confirm;

		public f_confirmBox(string Text, string Details) {
			InitializeComponent();
			l_text.Text = Text;
			t_details.Text = Details;
        }

		private void b_yes_Click(object sender, EventArgs e) {
			confirm = true;
			Close();
		}

		private void b_no_Click(object sender, EventArgs e) {
			confirm = false;
			Close();
		}

		private void f_confirmBox_FormClosing(object sender, FormClosingEventArgs e) {
			DialogResult = confirm ? DialogResult.Yes : DialogResult.No;
		}
	}
}
