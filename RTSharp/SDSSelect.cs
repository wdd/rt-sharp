﻿using RTSharpIFace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTSharp
{
	public partial class SDSSelect : Form
	{
		readonly SemaphoreSlim ss = new SemaphoreSlim(0, 1);
		DialogResult res;
		public ISDS SDS { get; private set; }

		public SDSSelect(IEnumerable<ISDS> Exclude = null)
		{
			InitializeComponent();

			var toAddArr = new SystemDataSupplierClass[Global.SDSS.Count];
			Global.SDSS.CopyTo(toAddArr);
			var toAdd = toAddArr.ToList();
			if (Exclude != null)
				toAdd.RemoveAll(x => Exclude.Any(i => i.UniqueGUID.Equals(x.SDS.UniqueGUID)));

			cmb_sds.Items.AddRange(toAdd.ToArray());
		}

		public new async Task<DialogResult> ShowDialog()
		{
			base.ShowDialog();
			await ss.WaitAsync();

			SDS = ((SystemDataSupplierClass)cmb_sds.SelectedItem)?.SDS;

			return res;
		}

		private void b_ok_Click(object sender, EventArgs e)
		{
			ss.Release();
			res = DialogResult.OK;
			Close();
		}

		private void b_cancel_Click(object sender, EventArgs e)
		{
			ss.Release();
			res = DialogResult.Cancel;
			Close();
		}
	}
}
